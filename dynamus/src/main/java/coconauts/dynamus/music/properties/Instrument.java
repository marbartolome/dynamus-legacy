package coconauts.dynamus.music.properties;

public enum Instrument implements MusicProperty{
	/*
	 * A Dynamus instrument, currently directly maps to a General MIDI instrument number.
	 */
	piano(1),
	guitar(25),
	violin(41),
	strings(49);
	
	int generalMidiNumber;
	
	Instrument(int number){
		generalMidiNumber = number;
	}
	
	public int toMidi(){
		return generalMidiNumber;
	}
	
}
