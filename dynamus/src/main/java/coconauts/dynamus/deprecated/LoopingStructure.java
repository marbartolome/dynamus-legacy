//package coconauts.dynamus.deprecated;
//
//import java.io.Serializable;
//
//import coconauts.dynamus.music.MidiPlayer;
//import coconauts.dynamus.music.MusicStructure.PlayException;
//import coconauts.dynamus.music.playables.EmptyMusic;
//import coconauts.dynamus.music.playables.Playable;
//
//import coconauts.dynamus.events.*;
//
//public class LoopingStructure extends Playable implements Serializable {
//	MusicTransitions transitions;
//	EventLogic evl;
//	
//	private LoopingStructure(){
//		transitions = new MusicTransitions();
//	}
//	
//	public LoopingStructure(Playable mus, EventLogic evl){
//		this();
//		this.setContent(mus);
//		this.setEventLogic(evl);
//	}
//	
//	public void setContent(Playable mus){
//		transitions.addState("content", mus);
//		transitions.addStartEOSTransition("content");
//		transitions.addEndOfStateTransition("content", "content");
//	}
//	
//	public void setEventLogic(EventLogic evl){
//		transitions.addState("endst", new EmptyMusic());
//		transitions.addTransition("content", "endst", evl);
//	}
//	
//
//	@Override
//	protected void playImpl(MidiPlayer player) throws PlayException {
//		transitions.play(player);
//	}
//	
//	/*
//	public void play(Object ... params) throws PlayException{
//		
//		super.isPlaying = true;
//		
//		EventListener evListener = new EventListener();
//		
//		escSeq.play(evListener, exitEL, params);
//
//		super.isPlaying = false;
//	}*/
//	
//
//	@Override
//	public String toString() {
//		return transitions.toString();
//	}
//
//
//}
