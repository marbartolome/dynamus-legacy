//package coconauts.dynamus.deprecated;
//import java.io.Serializable;
//import java.util.*;
//
//import coconauts.dynamus.music.MusicStructure.PlayException;
//import coconauts.dynamus.music.properties.TimeSignature;
//
//import org.apache.log4j.Logger;
//
//
//public class SequentialMusic extends Playable implements Serializable {
//	/**
//	 * 'Sequential music' is a sequential list of music structures to be executed one after another
//	 * 
//	 */
//	
//	private static final Logger log = Logger.getLogger(SequentialMusic.class);
//	
//	List<Playable> list ;
//	//TODO take TS out, don't want it now
//	TimeSignature timeSignature;
//	
//	
//	public SequentialMusic(){
//		this( new TimeSignature() );
//	}
//
//	public SequentialMusic(TimeSignature signature){
//		this.list = new ArrayList<Playable>();
//		this.timeSignature = signature;
//	}
//	
//	public SequentialMusic(int signatureNumerator, DurationValue signatureDenominator){
//		this.list = new ArrayList<Playable>();
//		this.timeSignature = new TimeSignature( signatureNumerator, signatureDenominator );
//	}
//	
//	
//	public SequentialMusic sequentiate(Playable ... structs){
//		for(Playable mSt:structs){
//			this.list.add(mSt);
//		}
//		return this;
//	}
//
//	/*
//	public Object toMidi(){
//		List<Object> contentsToMidi = new ArrayList<Object>();
//		for (MusicStructure me:sequence){
//			contentsToMidi.add(me.toMidi());
//		}
//	}*/
//	
//	public void play(Object ... params) throws PlayException{
//		//sequentially play each music structure
//		super.isPlaying = true;
//		
//		log.debug("we are playing a sequence");
//		for (Playable me:list){
//			me.play(params);
//		}
//		
//		super.isPlaying = false;
//	}
//	
//	public String toString(){
//		String str="";
//		if (this.list.isEmpty()){
//			str = "Empty SequentialMusic";
//		}
//		else {
//			for (Playable e : this.list)
//			    str=str+" "+e;
//		}
//		return str;
//		
//		
//	}
//	
//}
