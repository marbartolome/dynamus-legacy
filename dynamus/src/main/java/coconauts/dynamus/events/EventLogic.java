package coconauts.dynamus.events;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

public abstract class EventLogic {
	private static final Logger log = Logger.getLogger(EventLogic.class);
	
	//list of event references that will test against
	//our logic when the .evaluate() method is called.
	Set<EventReference> evalEvents;
	
	/*
	 * Tu be called by subclasses
	 */
	protected EventLogic(){
		this.evalEvents = new HashSet<EventReference>();
	}
	
	public Set<EventReference> getEvalEvents(){
		return this.evalEvents;
	}

	public void addEvalEvent(EventReference evr){
		//boolean added = false;
		log.trace("adding eval event: "+evr+" to logic: "+this);
		if (this.isInLogic(evr)){
			//added = evalEvents.add(evr);
			evalEvents.add(evr);
			log.trace("....added");
		}
		else {
			log.trace("..... not added bc not relevant to logic");
		}

	}
	
	/**
	 * Retrieve a set of sets of event references.
	 * The presence of all the expressions in each set
	 * would evaluate the EventLogic as true.
	 * 
	 * For instance, if our logic is:
	 * (A or B) and (D and C)
	 * 
	 * Out truth values set would be:
	 * 1. A, D, C
	 * 2. B, D, C
	 * 
	 * Either of those combinations will evaluate
	 * the event logic to true.
	 * 
	 */
	public abstract Set<Set<EventReference>> getTruthValues();
	
	
	public boolean equals(EventLogic evl){
		//Two event logics will be equal if the same event references
		//make them evaluate to true
		log.trace("testing if "+(evl)+" equals "+this);
		boolean result = this.getTruthValues().equals(evl.getTruthValues());
		log.debug(".... resul: "+result);
		return result;
	}
	
	@Override
	public boolean equals(Object obj){
		log.trace("testing if "+((EventLogic)obj)+" equals "+this);
		EventLogic evl = (EventLogic) obj;
		boolean result =  this.equals(evl);
		log.debug(".... resul: "+result);
		return result;
	}
	
	public abstract boolean isEmpty();
	
	public abstract  boolean isInLogic(EventReference evr);
	
	
	public boolean evaluate(){
		log.debug("evaluating with inner events: "+Arrays.toString(evalEvents.toArray()));
		return this.evaluate(evalEvents.toArray(new EventReference[evalEvents.size()]));
	}
	
	public boolean evaluate(Set<EventReference> evrefs){
		return this.evaluate(evrefs.toArray(new EventReference[evalEvents.size()]));
	}
	
	public abstract boolean evaluate(EventReference ... params);
	
	
}
