package coconauts.dynamus.tests;


import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import coconauts.dynamus.events.AgregateEventLogic;
import coconauts.dynamus.events.EventListener;
import coconauts.dynamus.events.EventLogic;
import coconauts.dynamus.events.EventReference;
import coconauts.dynamus.events.EventServer;
import coconauts.dynamus.events.API.Event;
import coconauts.dynamus.events.API.EventClient;
import coconauts.dynamus.events.EventListener.AlreadyListeningException;
import coconauts.dynamus.events.AgregateEventLogic.Operator;


public class EventsTest {

	
	private static final Logger log = Logger.getLogger(EventsTest.class);
	
	private EventServer server;
	private EventClient client;
	private EventLogic fooAndBar, fooOrBar;
	private EventReference fooEv, barEv;
	
	private boolean infiniteClientStop;
	Thread infiniteClientDaemon;

	@Before
	public void setUp(){
		PropertyConfigurator.configure("src/main/java/coconauts/dynamus/logs/log4j.properties");
		
		log.debug("setting up...");
		server = new EventServer();
		log.debug("server created");
		client = new EventClient();
		log.debug("client created");
		server.connectToClient();
		server.start();
		log.debug("server started");
		fooEv = new EventReference("foo");
		barEv = new EventReference("bar");
		fooAndBar = new AgregateEventLogic(Operator.and, fooEv, barEv);
		fooOrBar = new AgregateEventLogic(Operator.or, fooEv, barEv);
		
		infiniteClientStop = false;
		
		log.debug("set up complete");
		
	}
	
	@After
	public void tidyUp(){
		log.debug("tidying up");
		server.stop();
		client.close();
		log.debug("finished cleaning");
	}
	
	@Test
	public void testSendEventFromClientToServer(){
		log.debug("sending event");
		try {
			client.send(new Event("foo"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("event sent");
	}
	
	@Test
	public void testListeners(){
		EventListener andList = new EventListener();
		EventListener orList = new EventListener();
		try {
			andList.listen(fooAndBar);
			orList.listen(fooOrBar);
		} catch (AlreadyListeningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		server.registerListener(andList);
		server.registerListener(orList);*/
		
		try {
			client.send(new Event("foo"));
			client.send(new Event("nanana"));
			client.send(new Event("bar"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Set<EventReference> andSet = andList.waitForLogic();
		Set<EventReference> orSet = orList.waitForLogic();
		
		
		log.debug("andSet: "+ andSet);
		log.debug("orSet: "+orSet);
		
	}
	
	
	/**
	 * Tests creation and closure of a client that
	 * continously sends random events.
	 * 
	 * Client is running for 5 seconds, and is then 
	 * stopped.
	 */
	@Test
	public void testInfiniteLoopClient(){
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		infiniteLoopClient(client);
		infiniteLoopClient();
		try {
			Thread.sleep(5000);
			infiniteLoopClientClose();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Generates events whose names are single characters 
	 * from the alphabet in uppercase (a, b, c...) at random
	 * intervals between 100 and 5100 milliseconds.
	 * 
	 * Don't forget to call the infiniteLoopClientClose()
	 * method if this one is called
	 */
//	public void infiniteLoopClient(final EventClient client){
	public void infiniteLoopClient(){
		final Runnable action = new Runnable ( ) { 
//			EventClient client;
			
			public void run ( ) {
				Random randomGen = new Random();
				log.debug("parallel thread ");
				while(!infiniteClientStop) {
					//create event with random tag
					Character randomChar = (char)((int)'a'+randomGen.nextInt(25));
					//send it
					try {
						client.send(new Event(""+randomChar));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//sleep for some milliseconds
					int randomMs = 100 + randomGen.nextInt(5000); 
					try {
						Thread.sleep(randomMs);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}	
		};

//		this.client = client;
		infiniteClientDaemon = new Thread(action);
		infiniteClientDaemon.start();
	}
	
	public void infiniteLoopClientClose() throws InterruptedException{
		infiniteClientStop = true;
		infiniteClientDaemon.join();
	}

}
