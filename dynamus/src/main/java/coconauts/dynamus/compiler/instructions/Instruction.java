package coconauts.dynamus.compiler.instructions;

public interface Instruction {
	/*
	 * Executes the instruction
	 */
	public void exec();
}
