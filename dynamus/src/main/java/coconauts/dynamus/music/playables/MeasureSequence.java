package coconauts.dynamus.music.playables;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import coconauts.dynamus.music.MidiPlayer;

public class MeasureSequence extends Playable{
	List<Measure> contents;

	public MeasureSequence(){
		contents = new ArrayList<Measure>();
	}
	
	@Override
	protected void playImpl(MidiPlayer dev) throws PlayException {
		// TODO Auto-generated method stub
		for(Measure measure : contents){
			measure.play(dev);
		}
	}
	
	public void addMeasure(Measure measure){
		contents.add(measure);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return StringUtils.join(contents, " | ")+"||";
	}
	
	/*
	 * TODO access methods
	 */
	
	
}
