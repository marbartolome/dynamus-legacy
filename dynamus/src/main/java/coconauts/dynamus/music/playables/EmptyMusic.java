package coconauts.dynamus.music.playables;

import java.io.Serializable;

import coconauts.dynamus.music.MidiPlayer;

/**
 * This class is simply an empty music structure.
 * It contains nothing, and when played, it will
 * simply start and finish doing nothing in between.
 * @author marbu
 *
 */
public class EmptyMusic extends Playable implements Serializable{

	/*@Override
	public void play(Object... params) {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "<<empty>>";
	}

	@Override
	protected void playImpl(MidiPlayer dev) throws PlayException {
		// TODO Auto-generated method stub
		//nothing to do!
	}
	
	
}
