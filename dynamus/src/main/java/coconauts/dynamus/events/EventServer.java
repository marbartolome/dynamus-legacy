package coconauts.dynamus.events;

import java.net.*;
import java.io.*;
import java.util.*;

import org.apache.log4j.Logger;

import coconauts.dynamus.events.API.Event;
import coconauts.dynamus.events.API.EventClient;
import coconauts.dynamus.events.API.EventOutputStream;
import coconauts.dynamus.events.API.EventSystem;
import coconauts.dynamus.events.EventListener.AlreadyListeningException;

/**
 * It's a Runnable, so to start the daemon use the .start() method.
 * 
 * TODO get rid of all the static elements
 * TODO also, watch out for thread safety. Probably a bunch of fixes will be needed when the time comes. 
 * 
 * @author marbu
 *
 */
public class EventServer implements Serializable{
	/**
	 * 
	 */
	
	//TODO change all logger instances so that they are not static
	//and do this.GetClass().Class, so we can copypaste
	//the same line everywhere
	
	private static final Logger log = Logger.getLogger(EventServer.class);
	
	private static ServerSocket serverSocket;
	private static Socket connToExterior, connFromExterior;
	//private static Socket extClientSocket;
	//input will be used for receiving events
	private static EventInputStream input;
	//output for throwing them
	private static EventOutputStream output;
	
	//a custom event client will be used to thow events from within Dynamus
	//private static Socket innClientSocket;
	
	static Set<EventOutputStream> outToListeners ;
	
	private static boolean running;
	private static Thread daemonThread;
	

	/**
	 * Contains the action code that the server will
	 * execute when running in daemon mode.
	 * @author marbu
	 *
	 */
	private class Daemon extends Thread {
		public void run ( ) {
			log.debug("parallel thread ");
			
			try {
				/*
				innClientSocket = new Socket("localhost", EventSystem.serverPort);
				input = new EventInputStream(innClientSocket.getInputStream());
				PipedInputStream pinput = new PipedInputStream();
				PipedOutputStream poutput = new PipedOutputStream(pinput);
				
				
				innClientSocket = new Socket("localhost", EventSystem.serverPort);
				input = new EventInputStream(innClientSocket.getInputStream());
				output = new EventOutputStream(innClientSocket.getOutputStream());
				
				Socket extConnection = serverSocket.accept();
				EventInputStream inputFromExterior = new EventInputStream(extConnection.getInputStream());
				EventOutputStream outputToExterior = new EventOutputStream(extConnection.getOutputStream());
				
				
				//internal client
				innerClient = new EventClient();
				innClientSocket = serverSocket.accept();
			*/
				
				log.info("waiting for clientsocket to connect on port "+EventSystem.serverPort);
				connFromExterior = serverSocket.accept();
				log.info("client socket accepted");
				input = new EventInputStream (connFromExterior.getInputStream());
			//	output = new EventOutputStream(clientSocket.getOutputStream());
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			running = true;
			//pleaseStop = false;
			
			//listen to events from client 
			log.debug("listening to events in daemon mode");
			Event readEvent;
			readEvent = readEvent();
			while(!readEvent.equals(EventSystem.endOfProgramEvent)){
				//send event to all listeners
				sendToListeners(readEvent);
				log.debug("an event arrived: "+readEvent.getName());
				//read next event
				log.debug("waiting for next event");
				readEvent = readEvent();
				
			}
			//pleaseStop = false;
			running = false;
		}	
	}
	
	public EventServer(){
		try{
			/*DEBUG*/ // System.out.println("blablabla");
			//serverSocket = new ServerSocket(EventSystem.serverPort);
			serverSocket = new ServerSocket();
			serverSocket.setReuseAddress(true);
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		int retryCount = 0;
		while((!serverSocket.isBound())&&(retryCount<5)){
			try{
				log.debug("trying to bind server socket to port (SERVERPORT)");
				serverSocket.bind(new InetSocketAddress(EventSystem.serverPort));
		
			} catch (IOException e) {
				//try again after a while
				try {
					log.debug("binding to SERVERPORT failed " + e);
					//e.printStackTrace();
					retryCount++;
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				/*e.printStackTrace();
				//System.err.println("Event Server error: could not stablish a connection");
			    System.exit(-1);*/
			}
		}
		if(!serverSocket.isBound()){
			log.error("Event Server error: could not stablish a connection (port: "+EventSystem.serverPort+")");
			System.exit(-1);
		}
		else{
			log.debug("serversocket created");
			outToListeners = new HashSet<EventOutputStream>();
			running = false;
			daemonThread = new Daemon();
		}

	}
	
	public void connectToClient(){
		try {
			log.debug("connecting to exterior");
			//connToExterior = new Socket("localhost", EventSystem.clientPort);
			connToExterior = new Socket();
			connToExterior.setReuseAddress(true);
			int retryCount = 0;
			while((!connToExterior.isBound())&&(retryCount<5)){
				try{
					log.debug("trying to connect dynamus to client (on CLIENTPORT)");
					connToExterior.connect(new InetSocketAddress("localhost", EventSystem.clientPort));
			
				} catch (IOException e) {
					//try again after a while
					try {
						e.printStackTrace();
						retryCount++;
						Thread.sleep(10000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					
					/*e.printStackTrace();
					//System.err.println("Event Server error: could not stablish a connection");
				    System.exit(-1);*/
				}
			}
			if(!connToExterior.isBound()){
				log.error("Event Server error: could not stablish a connection (port: "+EventSystem.serverPort+")");
				System.exit(-1);
			}
			
			output = new EventOutputStream(connToExterior.getOutputStream());
			log.debug("connected");
			
		} catch (IOException e) {
			log.error("failed attempt to connect to client, port:"+ EventSystem.clientPort);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void start(){
		/*final Runnable action = new Runnable ( ) { 
			public void run ( ) {
				log.debug("parallel thread ");
				
				//connect to client
				log.info("waiting for clientsocket to connect");
				try {
					clientSocket = serverSocket.accept();

					log.info("client socket accepted");

					input = new EventInputStream (clientSocket.getInputStream());
					output = new EventOutputStream(clientSocket.getOutputStream());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//listen to events from client 
				Event readEvent;
				readEvent = input.readEvent();
				while(readEvent!=endOfProgramEvent){
					//send event to all listeners
					//read next event
					readEvent = input.readEvent();
					//send event to all listeners
					sendToListeners(readEvent);
				}
			}	
		};*/
		
		if (!running){
			log.debug("will start");
			//running = true;
			//Thread newThread = new Thread(action);
			//newThread.start();
			daemonThread.start();
			// do not return until the daemon is well settled and running
			log.debug("waiting for daemon to initialize...");
			while(!running){
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			log.debug("... done! daemon running");
		}
		else{
			//throw new AlreadyRunningException(); soon..
			log.debug("server already running");
		}
	}
	
	public static void stop(){
		log.debug("attempting to stop server");
		if(running){
			//throwEvent(EventSystem.endOfProgramEvent);
			//pleaseStop = true;
			try {
				log.debug("waiting for thread");
				if(!daemonThread.isAlive()){ log.debug("thread is not alive anymore"); }
				daemonThread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				log.debug("couldn't join parallel thread");
				e.printStackTrace();
			}
			running = false;
		}
		
		try {
			log.debug("clossing all conections");
			connToExterior.close();
			connFromExterior.close();
			serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.debug("server stopped sucessfully");
	}
	
	public static void throwEvent(Event ev){
		//write event to the output
		log.debug("throwing event: "+ev);
		output.writeEvent(ev);
		log.debug("sent");
	}
	
	public static void registerListener(EventListener lis){
		log.debug("registering listener: "+lis);
		outToListeners.add(lis.getOutputStream());
	}
	
	public static void unlinkListener(EventListener lis){
		log.debug("unlinking listener: "+lis);
		outToListeners.remove(lis.getOutputStream());
	}
	
	private static void sendToListeners(Event ev){
		log.debug("sending "+ev+" to "+outToListeners.size()+" listeners");
		for (EventOutputStream lOut:outToListeners){
			lOut.writeEvent(ev);
		}
	}

	private static Event readEvent(){
		return input.readEvent();
	}
}
