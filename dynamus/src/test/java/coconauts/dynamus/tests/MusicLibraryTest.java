package coconauts.dynamus.tests;

import static org.junit.Assert.*;

import javax.sound.midi.MidiUnavailableException;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.Test;

import coconauts.dynamus.deprecated.Playable;
import coconauts.dynamus.music.MidiPlayer;
import coconauts.dynamus.music.playables.Measure;
import coconauts.dynamus.music.playables.Note;
import coconauts.dynamus.music.playables.Playable.PlayException;
import coconauts.dynamus.music.playables.wrappers.InstrumentWrapper;
import coconauts.dynamus.music.properties.Defaults;
import coconauts.dynamus.music.properties.DurationValue;
import coconauts.dynamus.music.properties.Instrument;
import coconauts.dynamus.music.properties.Octave;
import coconauts.dynamus.music.properties.PitchClass;
import coconauts.dynamus.music.properties.PitchClass.Name;
import coconauts.dynamus.music.properties.TimeSignature;

public class MusicLibraryTest {

	@Before
	public void setUp(){
		PropertyConfigurator.configure("src/main/java/coconauts/dynamus/logs/log4j.properties");
	}
	
	@Test
	public void testPlayables() throws MidiUnavailableException, PlayException {
		//create some test data structures
		MidiPlayer player = new MidiPlayer();
		
		Measure m = new Measure(new TimeSignature(2, new DurationValue(1, 1)));
		Note c8 = new Note(new PitchClass(Name.C), new Octave(0), new DurationValue(1,8));
		Note d8 = new Note(new PitchClass(Name.D), new Octave(0), new DurationValue(1,8));
		Note c4 = new Note(new PitchClass(Name.C), new Octave(0), new DurationValue(1,4));
		Note d4 = new Note(new PitchClass(Name.D), new Octave(0), new DurationValue(1,4));
		Note e8 = new Note(new PitchClass(Name.E), new Octave(0), new DurationValue(1,8));
		Note f8 = new Note(new PitchClass(Name.F), new Octave(0), new DurationValue(1,8));
		Note g4 = new Note(new PitchClass(Name.G), new Octave(0), new DurationValue(1,4));
		Note a2 = new Note(new PitchClass(Name.A), new Octave(0), new DurationValue(1,2));
		Note g2 = new Note(new PitchClass(Name.G), new Octave(0), new DurationValue(1,2));
		
		m.append(c8);
		m.append(d8);
		m.append(c8);
		m.append(d8);
		m.append(e8);
		m.append(f8);
		m.append(g4);
		m.append(a2);
		m.append(g2);
				
		player.open();
//		try {
//			Thread.sleep(20);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		m.play(player);
//		while (true){
//			m.play(player);
//		}
		m.play(player);
		m.play(player);
		
		
	}
	
//	@Test
	public void testInstrumentWrappers() throws MidiUnavailableException {
		
		//create some test data structures
		Measure m = new Measure(new TimeSignature(2, new DurationValue(1, 4)));
		Note c4 = new Note(new PitchClass(Name.C), new Octave(0), Defaults.DEFAULT_DURATION);
		Note d4 = new Note(new PitchClass(Name.D), new Octave(0), Defaults.DEFAULT_DURATION);
		
		m.append(c4);
		m.append(d4);
		
		MidiPlayer player = new MidiPlayer();
		
		//play on piano
		
		player.open();
		
		try {
			m.play(player);
		} catch (PlayException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//play on guitar
		
		InstrumentWrapper wm = new InstrumentWrapper(Instrument.guitar, m);
		try {
			wm.play(player);
		} catch (PlayException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//play on violin again
		wm = new InstrumentWrapper(Instrument.violin, m);
		try {
			wm.play(player);
		} catch (PlayException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//play on piano again
		try {
			m.play(player);
		} catch (PlayException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		player.close();
		
	}

}
