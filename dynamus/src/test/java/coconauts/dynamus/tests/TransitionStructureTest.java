package coconauts.dynamus.tests;

import coconauts.dynamus.music.playables.Playable;
import coconauts.dynamus.music.playables.Note;
import coconauts.dynamus.compiler.controlStructures.SequentialStructure;
import coconauts.dynamus.compiler.controlStructures.TransitionStructure;


import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.Test;

import coconauts.dynamus.events.EventReference;
import coconauts.dynamus.events.EventServer;
import coconauts.dynamus.events.API.Event;


public class TransitionStructureTest {
	
	//TODO rewrite whole class, as the tested classes
	//have changed quite a bit!
	
//	private TransitionStructure sm;
//	private Playable music;
//	private SoundEngine soundEng;
//	
//	@Before
//	public void setUp() {
//		
//		PropertyConfigurator.configure("src/logs/log4j.properties");
//		
//	    sm = new TransitionStructure();
//	    
//	    SequentialStructure seq = new SequentialStructure();
//	    Note note = new Note("c4");
//	    Note note2 = new Note("c2");
//	    seq.sequentiate(note);
//	    seq.sequentiate(note);
//	    seq.sequentiate(note2);
//	    music = seq;
//	    
//	    soundEng = new SoundEngine();		
//	    
//	}
//
//	
//	
//	/**
//	 * Plays:
//	 * 
//	 * >(one) -eos-> (two) -eos-> x
//	 * 			`d4
//	 * 
//	 * @throws PlayException
//	 */
//	@Test
//	public void testPlayCorrectMachineWOEvents() throws PlayException {
//		//insert some states into the machine
//		sm.addStartState("one", music);
//		sm.addState("two", music);
//		try {
//			sm.addEndOfStateTransition("one", "two", new Note("d4"));
//			sm.addEOSandExitTransition("two");
//		} catch (InvalidStateTransitionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		//play it
//		//SoundEngine.play(sm);
//		sm.play();
//	}
//	
//	
//	/**
//	 * Attempts to play a completely empty state machine.
//	 * Since the machine doesn't have even an start state,
//	 * it should raise an error and not play.
//	 * @throws PlayException 
//	 */
//	@Test (expected= NoStartStateException.class)
//	
//	public void testPlayEmptyMachine() throws PlayException{
//		sm.play();
//	}
//	
//	
//	/**
//	 * Plays a state structure with unreachable states:
//	 * 
//	 * >(one) -eos-> (two) -eos-> x
//	 *          `d4
//	 *  (three)  
//	 * 
//	 * Expected result is the structure playing and
//	 * ending normally, just ignoring the unconnected states.
//	 * 
//	 * @throws PlayException 
//	 */
//	@Test
//	public void testPlaymachineWUnreachableStates() throws PlayException{
//		sm.addStartState("one", music);
//		sm.addState("two", music);
//		sm.addState("three", music);
//		try {
//			sm.addEndOfStateTransition("one", "two", new Note("d4"));
//			sm.addEOSandExitTransition("two");
//		} catch (InvalidStateTransitionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		sm.play();
//	}
//	
//	/**
//	 * Plays a minimal machine with one start state
//	 * and no transitions at all.
//	 * 
//	 * >(one)
//	 * 
//	 * Expected result should be a NoTransitionsException
//	 * being launched after the playback of the music
//	 * 
//	 * @throws PlayException 
//	 */
//	@Test  (expected= NoTransitionsException.class)
//	public void testPlaymachineWOTransitions() throws PlayException{
//		sm.addStartState("one", music);
//		sm.play();
//	}
//	
//	/**
//	 * Creates a machine with two states constantly looping
//	 * one back to the other, and never ending the cycle.
//	 *          ,d4 
//	 * >(one) -eos-> (two) 
//	 *        <-eos- 
//	 *           `e4
//	 * @throws PlayException 
//	 */
//	@Test
//	public void testPlayEndlessMachine() throws PlayException{
//		sm.addStartState("one", music);
//		sm.addState("two", music);
//		try {
//			sm.addEndOfStateTransition("one", "two", new Note("d4"));
//			sm.addEndOfStateTransition("two", "one", new Note("e4"));
//		} catch (InvalidStateTransitionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		sm.play();
//	}
//	
//	/**
//	 * Plays a machine that has no EOS transitions, 
//	 * only event driven transitions:
//	 * 
//	 * >(one) -a-> (two) -b-> x
//	 * 			`d4       `e4
//	 * 
//	 * The 'a' event will be thrown inmediatelly after the machine
//	 * starts playing, so after reaching the end of the first state, 
//	 * the machine should make the transition to the second one instantaneously.
//	 * 
//	 * The 'b' event will be thrown after a wait period of 30 seconds (much more
//	 * than the music's playtime). The second state then, after reaching the end,
//	 * should wait until this event reaches the machine, and only then proceed with
//	 * the final transition.
//	 * 
//	 * @throws PlayException 
//	 */
//	@Test
//	public void testPlayCorrectMachineWOnlyEventTransitions() throws PlayException{
//		//seting up an environment for event management
//		EventsTest eventsEnv = new EventsTest();
//		eventsEnv.setUp();
//		
//		//insert some states into the machine
//		sm.addStartState("one", music);
//		sm.addState("two", music);
//		try {
//			sm.addTransition("one", "two", new EventReference("a"), new Note("d4"));
//			sm.addExitTransition("two", new EventReference("b"), new Note("e4"));
//		} catch (InvalidStateTransitionException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (AmbiguousEventLogicException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		//play it
//		Thread player = new Thread(new Runnable(){
//			public void run() { 
//				try {
//				sm.play();
//				} catch (PlayException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		});
//		player.start();
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		EventServer.throwEvent(new Event("a"));
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		EventServer.throwEvent(new Event("b"));
//		try {
//			player.join();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		eventsEnv.tidyUp();
//	}
//	
	
}
