package coconauts.dynamus.music.properties;

import java.io.Serializable;

public class OctaveModifier implements Serializable{
	int octInterval;
	
	public OctaveModifier(int octRange){
		this.octInterval = octRange;
	}
	
	public int getInterval(){
		return this.octInterval;
	}
	
	public int getAbsoluteRange(){
		return Math.abs(this.octInterval);
	}
	
	public Octave applyToOctave(Octave oct){
		Octave copy = new Octave(oct.getOctaveNumber());
		copy.modify(this);
		return copy;
	}
}
