package coconauts.dynamus.deprecated;

import coconauts.dynamus.music.properties.Dynamics;
import coconauts.dynamus.music.properties.Instrument;
import coconauts.dynamus.music.properties.Tempo;
import coconauts.dynamus.music.properties.Defaults;

public interface Playable {
	//common music properties
	public Instrument instrument = Defaults.DEFAULT_INSTRUMENT;
	public Tempo tempo = Defaults.DEFAULT_TEMPO;
	public Dynamics dynamics = Defaults.DEFAULT_DYNAMICS;
	
	public void play();
}
