package coconauts.dynamus.music.properties;

public interface MusicProperty {
	/*returns the MIDI representation of this property*/
	public int toMidi();
}
