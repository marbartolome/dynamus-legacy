package coconauts.dynamus.music.playables;

import coconauts.dynamus.music.MidiPlayer;
import coconauts.dynamus.music.properties.DurationValue;

public abstract class BasicPlayableElement extends Playable{

	DurationValue duration;
	
	public DurationValue getDurationValue(){
		return duration;
	}
	
}
