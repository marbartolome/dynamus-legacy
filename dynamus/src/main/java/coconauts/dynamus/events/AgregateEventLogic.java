package coconauts.dynamus.events;
import java.io.Serializable;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
//import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class AgregateEventLogic extends EventLogic implements Serializable {
	private static final Logger log = Logger.getLogger(AgregateEventLogic.class);
	
	public enum Operator{
		and, or;
	}
	
	Set<EventLogic> operands;
	Operator op;
	//list of event references that will test against
	//our logic when the .evaluate() method is called.
	//Set<EventReference> evalEvents;
	
	public AgregateEventLogic(Operator op, EventLogic ... operands ){
		this(op, new HashSet<EventLogic>(Arrays.asList(operands)));
	}
	
	public AgregateEventLogic(Operator op, Set<EventLogic> operands){
		super();
		this.op = op;
		this.operands = operands;
	}

	public AgregateEventLogic.Operator getOp(){
		return this.op;
	}
	
	public Set<EventLogic> getOperands(){
		return this.operands;
	}
	/*
	public Set<EventReference> getEvalEvents(){
		return this.evalEvents;
	}

	public void addEvalEvent(EventReference evr){
		boolean added = false;
		if (this.isInLogic(evr)){
			added = evalEvents.add(evr);
		}
	}
	*/
	public boolean isInLogic(EventReference evr){
		log.trace("checking if ER "+evr+" is in logic: "+this);
		boolean result = true;
		Iterator<EventLogic> iterOperands = this.operands.iterator();
		result = false;
		while (iterOperands.hasNext() && !result){
				result = iterOperands.next().isInLogic(evr);
		}	
		log.trace("......"+result);
		return result;
	}
	
	
	/**
	 * Retrieve a set of sets of event references.
	 * The presence of all the expressions in each set
	 * would evaluate the EventLogic as true.
	 * 
	 * For instance, if our logic is:
	 * (A or B) and (D and C)
	 * 
	 * Out truth values set would be:
	 * 1. A, D, C
	 * 2. B, D, C
	 * 
	 * Either of those combinations will evaluate
	 * the event logic to true.
	 * 
	 */
	@Override
	public Set<Set<EventReference>> getTruthValues(){
		
		log.debug("getting truth values for logic: "+this);
		
		Set<Set<EventReference>> outerSet = new HashSet<Set<EventReference>>();
		//Set<EventReference> innerSet = new HashSet<EventReference>();
		//Iterator<EventLogic> iterOperands = this.operands.iterator();
		
		switch(this.op){
			case and: 				
					//foreach operand in our logic
					for (EventLogic operand : this.operands){
						//we get it's truth values
						Set<Set<EventReference>> opTruthValues = operand.getTruthValues();
						//and create a new outer set, that will eventually
						//replace our current (our set of valid sets of ERs)
						Set<Set<EventReference>> newOuterSet = new HashSet<Set<EventReference>>();
						
						//if it is our first operand, and the outer
						//set is still empty
						if (outerSet.isEmpty()){
							//we iterate through all the ER sets that make the operand true
							// (a.k.a, the operand's Truth Values
							for(Set<EventReference> opTruthValue : opTruthValues ){
								//and we create a new ER set (a new TV) containing both
								//the ERs of one inner set and the ERs of one of the
								//inner sets of the operand
								Set<EventReference> newInnerSet = new HashSet<EventReference>();
								newInnerSet.addAll(opTruthValue);
								//and finally add that ER set to our new outer set
								newOuterSet.add(newInnerSet);
							}	
						}
						
						else{ //not the first operand, so the outer set has elements
							//now for each set of ERs present in our outer set
							for (Set<EventReference> innerSet : outerSet){
								//we iterate through all the ER sets that make the operand true
								for(Set<EventReference> opTruthValue : opTruthValues ){
									//and we create a new ER set containing both
									//the ERs of one inner set and the ERs of one of the
									//inner sets of the operand
									Set<EventReference> newInnerSet = new HashSet<EventReference>();
									newInnerSet.addAll(innerSet);
									newInnerSet.addAll(opTruthValue);
									//and finally add that ER set to our new outer set
									newOuterSet.add(newInnerSet);
								}							
							}//after this loop, newOuterSet contains a set of
							//combinations of all the former outerSet ER sets, with
							//all the ER sets that make the operad true
						}
						
						//we make the new outer set replace our former one
						outerSet = newOuterSet;
					}

					break;
			case or:
					//foreach operand in our logic
					for (EventLogic operand : this.operands){
						//we get it's truth values
						Set<Set<EventReference>> opTruthValues = operand.getTruthValues();
						//and just add them to our outer set
						outerSet.addAll(opTruthValues);					
					}
					break;					
		}
		log.debug("TVs calculated: "+Arrays.toString(outerSet.toArray()));
		return outerSet;
	}
	
	
	
	public boolean evaluate(EventReference ... params ){
		//List<EventReference> paramList = Arrays.asList(params);
		log.debug("evaluating logic: "+this +" with events: "+Arrays.toString(params));
		boolean result = true;
		Iterator<EventLogic> iterOperands = this.operands.iterator();
		switch(this.op){
			case and: 				
					while (iterOperands.hasNext() && result){
						result = iterOperands.next().evaluate(params);
					}
					break;
			case or:
					result = false;
					while (iterOperands.hasNext() && !result){
						result = iterOperands.next().evaluate(params);
					}
					break;					
		}
		log.debug("result of eval:" +result);
		return result;
	}
	
	/*public boolean equals(EventLogic evl){
		//Two event logics will be equal if the same event references
		//make them evaluate to true
		return this.getTruthValues().equals(evl.getTruthValues());
	}*/
	
	public String toString() {
		/*
		String str = "";
		str += "( ";
		for (EventLogic o : this.operands){
			str += o + " "+ this.op.name() +" ";
		}
		str += ")";
		return str;*/
		return "(" + StringUtils.join(this.operands.toArray(), " "+this.op.name().toUpperCase()+" ") + ")";
		
	}

	@Override
	public boolean isEmpty() {
		return operands.isEmpty();
	}
}
