//package coconauts.dynamus.deprecated;
//import javax.sound.midi.*;
//
//import coconauts.dynamus.music.MusicStructure.PlayException;
//import coconauts.dynamus.music.properties.Octave;
//import coconauts.dynamus.music.properties.Tempo;
//
//
//import java.io.Serializable;
//import java.util.*;
//
//public class SoundEngine implements Serializable{
//	
//	/* A container structure for all variables
//	 * Shall I need other types of variables in the future than MusicStructure,
//	 * I could change the value type to Object*/
//	
//	static Map<String, Object> varMap ;
//	public static Receiver midiReceiver;
//	
//	//default values
//	public static Tempo defaultTempo = new Tempo(80);
//	public static int defaultDynamics = 100; //TODO change this for it's own Dynamics class
//	public static Instrument defaultInstrument = Instrument.piano;
//	public static DurationValue defaultDuration = new DurationValue((float)1/4);
//	public static Octave defaultOctave = new Octave(4);
//	public static PitchClass defaultPitch = PitchClass.C;
//	
//	
//
//	public SoundEngine() {
//		varMap = new HashMap<String, Object>();
//		try {
//			Synthesizer synth = MidiSystem.getSynthesizer();
//			synth.open();
//			midiReceiver = synth.getReceiver();
//		} catch (MidiUnavailableException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}		
//	}
//	
//
//	public static void storeVar(String varName, Object value){
//		varMap.put(varName, value );
//	}
//	
//	public static Object getVar(String varName){
//		return varMap.get(varName);
//	}
//	
//	public static Receiver getMidiReceiver(){
//		return midiReceiver;
//	}
//	
//	public static void play(Playable music) throws PlayException{
//		music.play(midiReceiver, defaultTempo);
//	}
//	
//	/**
//	 * Deprecated
//	 
//	public static void play(){
//		MusicStructure root = varMap.get("root");
//		root.play(midiReceiver, defaultTempo);
//	}*/
//	
//	/*	This class was only used within parallelmusic, and hence it has ben transferred there, inside the play method
//	public static class Forker implements Runnable{
//		private MusicStructure structure;
//		private Object[] varargs;
//		
//		public Forker(MusicStructure structure, Object ... params){
//			this.structure = structure;
//			this.varargs = params;
//		}
//		
//		public void run(){
//			structure.play(varargs);
//		}
//	}*/
//	
//	/*
//	 * This was the old sound engine. No longer needed now, will go away in next cleanup
//	 * 
//	static NoteSequence sequence;
//	static MidiPlayer player;
//	
//	public static final int ppqValue = 512;
//	public static final int defaultVelocity = 64;
//	public static  final int CUE_POINT_META_MESSAGE = 7;
//	public static final int END_OF_TRACK_META_MESSAGE = 47;
//	
//	
//	
//	public SoundEngine() throws InvalidMidiDataException{	
//		sequence = new NoteSequence(); //new sequence with the default quarter note pulse unit
//		player = new MidiPlayer();
//		varMap = new HashMap<String, MusicStructure>();
//	}
//	
//	
//	public static void storeVar(String varName, MusicStructure var){
//		varMap.put(varName, var );
//	}
//	
//	public static MusicStructure getVar(String varName){
//		return varMap.get(varName);
//	}
//	
//	public static void appendNote(Note note){
//		sequence.appendNote(note);
//	}
//	
//	static public void appendEvent() throws InvalidMidiDataException{
//		sequence.appendEvent();
//	}
//	
//	
//	public void play(){
//		
//		NoteSequence root = (NoteSequence) varMap.get("root");
//		System.out.println("sound engine root: "+root);
//		player.play(root.getMIDIsequence());
//		
//		//root.play(this.player);
//
//	}
//	*/
//	
//	
//	
//	
//	
//	
//	
//	 
//}
