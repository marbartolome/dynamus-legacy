package coconauts.dynamus.demo.inc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import coconauts.dynamus.events.API.Event;
import coconauts.dynamus.events.API.EventClient;
import coconauts.dynamus.events.API.EventSystem;

public class Player implements Runnable{

	public EventClient c;
	public int playerNumber;

	
	public Player(EventClient c, int playerNumber){
		this.c=c;
		this.playerNumber = playerNumber;
	}
	
	@Override
	public void run() {
		System.out.println("Eres el músico número "+playerNumber);
		readFromConsole();
	}
	
	public void sendEvent() throws IOException{
		c.send(new Event("evpy"+playerNumber));
		System.out.println("    enviado evpy"+playerNumber+ " a "+EventSystem.serverPort);
	} 
	
	public void sendEndEvent() throws IOException{
		c.send(new Event("evpy"+playerNumber+"end"));
	}

	public void readFromConsole() {
		String input = "dummy";
		
		while(!"s".equals(input)){
			try{	
			    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			    System.out.println("Pulsa ENTER para cambiar de motivo ('s' para salir).");
			    input = bufferRead.readLine();
			    if ("".equals(input)){
			    	sendEvent();
			    } else if ("s".equals(input)){
			    	sendEndEvent();
			    }
			}
			catch(IOException e)
			{
				e.printStackTrace();
				c.close();
			}
		}
		
	 
		
	 
	}

	

}
