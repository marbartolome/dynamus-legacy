package coconauts.dynamus.tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Random;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.Test;

import coconauts.dynamus.compiler.DynamusVM;
import coconauts.dynamus.compiler.instructions.Instruction;
import coconauts.dynamus.compiler.Compiler;
import coconauts.dynamus.events.API.Event;
import coconauts.dynamus.events.API.EventClient;

import coconauts.dynamus.tests.EventsTest;

public class DynamusTest {

	private static final Logger log = Logger.getLogger(EventsTest.class);
	private Thread infiniteClientDaemon;
	private boolean infiniteClientStop;
	private Thread manualClientDaemon;
	private boolean manualClientStop;
	
	@Before
	public void setUp(){
		PropertyConfigurator.configure("src/main/resources/log4j.properties");
	}
	
	@Test
	public void testNoEvents() {
		
		Compiler comp = new Compiler();
		Instruction instr = comp.compile("src/main/resources/example programs/in_c.dyn");
		
		System.out.println("program compiled, now to run it");
		
		DynamusVM vm = new DynamusVM();
//		EventClient client = new EventClient();
//		vm.getEventServer().connectToClient();
		vm.run(instr);
		
//		client.close();
		
	
	}
	
//	@Test
	public void testEvents(){
		Compiler comp = new Compiler();
		Instruction instr = comp.compile("src/main/resources/example programs/example2.2.dyn");
		
		System.out.println("program compiled, now to run it");
		
		DynamusVM vm = new DynamusVM();
		EventClient client = new EventClient();
		vm.getEventServer().connectToClient();
		
		//set infinite client running on background
		infiniteLoopClient(client);
		
		//play music
		vm.run(instr);
		
		try {
			infiniteLoopClientClose();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		client.close();
		
	}
	
//	@Test
	public void testManualEvents(){
		Compiler comp = new Compiler();
		Instruction instr = comp.compile("src/main/resources/example programs/example2.2.dyn");
		
		System.out.println("program compiled, now to run it");
		
		DynamusVM vm = new DynamusVM();
		EventClient client = new EventClient();
		vm.getEventServer().connectToClient();
		
		//set client as manual
		manualEventClient(client, new Event("a"));
		
		//play music
		vm.run(instr);
		
		try {
			manualEventClientClose();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		client.close();
	
	}
		
	public void manualEventClient(final EventClient client, Event e ){
		final Event eventToSend = e;
		
		final Runnable action = new Runnable ( ) { 
	//		EventClient client;
			
			public void run ( ) {
				Random randomGen = new Random();
				log.debug("parallel thread ");
				while(!manualClientStop){
					//whenever we read a keypress, we send the event
					try {
						int keystroke = System.in.read();
						client.send(eventToSend);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}	
		};
	
	//	this.client = client;
		manualClientDaemon = new Thread(action);
		manualClientDaemon.start();
	}
	
	public void manualEventClientClose() throws InterruptedException{
		manualClientStop = true;
		manualClientDaemon.join();
	}
	
	
	/**
	 * Generates events whose names are single characters 
	 * from the alphabet in lowercase (a, b, c, d, e) at random
	 * intervals between 100 and 5100 milliseconds.
	 * 
	 * Don't forget to call the infiniteLoopClientClose()
	 * method if this one is called
	 */
//	public void infiniteLoopClient(final EventClient client){
	public void infiniteLoopClient(final EventClient client){
		final Runnable action = new Runnable ( ) { 
//			EventClient client;
			
			public void run ( ) {
				Random randomGen = new Random();
				log.debug("parallel thread ");
				while(!infiniteClientStop) {
					//create event with random tag
					Character randomChar = (char)((int)'a'+randomGen.nextInt(5));
					//send it
					try {
						log.debug("sending event "+randomChar);
						client.send(new Event(""+randomChar));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//sleep for some milliseconds
					int randomMs = 100 + randomGen.nextInt(5000); 
					try {
						Thread.sleep(randomMs);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}	
		};

//		this.client = client;
		infiniteClientDaemon = new Thread(action);
		infiniteClientDaemon.start();
	}
	
	public void infiniteLoopClientClose() throws InterruptedException{
		infiniteClientStop = true;
		infiniteClientDaemon.join();
	}

}
