package coconauts.dynamus.compiler.instructions;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;

import coconauts.dynamus.music.MidiPlayer;
import coconauts.dynamus.music.playables.Playable;
import coconauts.dynamus.music.playables.Playable.PlayException;

public class PlayMusicInstruction implements Instruction{
	Playable mus;
	MidiPlayer player;
	
	public PlayMusicInstruction(Playable mus){
		this.mus = mus;
		try {
			this.player = new MidiPlayer();
		} catch (MidiUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public PlayMusicInstruction(Playable mus, MidiPlayer player){
		this.mus = mus;
		this.player = player;
	}

	@Override
	public void exec() {
		try {
			player.open();
		} catch (MidiUnavailableException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			mus.play(player);
		} catch (PlayException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			player.close();
		}
	}
	

}
