package coconauts.dynamus.tests;

import static org.junit.Assert.*;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiUnavailableException;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.Test;

import coconauts.dynamus.music.MidiPlayer;

public class MidiTest {

	@Before
	public void setUp(){
		PropertyConfigurator.configure("src/main/java/coconauts/dynamus/logs/log4j.properties");
	}
	
	
	@Test
	public void testLoadedInstruments(){
		MidiPlayer player;
		try {
			
			player = new MidiPlayer();
			
			player.open();
			
			//check loaded instruments
			Instrument[] instrs = player.getSynth().getLoadedInstruments();
			System.out.println("Default loaded instruments:");
			for (Instrument instr : instrs ){
				System.out.println("\t"+instr.getName() + " - b" + instr.getPatch().getBank() + " p" + instr.getPatch().getProgram());
			}
			
			//check available instruments
			instrs = player.getSynth().getAvailableInstruments();
			System.out.println("Default availaible instruments:");
			for (Instrument instr : instrs ){
				System.out.println("\t"+instr.getName() + " - b" + instr.getPatch().getBank() + " p" + instr.getPatch().getProgram());
			}
			
			//play a note
			System.out.println("playing note on piano");
			player.getMidiChannel(1).noteOn(60, 100);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			player.getMidiChannel(1).noteOff(60, 100);
			
			//change instrument and replay note
			System.out.println("playing note on organ");
			player.getMidiChannel(1).programChange(20);
			player.getMidiChannel(1).noteOn(60, 100);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			player.getMidiChannel(1).noteOff(60, 100);
			
			
			//load default soundbank
			player.getSynth().loadAllInstruments(player.getSynth().getDefaultSoundbank());
			
			//check loaded instruments again
			instrs = player.getSynth().getLoadedInstruments();
			System.out.println("Default loaded instruments:");
			for (Instrument instr : instrs ){
				System.out.println("\t"+instr.getName() + " - b" + instr.getPatch().getBank() + " p" + instr.getPatch().getProgram());
			}
			
			//play notes again
			
			//play a note
			System.out.println("playing note on piano");
			player.getMidiChannel(1).programChange(0, 0);
			player.getMidiChannel(1).noteOn(60, 100);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			player.getMidiChannel(1).noteOff(60, 100);
			
			//change instrument and replay note
			System.out.println("playing note on organ");
			player.getMidiChannel(1).programChange(0, 20);
			player.getMidiChannel(1).noteOn(60, 100);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			player.getMidiChannel(1).noteOff(60, 100);
			
			player.close();
			
		} catch (MidiUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}

}
