package coconauts.dynamus.music.playables;

import javax.sound.midi.*;

import org.apache.log4j.Logger;

import coconauts.dynamus.music.MidiPlayer;
import coconauts.dynamus.music.MidiPlayer.InstrumentLimitReachedException;
import coconauts.dynamus.music.MidiPlayer.NullInstrumentException;
import coconauts.dynamus.music.properties.DurationValue;
import coconauts.dynamus.music.properties.MusicProperty;
import coconauts.dynamus.music.properties.Octave;
import coconauts.dynamus.music.properties.Pitch;
import coconauts.dynamus.music.properties.PitchClass;
import coconauts.dynamus.music.properties.Tempo;

import java.io.Serializable;
import java.lang.Math;


/**
 * 
 * @author marbu
 * A Class that represents a musical note. It 
 * possesses attributes pitch and duration
 * 
 * Attribute pitch is represented by two variables: a pitch class , and an octave number (from 0 to 9)
 * Attribute duration is represented by a durationValue variable
 * 
 * Can return MIDI events for pressing and lifting that note, and the number of beats and ticks representing it's duration.
 */



public class Note extends BasicPlayableElement implements Serializable {

	
	private static Logger log = Logger.getLogger(Note.class);

	
	//PitchClass pitchClass;
	
	/**
	 * Octave number. Valid numbers are from 0 to 9 (are the ones that the underlying MIDI protocol can handle)
	 */
	//Octave octave; 

	Pitch pitch;
	
	/*------Constructors--------*/
	
	/**
	 * Creates a note from the data contained in a text String representing a note's pitch class and duration, given an octave number 
	 *
	public Note( String noteString, int octave ){
		String note = noteString.substring(0,1);
		note = note.toUpperCase();
		int durationIndex = 1 ;
		float durationModifier = 1;
		String accidental = noteString.substring(1,2);
		
		//check for accidentals
		if (accidental.equals("#")) {
			accidental = "s";
			durationIndex = 2;
		}
		else if (accidental.equals("b")){
			accidental = "f";
			durationIndex = 2;
		}
		else{
			accidental="";
		}
		
		
		
		//asign values		
		this.pitchClass = PitchClass.valueOf(note+accidental);
		
		this.duration = new DurationValue( ( (float)1/Integer.parseInt(noteString.substring(durationIndex)) * durationModifier ) );
		
		this.octave = new Octave(octave);
	}*/
	
	/**
	 * Creates a note from the data contained in a text String representing a note's pitch class and duration, given an octave number, with default octave (4)
	 *
	public Note( String noteString ){
		this(noteString, 4);
	}*/
	
	public Note( PitchClass pitchClass, Octave octave, DurationValue duration){
		this( new Pitch(pitchClass, octave), duration);
	}
	
	public Note( Pitch pitch, DurationValue duration){
		super();
		this.pitch = pitch;
		super.duration = duration;
	}
	
	/*------Methods--------*/
	
	public DurationValue getDuration(){
		return duration;
	}
	
	public Pitch getPitch(){
		return pitch;
	}
	
	public Pitch getAbsolutePitch(){
//		log.debug("pitch class: "+pitch.getPitchClass());
//		log.debug("pitch octave: "+pitch.getOctave().getOctaveNumber());
//		log.debug("base octave: "+octave.getOctaveNumber());
		return new Pitch(pitch.getPitchClass(), new Octave(pitch.getOctave().getOctaveNumber()+octave.getOctaveNumber()));
	}
	
	public PitchClass getPitchClass(){
		return pitch.getPitchClass();
	}
	
	public String toString(){
		return new String(pitch.toString() +"-"+ duration.toString());	
	}
	
	
	/*public void play(Object ... params){
		MidiSystem.getSynthesizer().open();
		this.play(MidiSystem.getSynthesizer().getReceiver(), SoundEngine.defaultTempo );
		MidiSystem.getSynthesizer().close();
	}*/
	
	/*public void play(Receiver receiver, Tempo tempo){
		//we may need parameters float startstamp, float timeoffset if playing is not steady
		
		super.isPlaying = true;
	
		//create the midi note on and note off messages
	    ShortMessage noteOnMsg = new ShortMessage();
	    ShortMessage noteOffMsg = new ShortMessage();
	    try{
		   //arguments: status message, channel, pitch and velocity
		   noteOnMsg.setMessage(ShortMessage.NOTE_ON, 0, this.pitchClass.toMidi()+(12*(this.octave.toMidi())), 100); //TODO we are using an arbitrary channel and velocity values. Change this.
		   noteOffMsg.setMessage(ShortMessage.NOTE_OFF, 0, this.pitchClass.toMidi()+(12*(this.octave.toMidi())), 100); //TODO we are using an arbitrary channel and velocity values. Change this.
	    } catch (InvalidMidiDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	    
	    
	    //send note on message
	    receiver.send(noteOnMsg, -1); // -1 means no time stamp. Instead of this we may use in the future startstap+timeoffset+
	    log.debug("we are playing a note: "+this);
	    
	    log.debug("tempo: "+tempo);
	    
	    log.debug("note ms duration: "+tempo.calculateMsDuration(this.duration));
	    
	    //calculate the duration of the note in milliseconds at the given tempo
	    //then sleep for that amount of time
	    try {
			Thread.sleep((long) tempo.calculateMsDuration(this.duration));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    //send note off message		
	    receiver.send(noteOffMsg, -1); // -1 means no time stamp. Instead of this we may use in the future startstap+timeoffset+
	    
	    //we finished playing
	    super.isPlaying = false;
	}*/
		
	@Override
	protected void playImpl(MidiPlayer player) throws PlayException{
		//we may need parameters float startstamp, float timeoffset if playing is not steady
		
		MidiChannel midiChan;
		//get the midi channel we should send our messages to
	    try {
			int channelNum = player.reserveResourcesForInstrument(this.instrument);
			midiChan = player.getMidiChannel(channelNum);
	    } catch (InstrumentLimitReachedException e1) {
			log.error("simultaneous instruments limit reached. Will not play.");
			return;
	    }
	    
	    //set instrument
	    midiChan.programChange(this.instrument.toMidi());
	    
	    /*
	    //create the midi note on and note off messages
	    ShortMessage noteOnMsg = new ShortMessage();
	    ShortMessage noteOffMsg = new ShortMessage();
	    try{
		   //arguments: status message, channel, pitch and velocity
		   noteOnMsg.setMessage(ShortMessage.NOTE_ON, 0, this.pitchClass.toMidi()+(12*(this.octave.toMidi())), 100); //TODO we are using an arbitrary channel and velocity values. Change this.
		   noteOffMsg.setMessage(ShortMessage.NOTE_OFF, 0, this.pitchClass.toMidi()+(12*(this.octave.toMidi())), 100); //TODO we are using an arbitrary channel and velocity values. Change this.
	    } catch (InvalidMidiDataException e) {
			throw new PlayException(e.getMessage());
		}	*/
	    
	    //send 'note on' message
	  //  midiChan.noteOn(getPitchClass().toMidi()+(12*(this.octave.toMidi())), dynamics.toMidi());
	   /// midiChan.noteOn(pitch.toMidi(), dynamics.toMidi());
	    log.debug("NOTE ON: "+this);
	    long time = System.currentTimeMillis();
	    midiChan.noteOn(getAbsolutePitch().toMidi(), dynamics.toMidi());
	    
	    //receiver.send(noteOnMsg, -1); // -1 means no time stamp. Instead of this we may use in the future startstap+timeoffset+
//	    log.debug("we are playing a note: "+this);
//	    
//	    log.debug("tempo: "+tempo);
//	    
//	    log.debug("note ms duration: "+tempo.calculateMsDuration(this.duration));
	    
	    //calculate the duration of the note in milliseconds at the given tempo,
	    //then sleep for that amount of time
	    try {
			Thread.sleep((long) tempo.calculateMsDuration(this.duration));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    //send 'note off' message		
	    //receiver.send(noteOffMsg, -1); // -1 means no time stamp. Instead of this we may use in the future startstap+timeoffset+
	    //midiChan.noteOff(this.getPitchClass().toMidi()+(12*(this.octave.toMidi())), dynamics.toMidi());
	   /// midiChan.noteOff(pitch.toMidi());
	    log.debug("NOTE OFF: "+this);
	    midiChan.noteOff(getAbsolutePitch().toMidi());
	    time = System.currentTimeMillis()-time;
	    log.debug("      MIDI elapsed time:"+time);
	    
	    //release the midi channel so others can use
	    player.releaseResourcesForInstrument(instrument);
	}


	
}
