package coconauts.dynamus.events;
import  java.io.*;

import org.apache.log4j.Logger;

import coconauts.dynamus.events.API.Event;

public class EventInputStream extends FilterInputStream implements Serializable{
	private static final Logger log = Logger.getLogger(EventInputStream.class);
	DataInputStream in;
	
	public EventInputStream(InputStream in){
		super(in);
		this.in = new DataInputStream(in);
	}
	
	public Event readEvent(){
			String name;
			try {
				name = in.readUTF();
				Event ev = new Event(name);
				return ev;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
	}

}
