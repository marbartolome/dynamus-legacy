package coconauts.dynamus.music.properties;

import java.io.Serializable;

import org.apache.log4j.Logger;


public class DurationValue implements Serializable {
	public static class InvalidDurationValueException extends Exception{
		public InvalidDurationValueException(){
			super();
		}
		public InvalidDurationValueException(String message){
			super(message);
		}
	}
	
	private static Logger log = Logger.getLogger(DurationValue.class);
	
	float wholeNoteFraction;
	
	public DurationValue(float fraction){
		this.wholeNoteFraction = fraction;
	}
	
	/**
	 * Instead of supplying the fraction as a float, 
	 * supply it 
	 * @param numerator
	 * @param denominator
	 */
	public DurationValue(int numerator, int denominator){
		this.wholeNoteFraction = (float)numerator / (float) denominator ;
	}
	
	public float getFraction(){
		return this.wholeNoteFraction;
	}
	
	public void add(DurationValue addDur){
		this.wholeNoteFraction += addDur.getFraction();
	}

	public void substract(DurationValue subsDur){
		this.wholeNoteFraction -= subsDur.getFraction();
	}
	
	/**
	 * Returns the number of fragments this duration would
	 * be divided into by the given resolution value.
	 * 
	 * For instance, a duration of a quarter note would
	 * be divided into 4 fragments with a resolution of
	 * 16th notes. 
	 * 
	 * The same quarter note, if divided by an unorthodox
	 * resolution (for instance, 0.003), may give us a number
	 * of fragments with decimals (in this example, 8.333)
	 * @return
	 */
	public float fragmentsWithResolution(DurationValue res){
		return this.getFraction()/res.getFraction();
	}
	
	/**
	 * Returns the number of WHOLE fragments this duration would
	 * be divided into by the given resolution value.
	 * 
	 * For instance, a duration of a quarter note would
	 * be divided into 4 fragments with a resolution of
	 * 16th notes. 
	 * 
	 * If the division results in a non exact number of fragments, 
	 * the result will be rounded to the closest whole value.
	 * (for instance, a result of 2.22 fragments will be rounded to 2,
	 * and a result of 2.85 will be rounded to 3 fragments).
	 * @param res
	 * @return
	 */
	public int wholeFragmentsWithResolution(DurationValue res){
		return Math.round(fragmentsWithResolution(res));
	}
	
//	public float inResolution(DurationValue res){
//		log.debug(this + " in resolution of "+res+" => "+res.getFraction()/this.getFraction());
//		return res.getFraction()/this.getFraction();
//	}
//	
//	public int inClosestWholeResolution(DurationValue res){
//		return (int) inResolution(res);
//	}
	
	public String toString(){
		//TODO, show instead the actual fraction, use apache commons math library
		return Double.toString(this.wholeNoteFraction);
	}
	
	
}
