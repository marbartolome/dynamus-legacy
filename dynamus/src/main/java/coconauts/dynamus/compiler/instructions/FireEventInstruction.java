package coconauts.dynamus.compiler.instructions;

import coconauts.dynamus.compiler.controlStructures.TransitionStructure;
import coconauts.dynamus.events.EventReference;
import coconauts.dynamus.events.EventServer;
import coconauts.dynamus.events.API.Event;

public class FireEventInstruction implements Instruction{
	Event event;
	
	public FireEventInstruction(Event ev){
		event = ev;
	}
	
	public FireEventInstruction(EventReference evr){
		event = new Event(evr);
	}
	
	public void exec(){
		EventServer.throwEvent(event);
	}
}
