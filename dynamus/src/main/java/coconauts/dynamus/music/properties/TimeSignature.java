package coconauts.dynamus.music.properties;

import java.io.Serializable;

public class TimeSignature implements Serializable {
	public int numerator; //beats per measure. Any value.
	public DurationValue denominator; //duration value per beat (beat unit). Valid values: from 0 to 128.
	
	public TimeSignature(int numerator, DurationValue denominator){
		this.numerator = numerator;
		this.denominator = denominator;
	}
	
	/**
	 * Creates the default time signature (4/4)
	 */
	TimeSignature(){
		this(4, new DurationValue((float)1/4));
	}
	
	public String toString(){
		return numerator+"/"+denominator;
	}
	
	/**
	 * Gets the duration of a measure that would
	 * have this time signature
	 * @return
	 */
	public DurationValue measureDuration(){
		return new DurationValue(denominator.getFraction()*numerator);
	}

	
}
