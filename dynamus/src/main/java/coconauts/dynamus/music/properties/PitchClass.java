package coconauts.dynamus.music.properties;

import org.apache.log4j.Logger;

public class PitchClass implements Comparable{
	
	public enum Name {
		C (Value.BsC), Cs (Value.CsDf), Cf(Value.BCf), D(Value.D), Ds(Value.DsEf), Df(Value.CsDf),
		E(Value.EFf), Es(Value.EsF), Ef(Value.DsEf), F(Value.EsF), Fs(Value.FsGf), Ff(Value.EFf),
		G(Value.G), Gs(Value.GsAf), Gf(Value.FsGf), A(Value.A), As(Value.AsBf), Af(Value.GsAf),
		B(Value.BCf), Bs(Value.BsC), Bf(Value.AsBf);
		
		Value value ;
		
		Name(Value val){
			this.value = val;
		}
		
		static public Name fromValue(Value val){
			for(Name name : Name.values()){
				if (name.value.equals(val)){
					return name;
				}
			}
			return null; //this should never happen
		}
	}
	//C, Cs, Cf, D, Ds, Df, E, Es, Ef, F, Fs, Ff, G, Gs, Gf, A, As, Af, B, Bs, Bf;
	
	/**
	 * Actual possible different values of the different pitch classes.
	 * @author marbu
	 *
	 */
	public enum Value{
		BsC, CsDf, D, DsEf, EFf, EsF, FsGf, G, GsAf, A, AsBf, BCf;
		
		public Value nextTone(){
			return Value.values()[(this.ordinal()+2) % 12];
		}
		
		public Value previousTone(){
			return Value.values()[(this.ordinal()-2)  % 12];
		}
		
		public Value nextSemitone(){
			return Value.values()[(this.ordinal()+1) % 12];
		}
		
		public Value previousSemitone(){
			return Value.values()[(this.ordinal()-1) % 12];
		}
		
		public Value pitchAtInterval(){
			//TODO
			return null;
		}
		
		public int toMidi(){
			return this.ordinal();
		}
	}
	
	Name name;
	
	private static Logger log = Logger.getLogger(PitchClass.class);

	public PitchClass(String name){
		this.name = PitchClass.Name.valueOf(name);
	}
	
	public PitchClass(Name name){
		this.name = name;
	}

	public PitchClass nextTone(){
		return new PitchClass(Name.fromValue(name.value.nextTone()));
	}
	
	public PitchClass previousTone(){
		return new PitchClass(Name.fromValue(name.value.previousTone()));
	}
	
	public PitchClass nextSemitone(){
		return new PitchClass(Name.fromValue(name.value.nextSemitone()));
	}
	
	public PitchClass previousSemitone(){
		return new PitchClass(Name.fromValue(name.value.previousSemitone()));
	}
	
	public PitchClass pitchAtInterval(){
		//TODO
		return null;
	}
	
	public int toMidi(){
		return this.name.value.toMidi();
	}

	@Override
	public int compareTo(Object o) {
		//TODO check for type safety
		PitchClass pc = (PitchClass) o;
		return this.name.value.compareTo(pc.name.value);
	}
	
	@Override
	public boolean equals(Object o){
		//TODO check for type safety
		PitchClass pc = (PitchClass) o;
		return this.name.value.equals(pc.name.value);
		
	}
	
	@Override
	public String toString(){
		return name.toString();
		
	}

}
	
	
		/*int midiValue = 0;
		
		switch (this){
		case C: midiValue = 0;	break;
		case Cs: midiValue = 1;	break;
		case Cf: midiValue = 11;	break;
		case D: midiValue = 2;	break;
		case Ds: midiValue = 3;	break;
		case Df: midiValue = 1;	break;
		case E: midiValue = 4;	break;
		case Es: midiValue = 5;	break;
		case Ef: midiValue = 3;	break;
		case F: midiValue = 5;	break;
		case Fs: midiValue = 6;	break;
		case Ff: midiValue = 4;	break;
		case G: midiValue = 7;	break;
		case Gs: midiValue = 8;	break;
		case Gf: midiValue = 6;	break;
		case A: midiValue = 9;	break;
		case As: midiValue = 10;	break;
		case Af: midiValue = 8;	break;
		case B: midiValue = 11;	break;
		case Bs: midiValue = 0;	break;
		case Bf: midiValue = 10;	break;
		default: //this should never happen though
			log.error("no MIDI value for this pitch");
		}
		return midiValue;
		*/
//	}
//}


/*
 * 
 * 	
	//FIXME I don't like all the midi values mingled about with the actual musical structures. This should be changed
	/*C (0), Cs (1), Cf (11), D (2), Ds (3), Df (1), E (4), Es (5), Ef (3), F (5), Fs (6), Ff (4), G (7), Gs (8), Gf (6), A (9), As (10), Af (9), B (11), Bs (0), Bf (10); 
	*
 * 
public enum PitchClass {

	A ("a"), As ("a#"), Af ("ab");
	//B, Bs, Bf, C, Cs, Cf, D, Ds, Df, F, Fs, Ff, G, Gs, Gf 
	
	private String name;	
	
	PitchClass(String name){
		this.name = name;
	}
	
	public String toString(){
		return this.name;
	}
	

	public static PitchClass fromString(String name){
		//TODO
		return new PitchClass("a");
	}
	
}*/
	
	
/*
public class PitchClass{
	static private enum PitchClassEnum { A, B, C, D, E, F, G}
	
	static Map<PitchClassEnum, String> pitchClassMap = new EnumMap<PitchClassEnum, String>(PitchClassEnum.class);
	
	static{
		pitchClassMap.put(PitchClassEnum.A,"a");
		pitchClassMap.put(PitchClassEnum.B,"b");
		//etc
		
	}
	
	private String name;
	
	PitchClass(String name){
		if ( pitchClassMap.containsValue(name) ){
			this.name = name;
		}
		else{
			throw new Exception(name+" is not a valid Pitch Class")
		}
	}
	
	public String toString(){
		return this.name;
	}
	
}*/

/*
public class PitchClass{
	static private enum PitchClassEnum { A, B, C, D, E, F, G}
	
	static Map<String, PitchClassEnum> pitchClassMap = new HashMap<String, PitchClassEnum>();
	
	static{
		pitchClassMap.put("a",PitchClassEnum.A);
		pitchClassMap.put("b",PitchClassEnum.B);
		//etc
		
	}
	
	private String name;
	
	PitchClass(String name){
		if ( pitchClassMap.containsKey(name) ){
			this.name = name;
		}
		else{
			throw new Exception(name+" is not a valid Pitch Class")
		}
	}
	
	public String toString(){
		return this.name;
	}
	
}*/


/*
public class PitchClass{
	//con solo strings (en un set, por ejemplo)
}*/


