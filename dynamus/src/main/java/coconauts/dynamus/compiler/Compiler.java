package coconauts.dynamus.compiler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import coconauts.dynamus.compiler.instructions.Instruction;


public class Compiler {
	private final static Logger log = Logger.getLogger(Compiler.class);
	
	public Compiler(){}
	
	public Instruction compile(String sourceFilePath){
		File inputFile = new File(sourceFilePath); //file containing the Dynamus program code
		return compile(inputFile);
	}
	
	public Instruction compile(File sourceFile){
		
		try {
			FileInputStream fis = new FileInputStream(sourceFile);
			return compile(fis);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public Instruction compile(InputStream stream){
		DynamusLangLexer dynLexer = new DynamusLangLexer( stream );
		DynamusLangParser dynParser = new DynamusLangParser( dynLexer );
		Instruction program = null;
		try {
			program = (Instruction) dynParser.parse().value;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			//If syntax errors were found, exit
			if( dynParser.errorsPresent() || dynLexer.syntaxErrorsPresent()){
				
				System.err.println( "Compilation failed. "+(dynParser.errorCount()+dynLexer.syntaxErrors()) +" errors were found");
				System.exit(1);
			} else {
				System.out.println("Program compiled correctly");
			}
		}
		
		return program;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		PropertyConfigurator.configure("src/main/java/coconauts/dynamus/logs/log4j.properties");
		
		
		if (args.length == 0){
			
			log.error("Usage dynamus <source file>");
		}
		
		else{
			Compiler comp = new Compiler();
			comp.compile(args[0]);
//				try {
//					File inputFile = new File(args[0]); //file containing the Dynamus program code
//					FileInputStream fis;
//					
//					fis = new FileInputStream(inputFile);	
//	
//					DynamusLangLexer dynLexer = new DynamusLangLexer( fis );
//					DynamusLangParser dynParser = new DynamusLangParser( dynLexer );
//	
//					//SoundEngine soundEng = new SoundEngine();
//					Instruction program;
//					try {
//						
//						program = (Instruction) dynParser.parse().value;
//						log.info("Program compiled correctly");
//						
//						//If syntax errors were found, exit
//						if( dynParser.errorsPresent() || dynLexer.syntaxErrorsPresent()){
//							log.error( (dynParser.errorCount()+dynLexer.syntaxErrors()) +" errors were found");
//							System.exit(1);
//						}
//						
//						program.exec();
//					} catch (Exception e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}					
//					
//				} catch (FileNotFoundException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				
				System.exit(0);
				
		}
	}
}
