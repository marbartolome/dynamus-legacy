package coconauts.dynamus.compiler;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import coconauts.dynamus.compiler.instructions.Instruction;
import coconauts.dynamus.events.EventServer;
import coconauts.dynamus.events.API.EventClient;
import coconauts.dynamus.events.API.EventSystem;

public class DynamusVM {
	private final static Logger log = Logger.getLogger(DynamusVM.class);
	EventServer evServer;
	
	
	public DynamusVM(){
		evServer = new EventServer();
		
	}
	
	public EventServer getEventServer(){
		return evServer;
	}
	
	public void run(Instruction i){
		evServer.start();
		i.exec();
		evServer.stop();
	}
	
	public static Properties loadProperties(){
		
		// create and load default properties
		Properties defaultProps = new Properties();
		try {
			FileInputStream in = new FileInputStream("conf.properties");
			defaultProps.load(in);
			in.close();
		} catch(IOException e){
			log.info("Default ./conf.properties file not present");
		}
		
		return defaultProps;
	}

	public static Properties loadProperties(String customPath) throws IOException{
		// create application properties with default
		Properties props = new Properties(loadProperties());
		FileInputStream in = new FileInputStream(customPath);
		props.load(in);
		in.close();
		return props;
	}
	
	public static void config(Properties prop){
		if (prop.containsKey("network.port")){
			System.out.println("Loading from config: network.port="+prop.get("network.port"));
			EventSystem.serverPort = Integer.parseInt((String) prop.get("network.port"));
		}
	}

	
	public static void main(String[] args){
//		PropertyConfigurator.configure("src/main/java/coconauts/dynamus/logs/log4j.properties");
		Properties log4jprop = new Properties();
		try {
			log4jprop.load(ClassLoader.getSystemResourceAsStream("log4j.properties"));
		} catch (IOException e1) {
			System.err.println("Impossible to load log4j config file.");
		}
		PropertyConfigurator.configure(log4jprop);
		Properties prop = loadProperties();
		try {
			if(args.length>1 && args[1].contains("--config")){
				prop = loadProperties(args[1].substring(9));
			}
		} catch (IOException e){
			System.err.println("Impossible to load properties from custom config file: "+args[1]);
		}
		
		config(prop);
		
		Compiler comp = new Compiler();
		Instruction instr = comp.compile(args[0]);
		
		System.out.println("Valid program, running...");
		
		DynamusVM vm = new DynamusVM();
		//EventClient client = new EventClient();
		//vm.getEventServer().connectToClient();
		vm.run(instr);
		
		System.exit(0);
		
		//client.close();
	}
	
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		
//		//PropertyConfigurator.configure("src/main/java/coconauts/dynamus/logs/log4j.properties");
//		
//		Instruction i = ... deserialize from file ...
//		
//		DynamusVM vm = new DynamusVM();
//		vm.exec(i);
//		
//				
//		System.exit(0);
//	}
}
