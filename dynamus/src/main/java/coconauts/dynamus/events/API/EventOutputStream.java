package coconauts.dynamus.events.API;
import java.io.*;

public class EventOutputStream extends FilterOutputStream implements Serializable{
	
	DataOutputStream out;
	
	public EventOutputStream(OutputStream out){
		super(out);
		this.out = new DataOutputStream(out);
	}
	
	public void writeEvent(Event ev){
		try {
			out.writeUTF(ev.getName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
