package coconauts.dynamus.music.playables;


import java.io.Serializable;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Synthesizer;

import org.apache.log4j.Logger;


import coconauts.dynamus.music.MidiPlayer;
import coconauts.dynamus.music.properties.Dynamics;
import coconauts.dynamus.music.properties.Instrument;
import coconauts.dynamus.music.properties.MusicProperty;
import coconauts.dynamus.music.properties.Octave;
import coconauts.dynamus.music.properties.Tempo;
import coconauts.dynamus.music.properties.Defaults;


public abstract class Playable implements Serializable{
	
	private static Logger log = Logger.getLogger(Playable.class);
	
	protected boolean isPlaying;
	
	//common music properties
	protected Instrument instrument;
	protected Tempo tempo;
	protected Dynamics dynamics;
	protected Octave octave;
	
	
	public class PlayException extends Exception {
		public PlayException(){
			super();
		}
		public PlayException(String message){
			super(message);
		}
	}

	public Playable(){
		this.isPlaying = false;
		//initialized to their default values
		//may be changed during the object's lifetime
		this.instrument = Defaults.DEFAULT_INSTRUMENT;
		this.dynamics = Defaults.DEFAULT_DYNAMICS;
		this.tempo = Defaults.DEFAULT_TEMPO;
		this.octave = Defaults.DEFAULT_OCTAVE;
	}
	
	/**Plays the object on the MIDI device.
	 * Device must be opened and ready to receive MIDI messages.
	 * @param dev
	 * @param params
	 * @throws PlayException
	 */
	public void play(MidiPlayer player) throws PlayException{
		if (player.isOpen()){
			this.isPlaying = true;
			try{
				this.playImpl(player);
			}catch(PlayException e){
				throw new PlayException(e.getMessage());
			}finally{
				this.isPlaying = false;
			}
		}
		else {
			throw new PlayException("device unavailaible (not opened)");
		}
	}
	
	/**
	 * Parituclar implementation of the play method, that each
	 * subclass must provide.
	 * @param dev
	 * @param params
	 * @throws PlayException
	 */
	protected abstract void playImpl(MidiPlayer dev) throws PlayException;
	
	/**
	 * Sends midi messages in real time to the given midi receiver.
	 * @param rcv
	 * @param params
	 * @throws PlayException
	 */
	//protected abstract void play(MidiPlayer play, MusicProperty ... params) throws PlayException;
	
	public abstract String toString();
	
	public boolean isPlaying(){
		return this.isPlaying;
	}

	/**
	 * Perfoms some common tasks that are in most cases
	 * wanted be done before and after playing.
	 * @param params
	 * @throws PlayException
	 */
	/*protected void commonPlayWrapper(Object ... params) throws PlayException{
		this.isPlaying = true;
		try {
			Synthesizer synth = MidiSystem.getSynthesizer();
			synth.open();
			play(params);
			synth.close();
		} catch (MidiUnavailableException e) {
			// TODO Auto-generated catch block
			throw new PlayException(e.getMessage());
			//e.printStackTrace();
		}
		finally {
			this.isPlaying = false;
		}
	}*/
	
	/*public void play(Object ... params) throws PlayException{
		try {
			Synthesizer synth = MidiSystem.getSynthesizer();
			synth.open();
			synth.
			play(synth.getReceiver());
			synth.close();
		} catch (MidiUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	

	/**
	 * @return the tempo
	 */
	public Tempo getTempo() {
		return tempo;
	}

	/**
	 * @param tempo the tempo to set
	 */
	public void setTempo(Tempo tempo) {
		this.tempo = tempo;
	}

	/**
	 * @return the dynamics
	 */
	public Dynamics getDynamics() {
		return dynamics;
	}

	/**
	 * @param dynamics the dynamics to set
	 */
	public void setDynamics(Dynamics dynamics) {
		this.dynamics = dynamics;
	}

	/**
	 * @param instr the instrument to set
	 */
	public void setInstrument(Instrument instr){
		this.instrument = instr;
	}
	
	/**
	 * @return the instrument
	 */
	public Instrument getInstrument() {
		return instrument;
	}
	
	/**
	 * @param oct the octave to set
	 */
	public void setOctave(Octave oct){
		this.octave = oct;
	}
	
	/**
	 * @return the octave
	 */
	public Octave getOctave() {
		return octave;
	}
	
	/**
	 * Set any property
	 * @param <P>
	 * @param prop
	 */
	public <P extends MusicProperty> void setProperty(P prop){
		if (prop instanceof Instrument){
			setInstrument((Instrument)prop);
		}
		else if (prop instanceof Octave){
			setOctave((Octave)prop);
		}
		else if (prop instanceof Dynamics){
			setDynamics((Dynamics)prop);
		}
		else if (prop instanceof Tempo){
			setTempo((Tempo)prop);
		}
	}
	
	/**
	 * Get any property
	 * @param <P>
	 * @param prop
	 */
	public MusicProperty getProperty(Class clazz){
		if(clazz.equals(Instrument.class)){
			return getInstrument();
		}
		else if(clazz.equals(Octave.class)){
			return getOctave();
		}
		else if(clazz.equals(Dynamics.class)){
			return getDynamics();
		}
		else if(clazz.equals(Tempo.class)){
			return getTempo();
		}
		else {
			return null;
		}
		
	}
	
}
