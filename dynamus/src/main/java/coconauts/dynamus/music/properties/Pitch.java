package coconauts.dynamus.music.properties;

import java.io.Serializable;

import org.apache.log4j.Logger;

public class Pitch implements MusicProperty, Comparable, Serializable{
	private static final Logger log = Logger.getLogger(Pitch.class);
	PitchClass pClass;
	Octave octave;
	
	public Pitch(PitchClass c, Octave oct){
		this.pClass = c;
		this.octave = oct;
	}
	
	public Pitch calculateNearestOfClass(PitchClass pc){
		log.debug("Calculating nearest "+pc+" to "+this);
		//desired pitch on different octaves
		Pitch above = new Pitch(pc, new Octave(octave.getOctaveNumber()+1));
		Pitch same = new Pitch(pc, new Octave(octave.getOctaveNumber()));
		Pitch below = new Pitch(pc,new Octave(octave.getOctaveNumber()-1));
		
		Pitch[] pitches = {above, same, below};
		int min = 12; //an octave is 12 semitones
		Pitch selected = null;
		
		//we will select the one that is nearest to us
		//(min. number of absolute semitones)
		for(int i=0;i<3;i++){
			Interval interv = new Interval(this, pitches[i]);
			if (interv.getInSemitonesAbsolute()<min){
				min = interv.getInSemitonesAbsolute();
				selected = pitches[i];
			}
		}
		log.debug("    result: "+selected);
		return selected;
	}
	
	public Octave getOctave(){
		return octave;
	}
	
	public PitchClass getPitchClass(){
		return pClass;	
	}
	
	public int toMidi(){
		return this.octave.toMidi()*12+this.pClass.toMidi();
	}


	@Override
	public int compareTo(Object arg) {
		//TODO watch out for type safety
		Pitch p2 = (Pitch) arg;
		if (this.octave.equals(p2.octave)){
			return this.getPitchClass().compareTo(p2.getPitchClass());
		} else {
			return this.getOctave().compareTo(p2.getOctave());
		}
		
	}
	
	public void alterOctave(OctaveModifier mod){
		octave.modify(mod);
	}
	
	@Override
	public String toString(){
		return pClass.toString() + octave.toString();
	}
	
}
