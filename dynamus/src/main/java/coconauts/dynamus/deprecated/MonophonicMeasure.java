//package coconauts.dynamus.deprecated;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import coconauts.dynamus.compiler.controlStructures.SequentialStructure;
//import coconauts.dynamus.compiler.controlStructures.TransitionStructure;
//import coconauts.dynamus.compiler.controlStructures.TransitionStructure.InvalidStateTransitionException;
//import coconauts.dynamus.compiler.instructions.PlayMusicInstruction;
//import coconauts.dynamus.music.MidiPlayer;
//import coconauts.dynamus.music.properties.DurationValue;
//import coconauts.dynamus.music.properties.TimeSignature;
//
//public class MonophonicMeasure extends Measure {
//	
//	/**
//	 * Contents of a cell in our structure.
//	 * It will contain a fragment of a basic playable
//	 * element (note, rest...).
//	 * @author marbu
//	 *
//	 */
//	class MeasureCell {
//		BasicPlayableElement playableElement;
//		PlayableFragmentType fragmentType;
//		
//		public MeasureCell(BasicPlayableElement p, PlayableFragmentType s){
//			playableElement = p;
//			fragmentType = s;
//		}
//		
//		public boolean isStart(){
//			return fragmentType.equals(PlayableFragmentType.start);
//		}
//		
//		public boolean isEnd(){
//			return fragmentType.equals(PlayableFragmentType.end);
//		}
//		
//		public boolean isSustain(){
//			return fragmentType.equals(PlayableFragmentType.sustain);
//		}
//	}
//	
//	public class MeasureFullException extends RuntimeException {}
//	public class VoiceIndexOutOfBoundsException extends RuntimeException {}
//	public class PositionIndexOutOfBoundsException extends RuntimeException {}
//	public class BeatIndexOutOfBoundsException extends RuntimeException {}
//	
//	
//	List<MeasureCell> contents;
//	
//	public MonophonicMeasure(TimeSignature tsig, DurationValue resolution){
//		this.tsig = tsig;
//		this.resolution = resolution;
//		contents = new ArrayList<MeasureCell>();
//	}
//
//	/**
//	 * Converts a playable element to a list of measure cells
//	 * @param element
//	 * @return
//	 */
//	List<MeasureCell> toMeasureCells(BasicPlayableElement element){
//		List<MeasureCell> cellList = new ArrayList<MeasureCell>();
//		int fragments = (int) element.getDurationValue().inResolution(resolution);
//		for(int i=1; i<fragments; i++){
//			cellList.add(new MeasureCell(element, PlayableFragmentType.sustain));
//		}
//		cellList.add(new MeasureCell(element, PlayableFragmentType.end));
//		cellList.add(0, new MeasureCell(element, PlayableFragmentType.start));
//		
//		return cellList;
//	}
//	
//	public boolean isFull(){
//		return contents.size() == tsig.numerator * tsig.denominator.inResolution(resolution);
//	}
//	
//	public void append(BasicPlayableElement element) throws MeasureFullException{
//		if (isFull()){
//			throw new MeasureFullException();
//		}
//		else{
//			List<MeasureCell> cells = toMeasureCells(element);
//				for(MeasureCell cell : cells){
//					if(!isFull()){
//						contents.add(cell);
//					}
//					else {
//						break;
//					}
//				}
//		}
//	}
//	
//	/**
//	 * Gets the slice of the measure from the position
//	 * at index indexUnit*index, to the position resulting
//	 * of adding the 'sliceDuration' from that point.
//	 * 
//	 * For instance, slice(new DurationValue(1/8), 3, new DurationValue(1/2)),
//	 * would give us the contents from the third eight note of the measure 
//	 * to a half note from there.
//	 * @param durationUnit
//	 * @param index
//	 * @return
//	 */
//	public List<MeasureCell> slice(DurationValue indexUnit, int index, DurationValue sliceDuration) throws PositionIndexOutOfBoundsException{
//		//get delimiters
//		int init = ((int) indexUnit.inResolution(resolution))*index;
//		int end = init + (int) sliceDuration.inResolution(resolution);
//		//create slice and copy contents
//		List<MeasureCell> slice = new ArrayList<MeasureCell>();
//		for(int i=init; i<=end; i++){
//			slice.add(contents.get(i));
//		}
//		return slice;
//	}
//	
////	/**
////	 * Get contents of the given beat number.
////	 * @param beatNum
////	 * @return
////	 */
////	public List<BasicPlayableElement> getBeatContents(int beatNum){
////		List<MeasureCell> beatCells = slice(tsig.denominator, beatNum, tsig.denominator);
////		return list-to-notes(beatCells);
////	}
//
//
//	@Override
//	public String toString() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//TODO instead of calculating the list, mantain in updated inside the
//	//object through all it's life. This will make things faster.
//	public List<BasicPlayableElement> toPlayableElementList(){
//		List<BasicPlayableElement> list = new ArrayList<BasicPlayableElement>();
//		for(MeasureCell cell : contents){
//			if(cell.isStart()){
//				list.add(cell.playableElement);
//			}
//		}
//		return list;
//	}
//	
//	@Override
//	public TransitionStructure toTransitionStructure() {
//		// TODO Auto-generated method stub
//		SequentialStructure seq = new SequentialStructure();
//		for(MeasureCell cell : contents){
//			if(cell.isStart()){
//				try {
//					seq.addSingleElement(new PlayMusicInstruction(cell.playableElement));
//				} catch (InvalidStateTransitionException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//		return seq;
//	}
//	
//	@Override
//	protected void playImpl(MidiPlayer dev) throws PlayException {
//		toTransitionStructure().exec();	
//	}
//}
