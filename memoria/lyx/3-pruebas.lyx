#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass book
\use_default_options true
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Pruebas
\end_layout

\begin_layout Section
Pruebas de software
\end_layout

\begin_layout Standard
En todo proceso de ingeniería resulta fundamental realizar pruebas a los
 productos desarrollados, para comprobar que cumplen correctamente con las
 especificaciones, y encontrar posibles errores antes de su paso a producción.
 La ingeniería del software por supuesto no es una excepción, por lo que
 todo proceso de desarrollo incorpora siempre una o varias fases de pruebas.
\end_layout

\begin_layout Standard
En el caso de las metodologías iterativas de desarrollo software, cada iteración
 presenta una fase de pruebas, que nuestro software debe superar con éxito
 antes de proceder a la siguiente fase, que sería el despliegue.
 Estas pruebas, en software, suelen presentarse en varios niveles:
\end_layout

\begin_layout Description
Pruebas
\begin_inset space ~
\end_inset

unitarias Ponen a prueba un componente software de forma individualizada.
 Un componente puede ser cualquier subsistema de nuestra aplicación sobre
 el que consideremos que tiene sentido hacer pruebas: un método, una clase,
 un paquete, o incluso un conjunto de varios de estos elementos que, juntos,
 proporcionan una determinada funcionalidad...
 En cualquier caso, las pruebas comprueban el correcto funcionamiento de
 tan sólo este componente, de forma completmente independiente al resto.
 En ocasiones se las llama también «pruebas de caja blanca» (como contraposición
 a «caja negra»), ya que con ellas no se pretenden probar las funcionalidades
 de nuestro programa, sino las unidades que lo componen (los contenidos
 de la «caja», que por tanto no es negra, sino blanca).
\end_layout

\begin_layout Description
Pruebas
\begin_inset space ~
\end_inset

de
\begin_inset space ~
\end_inset

integración Ponen a prueba la integración de todos nuestros componentes
 unitarios entre sí, a nivel de interfaces.
 Dos componentes pueden ser correctos a nivel unitario, pero ser aún así
 la comunicación entre ellos defectuosa, por lo que son necesarias también
 este tipo de pruebas.
\end_layout

\begin_layout Description
Pruebas
\begin_inset space ~
\end_inset

de
\begin_inset space ~
\end_inset

sistema Ponen a prueba nuesto programa completo, corriendo sobre un entorno
 real con las mismas características que el de producción, como un sistema
 completamente autónomo.
 Se suelen utilizar herramientas específicas que simulan operaciones de
 usuario de forma automatizada.
 A las pruebas de sistema y de integración se les llama también «pruebas
 de caja negra», ya que en ellas se comprueba que la aplicación en sí nos
 da la funcionalidad que se le pide sin tener en cuenta absolutamente la
 manera en la que está implementada (que es una «caja negra»).
\end_layout

\begin_layout Description
Pruebas
\begin_inset space ~
\end_inset

de
\begin_inset space ~
\end_inset

integración
\begin_inset space ~
\end_inset

de
\begin_inset space ~
\end_inset

sistemas Prueban que nuestro programa completo, nuestro sistema, se integra
 adecuadamente con otros sistemas externos, cuando esta situación es relevante
 de acuerdo con nuestras especificaciones.
\end_layout

\begin_layout Description
Pruebas
\begin_inset space ~
\end_inset

de
\begin_inset space ~
\end_inset

aceptación Una vez todos los sistemas se integran correctamente, las pruebas
 de aceptación se encargan de validar si los requisitos iniciales se cumple.
 Este tipo de pruebas en ocasiones es el propio cliente quien las realiza,
 dando así su visto bueno sobre el producto final.
\end_layout

\begin_layout Standard
No todos los desarrollos software requieren realizar todos los tipos de
 pruebas: dependiendo del tipo de sistema que tengamos que construir, nos
 podría bastar con pruebas de nivel unitario, de integración, o de sistema.
\end_layout

\begin_layout Standard
Por lo general, las pruebas de software se intentan automatizar tanto como
 sea posible, gracias a herramientas que así lo permiten.
 Muchas herramientas de gestión del ciclo de desarrollo (herramientas de
 automatización de construcción, de integración continua...) suelen integrarse
 con sistemas de pruebas, de modo que estas se ejecuten de forma automática.
 Esto es especialmente cierto con las pruebas unitarias, que en un ciclo
 de desarrollo automatizado suelen ejecutarse cada vez que un programa es
 compilado, o incorporado al sistema de control de versiones.
 Conforme aumenta el nivel de las pruebas, estas se realizan menos a menudo,
 y suelen incluso realizarse de manera manual.
\end_layout

\begin_layout Standard
Para el caso de Dynamus hemos realizado suites de pruebas unitarias y de
 integración con JUnit para probar nuestros componentes.
 Para las pruebas a nivel de sistema, hemos optado por realizarlas de forma
 manual.
\end_layout

\begin_layout Section
Pruebas unitarias
\end_layout

\begin_layout Standard
Las pruebas unitarias son probablemente las más realizadas en cualquier
 desarrollo software.
 Como sabemos, con este tipo de pruebas se pretende comprobar el funcionamiento
 de nuestros componentes más básicos, de forma aislada.
\end_layout

\begin_layout Standard
Las pruebas unitarias se programan como si fuesen una parte más de nuestro
 código, y de hecho, lo más normal es distribuirlas junto con el programa
 al que deben poner a prueba.
\end_layout

\begin_layout Standard
Por lo general, se construye para cada componente o unidad una clase de
 pruebas, que contiene una serie de métodos destinados a probar el comportamient
o de las diferentes funcionalidades del componente ante diferentes tipos
 de circunstancias.
 En los métodos se debería comprobar que nuestro componente actúa como debiera,
 tanto ante un uso adecuado como inadeacuado del mismo.
 En la figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Métodos-de-nuestra"

\end_inset

 se muestra un ejemplo de los métodos de nuestra clase de pruebas de nuestra
 unidad de máquina de estados.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "breaklines=true,frame=single,language=Java,numbers=left,showstringspaces=false,tabsize=4"
inline false
status open

\begin_layout Plain Layout

public class TransitionStructureTest {
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

	public void testPlayCorrectMachineWOEvents() throws PlayException { ...
 }
\end_layout

\begin_layout Plain Layout

	
\end_layout

\begin_layout Plain Layout

	public void testPlayEmptyMachine() throws PlayException{ ...
 }
\end_layout

\begin_layout Plain Layout

	
\end_layout

\begin_layout Plain Layout

	public void testPlaymachineWUnreachableStates() throws PlayException{ ...
 }
\end_layout

\begin_layout Plain Layout

	
\end_layout

\begin_layout Plain Layout

	public void testPlaymachineWOTransitions() throws PlayException{ ...
 }
\end_layout

\begin_layout Plain Layout

	
\end_layout

\begin_layout Plain Layout

	public void testPlayEndlessMachine() throws PlayException{ ...
 }
\end_layout

\begin_layout Plain Layout

	
\end_layout

\begin_layout Plain Layout

	public void testPlayCorrectMachineWOnlyEventTransitions() throws PlayException{
 ...
 }
\end_layout

\begin_layout Plain Layout

	
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Métodos-de-nuestra"

\end_inset

Métodos de nuestra clase de pruebas para la unidad de estructura de transición
 de estados.
 Podemos ver cómo en cada método probamos un caso distinto de máquina de
 estados, con distintas características (máquina correcta, incorrecta, infinita,
 con eventos, sin eventos...).
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Para realizar estas clases de pruebas se suele utilizar algún framework
 que aporte ciertas facilidades para la construcción, ejecución y automatización
 de nuestras pruebas.
 En nuestro caso, como comentamos en el apartado de medios y materiales,
 hemos utilizado JUnit, el framework de pruebas para Java más popular.
\end_layout

\begin_layout Standard
JUnit nos ofrece la posibilidad de marcar mediante anotaciones nuestros
 métodos de prueba, para integrarlos en el framework.
 Al realizar la integración, podremos ejecutar nuestra clase de pruebas
 como un test de unidad completo, en el que se ejecutarán nuestros métodos,
 de prueba o auxiliares, dependiendo del tipo de anotación con el que estén
 marcados.
 JUnit ofrece varios tipos de anotaciones:
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
@Test Marca un método como método de prueba.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
@Before Marca un método para ser ejecutado antes de cada método de prueba.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
@After Marca un método para ser ejecutado después de cada método de prueba.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
@BeforeClass Marca un método para ser ejecutado una sola vez, antes de comenzar
 a ejecutar cualquier otro tipo de método de la clase.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
@AfterClass Marca un método para ser ejecutado una sola vez, después de
 terminar de ejecutar todos los demás métodos de la clase.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
@Ignore Marca un método para ser ignorado y no ejecutado.
\end_layout

\begin_layout Standard
Cuando se ejecuta una clase de prueba con JUnit, el framework emite un resultado
 indicando si todos nuestros métodos marcados como casos de prueba (@Test)
 han finalizado adecuadaente, o si por el contrario alguno ha fallado.
 Si este último ha sido el caso, nos muestra una traza de la pila de ejecución
 para cada caso de prueba fallido.
\end_layout

\begin_layout Standard
JUnit nos ofrece además una biblioteca con métodos útiles para realizar
 comprobaciones en nuestros tests: las aserciones (o «asserts»).
 Estos son una serie de métodos que nos permiten realizar comprobaciones
 de manera fácil y cómoda, que de otro modo tendrían que realizarse con
 sentencias condicionales y lanzado de excepciones, con más líneas de código.
 Además de esto, las aserciones suponen una ideonea centralización de la
 documentación del tratamiento que deben seguir nuestras unidades con respecto
 a los casos excepcionales.
 En la figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Método-de-aserción"

\end_inset

 mostramos un ejemplo de una aserción y el código Java equivalente al que
 sustituiría.
 JUnit ofrece aserciones de este tipo para comprobar igualdad, desigualdad,
 nulidad, verdad o falsedad, operaciones que vamos a querer realizar de
 forma intensiva durante nuestros tests, y que por tanto nos van a ser de
 utilidad.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout

a)
\lang spanish

\begin_inset listings
lstparams "frame=single"
inline false
status open

\begin_layout Plain Layout

assertEquals(foo, bar);
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
b)
\end_layout

\begin_layout Plain Layout
\begin_inset listings
lstparams "frame=single"
inline false
status open

\begin_layout Plain Layout

if(!foo.equals(bar)){
\end_layout

\begin_layout Plain Layout

	throw new Exception();
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Método-de-aserción"

\end_inset

Método de aserción de JUnit a) y su equivalencia en código Java b).
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Además de las aserciones, JUnit también ofrece, en forma de parámetro para
 las anotaciones de @Test, la posibilidad de declarar un estado de finalización
 esperado (figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "expected-result"

\end_inset

).
 Así, si estamos probando un uso erróneo de nuestra componente, al que este
 debería responder con una excepción, no es necesario que la capturemos
 de forma explícita en nuestro método de prueba, sino que podemos indicarle
 a JUnit que ese es el resultado que esperamos obtener de la ejecución de
 ese método.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset listings
lstparams "frame=single"
inline false
status open

\begin_layout Plain Layout

@Test (expected= NoStartStateException.class)
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Atributo para una anotación de @Test, en el que especificamos que el resultado
 esperado de la ejecución es un estado de excepción (del tipo NoStartStateExcepc
ion).
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "expected-result"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
JUnit se integra con multitud de herramientas de desarrollo, como Eclipse,
 Maven o sistemas de integración continua, por lo que adaptar nuestras clases
 de prueba al framework nos permite sacar provecho de esto.
 La integración de JUnit con Eclipse, por ejemplo, nos permite ejecutar
 nuestras clases de test desde la vista del editor, y ver gráficamente el
 resultado de la ejecución de cada una de ellas (ver figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "eclipse-junit"

\end_inset


\begin_inset Foot
status collapsed

\begin_layout Plain Layout
Fuente de la figura: http://archive.eclipse.org/eclipse/downloads/drops/R-3.0-20040
6251208/eclipse-news-part7-R3.html
\end_layout

\end_inset

).
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename svg_images/28 junit-eclipse.gif
	width 60text%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
Resultado de la ejecución de un test con JUnit en Eclipse.
 En este caso, una de las pruebas se ha ejecutado con éxito, mientras que
 la otra ha fallado.
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "eclipse-junit"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
En cuanto a Maven, como comentamos brevemente en el capítulo dedicado a
 las herramientas de desarrollo, la ejecución de las pruebas unitarias,
 si las detecta en un proyecto, es una fase más en su ciclo de construcción,
 que se realiza tras la correcta compilación del programa, y antes del empaqueta
do.
 Maven realiza esta comprobación porque está construido de acuerdo con una
 filosofía que sostiene que las pruebas no sólo deberían servirnos para
 probar que nuestro código hace lo que debe tras haberlo desarrollado, sino
 también para comprobar que sigue funcionando durante toda la vida de nuestro
 producto.
 
\end_layout

\begin_layout Standard
En efecto, si tenemos un componente en nuestro programa con su clase de
 pruebas actualizada, podremos, si en un futuro modificamos nuestro código,
 utilizar las pruebas para comprobar si hemos «roto» algo involuntariamente,
 lo que haría que las pruebas no pasasen.
 Es por ello que herramientas como Maven las incluyen en su ciclo de trabajo,
 para poder ejecutarlas de forma automática con cada compilación, y así
 asegurar que el programa resultante compilado sea correcto.
\end_layout

\begin_layout Subsection
Suites de pruebas de Dynamus
\end_layout

\begin_layout Standard
Para el caso de Dynamus hemos realizados una serie de pruebas unitarias
 para probar algunos de nuestros componentes de forma aislada:
\end_layout

\begin_layout Description
TransitionStructureTest Prueba el correcto funcionamiento de las estructuras
 de control de Dynamus.
 Involucra clases de 
\family typewriter
coconauts.dynamus.compiler.controlStructures
\family default
, y 
\family typewriter
coconauts.dynamus.events
\family default
.
\end_layout

\begin_layout Description
MusicLibraryTest Prueba el correcto funcionamiento de nuestra biblioteca
 musical.
 Involucra clases del paquete 
\family typewriter
coconauts.dynamus.music
\family default
.
\end_layout

\begin_layout Description
MidiTest Prueba el correcto funcionamiento de 
\family typewriter
coconauts.dynamus.music.MidiPlayer
\family default
.
\end_layout

\begin_layout Description
EventsTest Prueba el correcto funcionamiento de nuestro sistema de eventos
 (clases del paquete 
\family typewriter
coconauts.dynamus.events
\family default
).
\end_layout

\begin_layout Section
Pruebas de sistema
\end_layout

\begin_layout Standard
Como hemos definido antes, las pruebas de sistema, comprueban el correcto
 funcionamiento de nuestro sistema completo, con todos sus componentes integrado
s.
 Su diferencia fundamental con las pruebas unitarias es que las pruebas
 de sistema consideran a nuestro programa completo como una «caja negra»,
 con la que sólo podemos interacturar por las mismas vías que queremos que
 los usuarios finales interactúen con el sistema.
 
\end_layout

\begin_layout Standard
Dependiendo del tipo de sistema con el que estemos tratando, el enfoque
 para realizar las pruebas sistémicas también cambiará.
 Así, si nuestro sistema es un programa Java autocontenido sin más dependencias
 que la propia máquina virtual de Java para su ejecución, podremos escribir
 las pruebas de sistema como clases Java, utilizando incluso un framework
 de pruebas como JUnit, al igual que las pruebas unitarias.
 En cambio, si nuestro programa es una aplicación web empresarial pensada
 para correr en un entorno junto con otros sistemas de los que depende (servidor
 de aplicaciones, servicios web...) las pruebas tendrán que hacerse por medio
 de scripts externos, quizá utilizando herramientas de simulación de acciones
 de usuario.
 
\end_layout

\begin_layout Standard
En Dynamus nos encontramos en el primero de los casos, por lo que nuestras
 pruebas de sistema se realizan dentro de una clase de nuestro paquete de
 tests, 
\family typewriter
DynamusTest
\family default
.
 En las pruebas de esta clase, cargaremos una instancia del intérprete de
 Dynamus, y la pondremos a ejecutar una serie de programas Dynamus de ejemplo.
 Cada uno de estos programas de ejemplo ha sido desarrollado con cada nueva
 iteración de Dynamus, para poder probar que las funcionalidades añadidas
 en la iteración funcionaban correctamente con el sistema al completo (como
 efecto secundario, por tanto, esta serie de programas de ejemplo refleja
 perfectamente la historia de iteraciones y los cambios que cada una ha
 incorporado al sistema).
\end_layout

\begin_layout Standard
Nuestros programas de ejemplo hasta el momento son los siguientes:
\end_layout

\begin_layout Itemize
Ejemplos
\begin_inset space ~
\end_inset

1.x: Seis pruebas en las que se declaran cadenas musicales, que incluyen
 notas con duraciones, alteraciones, y modificadores de octava.
 Asignan las cadenas musicales a variables, y las variables son posteriormente
 referenciadas.
 Se combinan cadenas musicales en estructuras de control no dependientes
 de eventos, como secuencias o paralelizaciones.
\end_layout

\begin_layout Itemize
Ejemplos
\begin_inset space ~
\end_inset

2.x: Dos pruebas en las que se utilizan estructuras de control dependientes
 de eventos, como bucles y estructuras de transición de estados.
 Para realizar pruebas sobre estos elementos hemos tenido que incluir en
 la clase de pruebas una simulación de clientes de eventos, que envíen periodica
mente al servidor eventos fijos o semialeatorios, para poder comprobar el
 correcto avance de nuestro programa.
\end_layout

\begin_layout Standard
Nuestra clase de pruebas se encarga de simular un entorno de ejecución adecuado
 para los programas Dynamus, cargando estos desde disco, y dandoselos a
 una instancia del intérprete de Dynamus, del mismo modo que un usuario
 lo haría desde la línea de comandos.
 Para los tests que así lo han necesitado, era necesario definir en la clase
 de pruebas clientes de eventos, que no difieren de los que un usuario cualquier
a podría crear.
 De este modo, podemos probar la ejecución de nuestro sistema completo de
 forma automatizada, comprobando su validez e integridad.
\end_layout

\end_body
\end_document
