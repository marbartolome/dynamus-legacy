package coconauts.dynamus.events;
import java.io.*;
import java.util.*;

//import coconauts.dynamus.music.StateMachineMusic;

import org.apache.log4j.Logger;

import coconauts.dynamus.events.API.Event;
import coconauts.dynamus.events.API.EventOutputStream;


public class EventListener implements Serializable{
	
		private static Logger log = Logger.getLogger(EventListener.class);
	
		public class AlreadyListeningException extends Exception{
			public AlreadyListeningException(){
				super();
			}
			public AlreadyListeningException(String message){
				super(message);
			}
		}
		
		public class Daemon extends Thread {
			public void run(){
				Event received;
				EventReference receivedRef;
				PipedInputStream pIn;
				
				log.debug("parallel thread ");
				
				try {	
					//init output
					pOut = new PipedOutputStream();
					out = new EventOutputStream(pOut);
					
					//init pipes
					log.debug("piping streams");
					pIn = new PipedInputStream(pOut);
					log.debug("streams piped");
					EventInputStream input = new EventInputStream(pIn);
//					Event ev = input.readEvent(); CAUTION, this may be neccesary!!
//					log.debug("read: " + ev.getName());
					log.debug("moving on in 3....... 2....... 1.......");
					log.debug("registering listener");
					EventServer.registerListener(EventListener.this);
					listeningSetUpComplete = true;
					//receive an event
					log.debug("listening start! condition to stop: "+evlog);
					received = input.readEvent();
					receivedRef = new EventReference(received.getName());
					log.debug("received event: "+receivedRef.getName());
					
					//Add event to logic and evaluate.
					//Repeat read + eval until logic is fulfilled.
					evlog.addEvalEvent(receivedRef);
					while(!evlog.evaluate() && !doStop){
						received = input.readEvent();
						receivedRef = new EventReference(received.getName());
						log.debug("received event: "+receivedRef.getName());
						evlog.addEvalEvent(receivedRef);
					}	
					log.debug("event logic fulfilled. Stopping listening.");
					
					capturedEvents = evlog.getEvalEvents();
					log.debug("captured events: "+capturedEvents);
					EventServer.unlinkListener(EventListener.this);
					evlog = null;
					pOut.close();
					pIn.close();
					out.close();
					input.close();
					listening = false;
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	

		}
	
		EventOutputStream out;
		PipedOutputStream pOut;
		boolean listening;
		boolean listeningSetUpComplete;
		boolean doStop;
		Set<EventReference> capturedEvents;
		EventLogic evlog;
		Daemon listeningDaemon;
		
		public EventListener(){
			//this.pOut = new PipedOutputStream();
			//this.out = new EventOutputStream(pOut);
			this.listening = false;
			this.doStop = false;
			this.capturedEvents = new HashSet<EventReference>(); 
			this.listeningDaemon = new Daemon();
			log.debug("EventListener created");
		}
		
		/**
		 * Return a stream to write events 
		 * into the listener.
		 * @return
		 */
		public EventOutputStream getOutputStream(){
			return out;
		}
		
		public void listen(EventLogic evLog) throws AlreadyListeningException{
			/*
			final Runnable action = new Runnable ( ) { 
				public void run ( ) {
					Event received;
					EventReference receivedRef;
					PipedInputStream pIn;
					
					log.debug("parallel thread ");
					
					try {
						pIn = new PipedInputStream(pOut);
						EventInputStream input = new EventInputStream(pIn);
						//receive an event
						received = input.readEvent();
						receivedRef = new EventReference(received.getName());
						log.debug("received event: "+receivedRef.getName());
						evlog.addEvalEvent(receivedRef);
						while(!evlog.evaluate() && !doStop){
							received = input.readEvent();
							receivedRef = new EventReference(received.getName());
							log.debug("received event: "+receivedRef.getName());
							evlog.addEvalEvent(receivedRef);
						}	
						log.debug("event logic fulfilled. Stopping listening.");
						capturedEvents = evlog.getEvalEvents();
						log.debug("captured events: "+capturedEvents);
						evlog = null;
						listening = false;
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
				} 
			}; */

			if (!listening){
				if(evLog.isEmpty()){
					log.debug("will listen for... nothing, so we're done");
				}
				else {
					log.debug("will listen for "+evLog);
					this.listening = true;
					this.evlog = evLog;
					this.listeningSetUpComplete = false;
					this.listeningDaemon.start();
					while (!listeningSetUpComplete){
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					/*Thread newThread = new Thread(action);
					newThread.start();*/
					log.trace("listening for: "+ evLog);
				}
			}
			else{
				throw new AlreadyListeningException();
			}
			
		}
		/*
		public void send(Event ev){
			out.writeEvent(ev);
		}*/
		
		public boolean listening(){
			return this.listening;
		}
		
		
		public Set<EventReference> capturedEvents(){
			return capturedEvents;
		}
		
		public void stopListening(){
			log.debug("we have been told to stop");
			this.doStop = true;
			try {
				if(listening()){
					this.listeningDaemon.join();
					log.debug("stopped listening");
				}
				else {
					log.debug("  ...we were stopped anyway...");
				}
				listening = false;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/**
		 * Wait until we listen enough events to
		 * fulfill the event logic we're listening for.
		 */
		public Set<EventReference> waitForLogic(){
			log.debug("waiting for logic to be satisfied");
			/*while(this.listening()){
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}*/
			try {
				this.listeningDaemon.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (this.doStop){
				log.debug("listening stopped by external command");
				return null;
			}
			else{
				log.debug("logic satisfied. Captured events: "+this.capturedEvents());
				return this.capturedEvents();
			}
			
			
		}
		
		
		
}
