package coconauts.dynamus.tests;

import org.junit.Test;

import coconauts.dynamus.events.AgregateEventLogic;
import coconauts.dynamus.events.AgregateEventLogic.Operator;
import coconauts.dynamus.events.EventReference;
import static org.junit.Assert.*;

public class TestEventLogic {
	
	@Test
	public void testEvaluateAggregate(){
		AgregateEventLogic ael = new AgregateEventLogic(Operator.or, new EventReference("ev"));
		
		ael.addEvalEvent(new EventReference("ev"));
		
		assertTrue(ael.evaluate());
	}
}
