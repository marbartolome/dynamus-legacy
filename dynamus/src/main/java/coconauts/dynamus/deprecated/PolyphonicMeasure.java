//package coconauts.dynamus.deprecated;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import coconauts.dynamus.compiler.controlStructures.ParallelStructure;
//import coconauts.dynamus.compiler.controlStructures.TransitionStructure;
//import coconauts.dynamus.compiler.controlStructures.TransitionStructure.InvalidStateTransitionException;
//import coconauts.dynamus.music.MidiPlayer;
//
//public class PolyphonicMeasure extends Measure {
//	List<MonophonicMeasure> voices;
//	
//	public PolyphonicMeasure(){
//		voices = new ArrayList<MonophonicMeasure>();
//	}
//	
//	public void addVoice(MonophonicMeasure voice) {
//		voices.add(voice);
//		
//	}
//	
//	@Override
//	protected void playImpl(MidiPlayer dev) throws PlayException {
//		toTransitionStructure().exec();
//	}
//
////	@Override
////	public List<BasicPlayableElement> toPlayableElementList() {
////		// TODO Auto-generated method stub
////		return null;
////	}
//
//	@Override
//	public String toString() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public TransitionStructure toTransitionStructure() {
//		ParallelStructure struct = new ParallelStructure();
//		for (MonophonicMeasure voice : voices){
//			try {
//				struct.addSingleElement(voice.toTransitionStructure());
//			} catch (InvalidStateTransitionException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		return struct;
//	}
//
//
//}
