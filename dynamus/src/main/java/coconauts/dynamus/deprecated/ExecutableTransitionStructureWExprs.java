//package coconauts.dynamus.deprecated;
//
//import java.io.Serializable;
//import java.util.*;
//
//import coconauts.dynamus.music.MidiPlayer;
//import coconauts.dynamus.music.playables.EmptyMusic;
//import coconauts.dynamus.music.playables.Playable;
//import coconauts.dynamus.music.playables.Playable.PlayException;
//import coconauts.dynamus.music.playables.controlStructures.MusicTransitions.State.AmbiguousEventLogicException;
//
//import org.apache.log4j.Logger;
//
//
//import coconauts.dynamus.compiler.controlStructures.ExecutableTransitionStructureWExprs.State.AmbiguousEventLogicExprException;
//import coconauts.dynamus.compiler.instructions.EmptyInstruction;
//import coconauts.dynamus.compiler.instructions.EventLogicExpr;
//import coconauts.dynamus.compiler.instructions.Instruction;
//import coconauts.dynamus.events.*;
//import coconauts.dynamus.events.EventListener;
//import coconauts.dynamus.events.EventListener.AlreadyListeningException;
//import coconauts.dynamus.events.AgregateEventLogic.Operator;
//
//public class ExecutableTransitionStructureWExprs extends Playable implements Serializable{
//	
//	private static Logger log = Logger.getLogger(ExecutableTransitionStructureWExprs.class);
//	
//	public class Transition implements Instruction, Serializable{
//			String to;
//			//EventLogicExpr evl;
//			//MusicStructure musicTransition;
//			Instruction executable;
//			
//			public Transition(String to, Instruction trans ){
//				this.to = to;
//				//this.evl = evl;
//				this.executable = trans;
//			}
//			
//			public Transition(String to){
//				this.to = to;
//				this.executable = new EmptyInstruction();
//				///this.evl= evl;
//			}
//			
//			public String getNext(){
//				return this.to;
//			}
//
//			
//			public void exec(){ //replacement for "play"
//				this.executable.exec();
//			}
//
//			@Override
//			public String toString() {
//				// TODO Auto-generated method stub
//				return null;
//			}
//		}
//
//	//public Object playParams;
//	protected MidiPlayer player;
//	
//	public class State implements Instruction, Serializable, Runnable{
//		
//		public class AmbiguousEventLogicExprException extends Exception{
//			public AmbiguousEventLogicExprException(){
//				super();
//			}
//			public AmbiguousEventLogicExprException(String message){
//				super(message);
//			}
//		}
//		
//		Instruction executable;
//		//Set<Transition> transitions;
//		Map<EventLogicExpr, Set<Transition>> transitions;
//		Set<Transition> EOSTransitions;
//		
//		
//		public State(Instruction exec){
//			this.executable = exec;
//			this.EOSTransitions = new HashSet<Transition>();
//			this.transitions = new HashMap<EventLogicExpr, Set<Transition>>();
//		}
//		
//		public void addTransition(EventLogicExpr evl, Transition trans){
//				if (this.transitions.containsKey(evl)){
//					this.transitions.get(evl).add(trans);
//				}
//				else{
//					Set<Transition> transet = new HashSet<Transition>();
//					transet.add(trans);
//					this.transitions.put(evl, transet);
//				}
//			
//		}
//		
//		public void addTransitions(EventLogicExpr evl, Set<Transition> trans){
//				if (this.transitions.containsKey(evl)){
//					this.transitions.get(evl).addAll(trans);
//				}
//				else{
//					this.transitions.put(evl, trans);
//				}
//		
//		}
//		
//		public void addEOSTransition(Transition trans){
//			this.EOSTransitions.add(trans);
//		}
//		
//		public void EOSTransitions(Set<Transition> trans){
//			this.EOSTransitions = trans;
//		}
//		
//		/**
//		 * Removes the transition trans triggered be evl.
//		 * @param evl
//		 * @param trans
//		 */
//		public void removeTransition(EventLogicExpr evl, Transition trans){
//			if(this.transitions.containsKey(evl)){
//				//gets the set of trans. triggered by evl, removes the one
//				this.transitions.get(evl).remove(trans);
//			}
//		}
//		
//		/**
//		 * Removes the transition triggered by evl whose next state is 'to'.
//		 * @param evl
//		 * @param to
//		 */
//		public void removeTransition(EventLogicExpr evl, String to){
//			if(this.transitions.containsKey(evl)){
//				Transition removeMe = null;
//				for (Transition trans : this.transitions.get(evl)){
//					if(trans.getNext().equals(to)){
//						removeMe = trans;
//						break;
//					}
//				}
//				this.transitions.remove(removeMe);
//			}
//		}
//		
//		public void removeEOSTransition(Transition trans){
//			this.EOSTransitions.remove(trans);
//		}
//		
//		
//		/**
//		 * Removes the EOS transition that goes to 'to'
//		 * @param to
//		 */
//		public void removeEOSTransition(String to){
//			Transition removeMe = null;
//			for (Transition trans : this.EOSTransitions){
//				if(trans.getNext().equals(to)){
//					removeMe = trans;
//					break;
//				}
//			}
//			this.transitions.remove(removeMe);
//		}
//		
//		/**
//		 * Checks if an EventLogicExpr expression is ambiguous
//		 * with the ones present in out transition set.
//		 * The expression is ambiguous if there could be
//		 * an event or set of events that evaluates both
//		 * this expression and any other in our set as true.
//		 * Only one expression can be true under any circumstances.
//		 * @param evl
//		 * @return
//		 */
//		/*public boolean isAmbiguousAddition(EventLogicExpr newevl){
//			Set<Set<EventReference>> newtv = newevl.getTruthValues();
//			Set<EventLogicExpr> transitionEvents = transitions.keySet();
//			boolean isAmb = false;
//			//we check if any of our new event logic's truth values (newtv)
//			//is also a TV of any of our transitions' event logics
//			for(EventLogicExpr transevl : transitionEvents){ //iterate over transitions
//				//get TVs of our transitions' event logic
//				Set<Set<EventReference>> tvals = transevl.getTruthValues();
//				//check if any of them is contained in the new truth values
//				for (Set<EventReference> tval : tvals){
//					if (newtv.contains(tval)){
//						isAmb = true;
//						break;
//					}
//				}
//				//if we already found out that the newevl is ambiguous,
//				//we break our loop, no sense in checking with the
//				//rest of transitions's EvLogs
//				if (isAmb){
//					break;
//				}
//					
//			}
//			return isAmb;
//		}
//		*/
//		public Set<EventLogicExpr> getTransitionsEventLogicExprs(){
//			return this.transitions.keySet();
//			/*Iterator<Transition> transIter = this.transitions.;
//			EventLogicExpr[] stateEvents = new EventReference[this.transitions.size()];
//			int i=0;
//			while (transIter.hasNext()){
//				stateEvents[i] = transIter.next().evl;
//			}
//			return stateEvents;*/
//		}
//		
//		public Set<Transition> getTransitionsForEventLogicExpr(EventLogicExpr evl){
//			return this.transitions.get(evl);
//		}
//		
//		/**
//		 * Returns the set of the evaluated event logic expressions
//		 * @return
//		 */
//		public Set<EventLogic> getTransitionsEventLogics(){
//			Set<EventLogic> evls = new HashSet<EventLogic>();
//			for(EventLogicExpr expr : this.transitions.keySet()){
//				evls.add(expr.eval());
//			}
//			return evls;
//		}
//		
//		
//		/**
//		 * returns the total number of transitions , including
//		 * those triggered by the same event logic that go to different states
//		 *
//		public int numberOfEventTransitions(){
//			Set<EventLogicExpr> transevents = this.transitions.keySet();
//			int counter = 0;
//			for(EventLogicExpr evl : transevents){
//				counter += this.transitions.get(evl).size();
//			}
//			return this.transitions.size();
//		}*/
//		
//		/**
//		 * Returns number of transitions triggered by different events.
//		 * Those triggered by the same EventLogicExpr will be grouped to count
//		 * just as 1.
//		 * 
//		 * In other words, it returns the number of key-value mappings
//		 * of evl <-> set of states.
//		 */
//		public int numberOfTriggeringEventLogicExprs(){
//			return this.transitions.size();
//		}
//		
//		/**
//		 * Number of transitions triggered by a given event logic.
//		 * @param evl
//		 * @return
//		 */
//		public int numberOfEventTransitions(EventLogicExpr evl){
//			return this.transitions.get(evl).size();
//		}
//		
//		/**
//		 * Number of EOS transitions
//		 * @return
//		 */
//		public int numberOfEOSTransitions(){
//			return this.EOSTransitions.size();
//		}
//		
//		/**
//		 * Number of all different ways to trigger a transition
//		 * (an event logic, or the eos)
//		 * @return
//		 */
//		public int numberOfTransitionsTriggers(){
//			return this.numberOfTriggeringEventLogicExprs() + 1 ;
//		}
//		
//		/**
//		 * Total number of possible transitions.
//		 * @return
//		 */
//		public int totalNumberOfTransitions(){
//			Set<EventLogicExpr> transevents = this.transitions.keySet();
//			int counter = 0;
//			for(EventLogicExpr evl : transevents){
//				counter += this.transitions.get(evl).size();
//			}
//			return this.transitions.size()+this.numberOfEOSTransitions();
//		}
//		
//		/**
//		 * Returns a set of the state names that are the goal of all the
//		 * transitions triggered by the evl.
//		 * @param evl
//		 * @return
//		 */
//		public Set<String> nextState(EventLogicExpr evl){
//			Set<String> nextStates = new HashSet<String>();
//			for (Transition trans : this.transitions.get(evl)){
//				nextStates.add(trans.getNext());
//			}
//			return nextStates;
//			/*
//			Iterator<Transition> transIter = this.transitions.iterator();
//			String nextState = null;
//			while (transIter.hasNext() && nextState.equals(null)){
//				Transition trans = transIter.next();
//				if (evl.equals(trans.evl)){
//					nextState = trans.to;
//				}
//			}
//			return nextState;*/
//		}
//		
//		
//		
//		/**
//		 * Returns a set of all the transitions whose event logic
//		 * evaluates to true with the given set of event references.
//		 * @param evrefs
//		 * @return
//		 */
//		public Set<Transition> getTransitions(Set<EventReference> evrefs){
//			Set<Transition> trans = new HashSet<Transition>();
//			Set<EventLogic> elKeys = this.getTransitionsEventLogics();
//			for (EventLogic keyevl : elKeys){
//				if (keyevl.evaluate(evrefs)){
//					trans.addAll(this.transitions.get(keyevl));
//				}
//			}
//			return trans;
//		}
//		
//		/**
//		 * Get a set of all EOS transitions
//		 * @return
//		 */
//		public Set<Transition> getEndOfStateTransitions(){
//			return this.EOSTransitions;
//		}
//		
//		/**
//		 * Return the set of next states we must transit to if 
//		 * the given set of event references is captured.
//		 * @param evrefs
//		 * @return
//		 */
//		public Set<String> nextStates(Set<EventReference> evrefs){
//			Set<String> nextStates = new HashSet<String>();
//			for (Transition trans : this.getTransitions(evrefs)){
//				nextStates.add(trans.getNext());
//			}
//			return nextStates;
//		}
//		
//		public Set<String> nextStatesIfEOS(){
//			Set<String> nextStates = new HashSet<String>();
//			for (Transition trans : this.getEndOfStateTransitions()){
//				nextStates.add(trans.getNext());
//			}
//			return nextStates;
//		}
//		
//		/**
//		 * Performs the pertinent transitions that get triggered
//		 * with the provided set of events, playing the transitions'
//		 * music and executing the goal states in new threads.
//		 * 
//		 * Returns the set of newly created threads.
//		 * @return
//		 * @throws PlayException 
//		 */
//		public Set<Thread> performEventTransitions(Set<EventReference> evrefs) throws PlayException{
//		//public Set<Thread> performEventTransitions(Set<EventReference> evrefs, Object ... params) throws PlayException{	
//			Set<Thread> nextStates = new HashSet<Thread>();
//			//foreach triggered transition
//			for (Transition trans : this.getTransitions(evrefs)){
//				//play transition music
//				trans.exec();
//				//parallel exec of goal state
//				Thread transThread = new Thread(trans.getNext());
//				transThread.start();
//				nextStates.add(transThread); 
//			}
//	
//			return nextStates;
//		}
//		
//		/**
//		 * Performs the pertinent transitions that get triggered
//		 * with the provided set of events, playing the transitions'
//		 * music and executing the goal states in new threads.
//		 * 
//		 * Returns the set of newly created threads.
//		 * @return
//		 * @throws PlayException 
//		 * @throws PlayException 
//		 */
//		//public Set<Thread> performEOSTransitions(Object ... params) throws PlayException{
//		public Set<Thread> performEOSTransitions() throws PlayException{	
//			Set<Thread> nextStates = new HashSet<Thread>();
//			//foreach EOS transition
//			for (Transition trans : this.getEndOfStateTransitions()){
//				//play transition music
//				trans.exec();
//				//parallel exec of goal state
//				Thread transThread = new Thread(trans.getNext());
//				transThread.start();
//				nextStates.add(transThread); 
//			}
//	
//			return nextStates;
//		}
//		
//		/**
//		 * Execution of transitions in parallel
//		 */
//		public void run() {
//			try {
//				
//				//start listener with combined transition logics
//				Set<EventReference> capturedEvents;
//				EventListener listener = new EventListener();
//				listener.listen(new AgregateEventLogic(Operator.or, this.getTransitionsEventLogics()));
//				//execute state's action
//				this.exec();
//				//see if we can transit with the captured events so far
//				capturedEvents = listener.capturedEvents();
//				Set<Thread> transThreads = this.performEventTransitions(capturedEvents);
//				if(!transThreads.isEmpty()){
//					//we performed some transitions, so now we wait 
//					//for the threads to finish
//					for (Thread thread : transThreads){
//						thread.join();
//					}
//				}
//				else{
//					//listenedEvents did not trigger any transitions
//					
//					//if we have EOS transitions, we perform them
//					transThreads = this.performEOSTransitions();
//					if (!transThreads.isEmpty()){
//						//and then wait for the threads to finish
//						for (Thread thread : transThreads){
//							thread.join();
//						}
//					}
//					//if we haven't, we wait until we can transit,
//					else {
//						//wait for events to arrive
//						capturedEvents = listener.waitForLogic();
//						//then transit
//						transThreads = this.performEventTransitions(capturedEvents);
//						//then wait for thread terminations
//						for (Thread thread : transThreads){
//							thread.join();
//						}
//					}
//					
//				}
//			
//			} catch (PlayException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (AlreadyListeningException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		
//
//		/*public Set<Instruction> getTransitionsMusic(Set<EventReference> evrefs){
//			Set<Instruction> musicTrans = new HashSet<Instruction>();
//			for (Transition trans : this.getTransitions(evrefs)){
//				musicTrans.add(trans.musicTransition);
//			}
//			return musicTrans;
//		}*/
//		
//		/*public Set<Instruction> getEOSTransitionsMusic(){
//			Set<Instruction> musicTrans = new HashSet<Instruction>();
//			for (Transition trans : this.getEndOfStateTransitions()){
//				musicTrans.add(trans.musicTransition);
//			}
//			return musicTrans;
//		}*/
//		
//	//	public void playTransitionsMusic(Set<EventReference> evrefs, Object ...  params) throws PlayException{
//		/*public void playTransitionsMusic(Set<EventReference> evrefs) throws PlayException{
//		
//			Set<Transition> transMusics = this.getTransitions(evrefs);
//			for(Transition mus : transMusics){
//				mus.play(player);
//			}
//		}*/
//		
//		/*
//		//public void playEOSTransitionsMusic(Object ... params) throws PlayException{
//		public void playEOSTransitionsMusic() throws PlayException{
//			Set<Transition> transMusics = this.getEndOfStateTransitions();
//			for(Transition mus : transMusics){
//				mus.play(player);
//			}
//		}*/
//		
//		/*@Override
//		public void play(Object... params) throws PlayException {
//			this.music.play(params);
//		}*/
//		/*@Override
//		protected void playImpl(MidiPlayer player) throws PlayException{
//			this.executable.play(player);
//		}*/
//		
//		public void exec(){
//			this.executable.exec();
//		}
//
//		@Override
//		public String toString() {
//			// TODO Auto-generated method stub
//			return null;
//		}
//		
//	}
//	public class InvalidStateTransitionException extends Exception{
//		public InvalidStateTransitionException(){
//			super();
//		}
//		public InvalidStateTransitionException(String message){
//			super(message);
//		}
//	}
//	public class InexistentStateException extends Exception{
//		public InexistentStateException(){
//			super();
//		}
//		public InexistentStateException(String message){
//			super(message);
//		}
//	}
//	public class NoStartTransitionsException extends PlayException{
//		public NoStartTransitionsException(){
//			super();
//		}
//		public NoStartTransitionsException(String message){
//			super(message);
//		}
//	}
//	public class NoTransitionsException extends PlayException{
//		public NoTransitionsException(){
//			super();
//		}
//		public NoTransitionsException(String message){
//			super(message);
//		}
//	}
//	
//	Map<String, State> states;
//	State startState;
//	
//	public ExecutableTransitionStructureWExprs(){
//		this.states = new HashMap<String,State>();
//		this.startState = new State(null); //start state with no music
//	}
//	
//	
//	
//	/**
//	 * Constructor with data to create the start state
//	 */
//	/*public TransitionStructure(String startState, Instruction music){
//		this();
//		addStartState(startState, music);
//	}*/
//	
//	/*public void addStartState(String name, Instruction music){
//		this.addState(name, music);
//		try {
//			this.makeStartState(name);
//		//It should  never enter the catch though
//		} catch (InexistentStateException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}*/
//	
//	public void addState(String name, Instruction music){
//		states.put(name, new State(music));
//	}
//	
//	/**
//	 * Makes the specified state the start state.
//	 * State must previously exist in our machine,
//	 * or a exception will be thrown.
//	 * @param name
//	 * @throws InexistentStateException 
//	 */
//	/*public void makeStartState(String name) throws InexistentStateException{
//		if(states.containsKey(name)){
//			this.startState = name;
//		}
//		else{
//			throw new InexistentStateException();
//		}
//	}*/
//	
//	public void addTransition(String from, String to, EventLogicExpr evl) throws InvalidStateTransitionException, AmbiguousEventLogicException {
//		if (states.containsKey(from) && states.containsKey(to)){
//			states.get(from).addTransition(evl, new Transition(to));
//			//states.get(from).transitions.put(evl, new Transition(to));
//			/*Transition trans = new Transition(to, evl);
//			states.get(from).addTransition(trans);*/
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addTransition(String from, String to, EventLogicExpr evl, Instruction using) throws InvalidStateTransitionException, AmbiguousEventLogicException{
//		if (states.containsKey(from) && states.containsKey(to)){
//			states.get(from).addTransition(evl, new Transition(to, using));
//			//states.get(from).transitions.put(evl, new Transition(to, using));
//			/*Transition trans = new Transition(to, evr, using);
//			states.get(from).addTransition(trans);*/
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addEndOfStateTransition(String from, String to) throws InvalidStateTransitionException{
//		if (states.containsKey(from) && states.containsKey(to)){
//			states.get(from).addEOSTransition(new Transition(to));
//			/*EventReference evr = new EventReference(from+".end");
//			states.get(from).transitions.put(evr, new Transition(to));
//			Transition trans = new Transition(to, evr);
//			states.get(from).addTransition(trans);*/
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addEndOfStateTransition(String from, String to, Instruction using) throws InvalidStateTransitionException{
//		if (states.containsKey(from) && states.containsKey(to)){
//			states.get(from).addEOSTransition(new Transition(to, using));
//			/*EventReference evr = new EventReference(from+".end");
//			states.get(from).transitions.put(evr, new Transition(to, using));
//			Transition trans = new Transition(to, evr, using);
//			states.get(from).addTransition(trans);*/
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	/*
//	public void addExitTransition(String from, EventLogicExpr evl) throws InvalidStateTransitionException, AmbiguousEventLogicExprException{
//		if (states.containsKey(from)){
//			String to = "SMEXIT";
//			states.get(from).addTransition(evl, new Transition(to));
//			//states.get(from).transitions.put(evl, new Transition(to));
//			/*Transition trans = new Transition(to, evl);
//			states.get(from).addTransition(trans);
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addExitTransition(String from, EventLogicExpr evl, Instruction using) throws InvalidStateTransitionException, AmbiguousEventLogicExprException{
//		if (states.containsKey(from)){
//			String to = "SMEXIT";
//			states.get(from).addTransition(evl, new Transition(to, using));
//			//states.get(from).transitions.put(evl, new Transition(to, using));
//			//Transition trans = new Transition(to, evl, using);
//			//states.get(from).addTransition(trans);
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addEOSandExitTransition(String from) throws InvalidStateTransitionException{
//		if (states.containsKey(from)){
//			String to = "SMEXIT";
//			states.get(from).addEOSTransition(new Transition(to));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addEOSandExitTransition(String from, Instruction using) throws InvalidStateTransitionException{
//		if (states.containsKey(from)){
//			String to = "SMEXIT";
//			states.get(from).addEOSTransition(new Transition(to, using));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	*/
//	
//	public void addStartTransition(String to, EventLogicExpr evl) throws AmbiguousEventLogicExprException, InvalidStateTransitionException{
//		if (states.containsKey(to)){
//			startState.addTransition(evl, new Transition(to));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addStartTransition(String to, EventLogicExpr evl, Instruction using) throws InvalidStateTransitionException, AmbiguousEventLogicExprException {
//		if (states.containsKey(to)){
//			startState.addTransition(evl, new Transition(to, using));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	
//	public void addStartEOSTransition(String to) throws InvalidStateTransitionException{
//		if (states.containsKey(to)){
//			startState.addEOSTransition(new Transition(to));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addStartEOSTransition(String to, Instruction using) throws InvalidStateTransitionException, AmbiguousEventLogicExprException {
//		if (states.containsKey(to)){
//			startState.addEOSTransition(new Transition(to, using));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void removeTransition(String from, String to, EventLogicExpr evl){
//		if(states.containsKey(from)&&states.containsKey(to)){
//			states.get(from).removeTransition(evl, to);
//		}
//		else {
//			//do nothing, nothing to remove
//		}
//	}
//	
//	public void removeEOSTransition(String from, String to){
//		if(states.containsKey(from)&&states.containsKey(to)){
//			states.get(from).removeEOSTransition(to);
//		}
//		else {
//			//do nothing, nothing to remove
//		}
//	}
//	
//	public int numberOfStates(){
//		return this.states.size();
//	}
//	
//	/*public boolean hasStartState(){
//		return this.startState!=null;
//	}*/
//	
//	/**
//	 * Returns true if the machine has at least one
//	 * start transition.
//	 */
//	public boolean isPlayable(){
//		return (this.startState.numberOfEOSTransitions()+this.startState.numberOfTriggeringEventLogicExprs()) > 0;
//	}
//	
//	/**
//	 * Returns true if the machine is complete and 
//	 * ready to be used.
//	 * 
//	 * A machine is complete if:
//	 * - there is at least one transition to the SMEXIT state.
//	 * - each state has at least one transition triggered by it's state end event.
//	 * - there are no inconexe states
//	 * 
//	 * Ok, for now we're not needing this
//	 * @return
//	 *
//	public boolean isComplete(){
//		
//	}*/
//
//
//	/**
//	 * Plays the concurrent state machine.
//	 * 
//	 * for each state:
//	 * 		set up listener for combined logic
//	 * 		play state's music
//	 * 		check fulfilled event transitions
//	 * 			yes: launch parallel transitions (playing trans music)
//	 * 			no: if EOS transitions, launch in parallel
//	 * 			wait for child threads to return
//	 * 		if no transitions at all
//	 * 			end
//	 * 		
//	 */
//	/*public void play(Object ... params ) throws PlayException{
//		if(!isPlayable()){
//			throw new NoStartTransitionsException();
//		}
//		
//		this.playParams = params;
//		this.startState.play(playParams);
//		
//		State currentState = this.startState;
//
//	}*/
//	@Override
//	protected void playImpl(MidiPlayer player) throws PlayException{
//		if(!isPlayable()){
//			throw new NoStartTransitionsException();
//		}
//		this.player = player;
//		//this will take care of all
//		Thread originThread = new Thread(startState);
//		//wait for all to end
//		try {
//			originThread.join();
//		} catch (InterruptedException e) {
//			throw new PlayException(e.getMessage());
//		}
//	}
//	
//	/**
//	 * Plays the state machine.
//	 * 
//	 * Starts with the start state. It plays it's music.
//	 * 
//	 * If an appropriate event arrives during the playing 
//	 * (one that triggers a transition),the music execution will
//	 * finalize at a proper point (escape point or at the end of
//	 * the music structure), and then the machine will move to
//	 * execute the next state.
//	 * 
//	 * If the music within the state reaches it's end and no 
//	 * event arrived, or has no event driven transitions:
//	 * 		- if the state has a end-of-state transition, it will
//	 * 		move to the corresponding next state
//	 * 		- if it doesn't it will just stay idling waiting for an
//	 * 		event that triggers a transition.
//	 * 		- if a state has no outgoing transitions watsoever
//	 * 		(event drived or end-of-state)... it will just stay
//	 * 		 stopped forever!
//	 * 			-
//	 * If the machine reaches a SMEXIT state, the paly method will
//	 * correctly finalize and exit.
//	 * 
//	 * Notice that incomplete state machines are allowed. Their execution 
//	 * might be awkward, and probably not what you should expect, but the
//	 * play method will not complain about it. It is up to you to notice
//	 * if you're constructing a state machine step by step.
//	 * 
//	 * You've got at your disposition several helper methods that can 
//	 * inform you about the current architecture of the state machine:
//	 * 		boolean isCorrect()
//	 * 			Machine if correct if:
//	 * 			- there are no unreachable states
//	 * 			- it has at least an exit transition from a reachable state
//	 * 			- each state has a state-end transition
//	 * 
//	 * 		Map<String, boolean> exitTransitions()
//	 * 			Returns a map with all states, and information about if
//	 * 			they have or don't have an exit transition
//	 * 
//	 * 		Map<String, String> endTransitions()
//	 * 			Returns a map with all states, and information about
//	 * 			if they have an end-of-state transition. If they have,
//	 * 			next State is associated with them, if they don't, 
//	 * 			the map content for that key will be null.
//	 * 
//	 * TODO these are not yet implemented, so implement them!
//	 * @throws PlayException 
//	 */
//	/*@Override
//	public void play(Object... params) throws PlayException {
//		if (!this.hasStartState()){
//			throw new NoStartStateException();
//		}
//		
//		super.isPlaying = true;
//		
//		String nextState = this.startState;
//		
//		log.debug("nextState is: "+nextState);
//		
//		while (!nextState.equals("SMEXIT")){
//			State currentState = this.states.get(nextState);
//			log.trace("currently in state: "+currentState);
//			Set<EventLogicExpr> stateTransEvents = currentState.getTransitionsEvents();
//			EventListener evListener = new EventListener();
//			
//			if (stateTransEvents.size()>0){
//				log.trace("state has "+stateTransEvents.size()+" event-driven transitions");
//				EventLogicExpr stateEL = new AgregateEventLogicExpr(Operator.or, stateTransEvents);
//				try {
//					evListener.listen(stateEL);
//					log.trace("listening to transitions logic: "+stateEL);
//				} catch (AlreadyListeningException e) {
//					// we will really never enter here, 
//					//but Java insists...
//					e.printStackTrace();
//				}
//			}
//			
//			//PLAY
//			//currentState.play(evListener, stateEL, params);
//			log.trace("starting to play");
//			currentState.play(params);
//			log.debug("finished playing");
//			//\PLAY
//			
//			//finished playing, now we have to decide what to do
//			//or where to go next
//
//			Transition trans = null;
//			log.debug("number of transitions:"+currentState.numberOfEventTransitions());
//			
//			//if our listener is still listening
//			//or if there was nothing to listen to (no event transitions)
//			if ((evListener.listening()) || (currentState.numberOfEventTransitions()==0)){
//				log.debug("proceeding with EOS transition");
//				//if we have a EOS transition, we take it
//				//and we stop our listener
//				//nextState = currentState.getNextIfEOS();
//				trans = currentState.getEndOfStateTransition();				
//				if (trans!=null){
//					log.debug("found EOS transition, stopping listening");
//					evListener.stopListening(); //weather it was listening or not, doesn't matter to us
//				}
//				//if we don't have it, we'll just have to wait
//				//until our listener gets what he wants
//				//and then move to the corresponding next state
//				else{
//					log.debug("no EOS transition, listening until logic is fulfilled");
//					//nextState = currentState.nextState(evListener.waitForLogic());
//					trans = currentState.getTransition(evListener.waitForLogic());
//				}
//			}
//			//the listener did listen and stopped because it catched
//			//something during the playback of music
//			else{
//					log.debug("event listener catched something, moving to corresponding state");
//					//nextState = currentState.nextState(evListener.capturedEvents());
//					trans = currentState.getTransition(evListener.capturedEvents());
//			}
//			
//			if (trans==null){ //nowhere to go!
//				throw new NoTransitionsException();
//			}
//			else{ //perform transition
//				log.debug("nextState is: "+trans.getNext());
//				log.debug("playing link music and going there");
//				//play linking music
//				trans.play(params);
//				//get next state
//				nextState = trans.getNext();			
//			}
//			
//		//and then we start from the begining with the new state
//		}
//		
//		super.isPlaying = false;
//	
//	}*/
//
//	@Override
//	public String toString() {
//		// TODO Auto-generated method stub
//		return states.toString();
//	}
//	
//	
//	//
//	//
//	// TODO make a new version that doesn't use expression, you probably dont need them!
//	//
//	//
//	//
//	
//}
