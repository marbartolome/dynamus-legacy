package coconauts.dynamus.compiler;

import java.util.HashMap;
import java.util.Map;

public class Environment {
	public class NameUndefinedException extends RuntimeException { }
	Map<String, Object> symbolTable ;
	Environment parentEnv;
	
	public Environment(Environment parentEnv){
		symbolTable = new HashMap<String, Object>();
		this.parentEnv = parentEnv;
	}
	
	public Environment(){
		this(null);
	}

	
	public void define(String id, Object obj){
		this.symbolTable.put(id, obj);
	}
	
	public Object get(String id){
		if (this.symbolTable.containsKey(id)){
			return this.symbolTable.get(id);
		}
		else if(parentEnv!=null){
			return parentEnv.get(id);
		}
		else{
			throw new NameUndefinedException();
		}
	}
	
	public Environment parentEnv(){
		return parentEnv;
	}
	
	/*
	 * 
	 * An alternative implementation that is
	 * type-checking aware.
	 * 
	Map<String, InmutablePair<Object, Class>> symbolTable;
	
	public <T extends Class> void define(String id, Object obj, T type){
		symbolTable.put(id, new InmutablePair(obj, type));
	}
	
	public InmutablePair<Object, Class> get(String id){
		symbolTable.get(id);
	}
	*/
}
