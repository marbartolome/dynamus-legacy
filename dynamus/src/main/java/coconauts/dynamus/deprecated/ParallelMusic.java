//package coconauts.dynamus.deprecated;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
//
//import coconauts.dynamus.music.MusicStructure.PlayException;
//import coconauts.dynamus.music.properties.TimeSignature;
//
//import org.apache.log4j.Logger;
//
//
//public class ParallelMusic extends Playable implements Serializable{
//	
//	/**
//	 * 'Parallel music' is a list of music structures to be executed at the same time, in parallel
//	 * 
//	 */
//	
//	private static final Logger log = Logger.getLogger(ParallelMusic.class);
//	
//	List<Playable> list ;
//	//TODO take TS out, don't want it now
//	TimeSignature timeSignature;
//	
//	
//	public ParallelMusic(){
//		this( new TimeSignature() );
//	}
//
//	public ParallelMusic(TimeSignature signature){
//		this.list = new ArrayList<Playable>();
//		this.timeSignature = signature;
//	}
//	
//	public ParallelMusic(int signatureNumerator, DurationValue signatureDenominator){
//		this.list = new ArrayList<Playable>();
//		this.timeSignature = new TimeSignature( signatureNumerator, signatureDenominator );
//	}
//	
//	
//	public ParallelMusic parallelize(Playable ... structs){
//		for(Playable mSt:structs){
//			this.list.add(mSt);
//		}
//		return this;
//	}
//	
//	/*
//	public Object toMidi(){
//		List<Object> contentsToMidi = new ArrayList<Object>();
//		for (MusicStructure me:list){
//			contentsToMidi.add(me.toMidi());
//		}
//	}*/
//	
//	public void play(Object ... params){
//		//Play each music structure in the list at the same time (or almost...)
//		
//		
//		class Forker implements Runnable{
//			private Playable structure;
//			Object[] varargs;
//			
//			public Forker(Playable structure, Object ... params){
//				this.structure = structure;
//				this.varargs = params;
//			}
//			
//			public void run(){
//				try {
//					structure.play(varargs);
//				} catch (PlayException e) {
//					// TODO I really dont want this exception
//					//being catched.. do something about it!
//					e.printStackTrace();
//				}
//			}
//		}
//		
//		super.isPlaying = true;
//		
//		for (Playable me:list){
//			Runnable runner = new Forker(me, params);
//			Thread newThread = new Thread(runner);
//			newThread.start();
//		}
//		//TODO idea: if we get delays between playbacks with this method, we could try calculating their values and make the new thread's run methods delay themselves
//		//we could pass an argument to the threads, an integer indicating in wich iteration they were summoned, and make them delay in consequence
//		//that way, all of them will start at precisely the same time.
//		
//		//wait until end of execution of contained music structures
//		for (Playable me:list){
//			while (me.isPlaying()){
//				try {
//					Thread.sleep(100);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//		
//		super.isPlaying = false;
//		
//	}
//	
//	public String toString(){
//		String str="";
//		for (Playable e : this.list)
//		    str=str+" "+e;
//		return str;
//		
//		
//	}
//	
//}

