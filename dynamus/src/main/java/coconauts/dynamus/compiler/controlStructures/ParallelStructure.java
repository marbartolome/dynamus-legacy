package coconauts.dynamus.compiler.controlStructures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;

import coconauts.dynamus.compiler.controlStructures.TransitionStructure.InvalidStateTransitionException;
import coconauts.dynamus.compiler.instructions.Instruction;
import coconauts.dynamus.music.MidiPlayer;
import coconauts.dynamus.music.playables.Playable;
import coconauts.dynamus.music.playables.Playable.PlayException;
//import coconauts.dynamus.music.playables.controlStructures.MusicTransitions.InvalidStateTransitionException;

public class ParallelStructure extends TransitionStructure{
	int currentInsertIndex;
	
	public ParallelStructure(Instruction ... instr){
		super();
		currentInsertIndex = 0;
		try {
			addElements(instr);
		} catch (InvalidStateTransitionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String stateNameGenerator(Integer pos){
		if (pos==null){
			return "st#"+currentInsertIndex;
		}
		else{
			return "st#"+pos;
		}
	}
	 
	 public void addSingleElement(Instruction instr) throws InvalidStateTransitionException{
		addState(stateNameGenerator(currentInsertIndex), instr); //TODO this should give and exception if we try to insert a bad state
		//add transition from start state
		addStartEOSTransition(stateNameGenerator(currentInsertIndex));
		currentInsertIndex++;
	 }
	 
	 public void addElements(Instruction ... instrs) throws InvalidStateTransitionException{
		 for(Instruction instr : instrs){
			 addSingleElement(instr);
		 }
	 }
	
	
/*	private String stateNameGenerator(Integer pos){
		if (pos==null){
			return "st#"+currentInsertIndex;
		}
		else{
			return "st#"+pos;
		}
	}
	
	public void addSingleElement(Instruction instr){
		addState(stateNameGenerator(currentInsertIndex), instr); //TODO this should give and exception if we try to insert a bad state
		if(currentInsertIndex==0){
			//this was our very first state, add transition from start state
			addStartEOSTransition(stateNameGenerator(currentInsertIndex));
		}
		else{
			//add transition from last state
			addEndOfStateTransition(stateNameGenerator(currentInsertIndex), stateNameGenerator(currentInsertIndex+1));
		}
		currentInsertIndex++;
	 }
	 
	 public void addElements(Instruction ... instrs){
		 for(Instruction instr : instrs){
			 addSingleElement(instr);
		 }
	 }*/
/*MusicTransitions transitions;
	
	public ParallelStructure(){
		transitions = new MusicTransitions();
	}
	
	public ParallelStructure(Playable ... musics){
		this();
		this.addElements(musics);
	}
	
	public void addElements(Playable ... musics){
		//Map<String, MusicStructure> musicmap =  new HashMap<String, MusicStructure>();
		List<ImmutablePair<String, Playable>> namedMusics = new ArrayList<ImmutablePair<String, Playable>>();
		int i = 0;
		for(Playable music : musics){
			namedMusics.set(i, new ImmutablePair<String, Playable>(null, music));
		}
		//this.addNamedElements(musicmap);
		addNamedElements(namedMusics);
		
	}

	
	 private String stateNameResolver(String customName, int position){
		 if (customName==null){
			return "st"+position; 
		 }
		 else{
			 return customName;
		 }
	 }
	 
	 public void addNamedElements(List<ImmutablePair<String, Playable>> elements){
		 Iterator<ImmutablePair<String, Playable>> iter = elements.iterator();
		 ImmutablePair<String, Playable> currentEntry;
		 try{
			//iterate over all map entries, adding all the states 
			//and transitions from the start state
			int pos = transitions.numberOfStates();
			while(iter.hasNext()){
				//add state
				currentEntry = iter.next();
				pos++;
				transitions.addState(stateNameResolver(currentEntry.getKey(),pos), currentEntry.getValue());
				//add transition
				transitions.addStartEOSTransition(stateNameResolver(currentEntry.getKey(),pos));
			}
		 }catch (InvalidStateTransitionException e){
				e.printStackTrace();
		}
	 
	 }

	
	@Override
	protected void playImpl(MidiPlayer player) throws PlayException{
		transitions.play(player);
	}
	

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return transitions.toString();
	}
*/
}
