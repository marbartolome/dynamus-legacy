#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass book
\use_default_options true
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Dynamus manual
\end_layout

\begin_layout Section
Installation
\end_layout

\begin_layout Standard
The easiest way to install Dynamus is to use the Maven build manager.
 You can find installation instructions for Maven in the 
\begin_inset CommandInset href
LatexCommand href
name "official documentation"
target "http://maven.apache.org/download.cgi#Installation"

\end_inset

.
 If you use a Debian based GNU/Linux distribution you can install it from
 the package manager.
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

$ apt-get install maven2
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Next you'll need to check out the Dynamus source code.
 Dynamus is hosted in a Gitorious git repository, so you'll need to have
 a git client in order to check it out.
 Using apt, you can install it with:
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

$ apt-get install git
\end_layout

\end_inset


\end_layout

\begin_layout Standard
To check out the Dynamus source code, pick a directory and type:
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

$ git clone git://gitorious.org/dynamus/dynamus.git
\end_layout

\end_inset


\end_layout

\begin_layout Standard
That will initialize a git repository and download Dynamus source code into
 it.
 Once it's done, from the root of your working directory type:
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

$ mvn clean install
\end_layout

\end_inset


\end_layout

\begin_layout Standard
This will download all of the dependencies, and compile the Dynamus jarfile.
 After it finishes, you will see a 
\begin_inset Quotes eld
\end_inset

target
\begin_inset Quotes erd
\end_inset

 folder created in the working directory.
 This folder holds the Dynamus installation; you may copy it and place it
 in any location of your filesystem convenient for you.
\end_layout

\begin_layout Standard
In order to compile and run Dynamus you will need a JRE compatible with
 the JavaSE-1.6 specification (OpenJDK’s is recommended).
 If you are user of any Debian based GNU/Linux distribution, you can install
 it from the package manager:
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

$ apt-get install openjdk-6-jre 
\end_layout

\end_inset


\end_layout

\begin_layout Section
Usage
\end_layout

\begin_layout Subsection
Compiling and running your programs
\end_layout

\begin_layout Standard
To run a Dynamus program, simply call java over the Dynamus jarfile, passing
 your dynamus program as an argument:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "breaklines=true"
inline false
status open

\begin_layout Plain Layout

$ java -jar path/to/Dynamus/install/dir/Dynamus-x.x.x.jar path/to/your/dynamus/prog
ram.dyn 
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Doing this will first run the compiler against the program, to check if
 any errors are present.
 If this is the case, the compiler will inform you about them as best as
 it can.
 If the program is correct, it will be sent to the interpreter to be executed.
\end_layout

\begin_layout Subsection
Sending events to Dynamus from an external program
\end_layout

\begin_layout Standard
The interpreter will load a whole instance of the Dynamus engine, including
 the event server.
 The event server runs by default on port 10000, though this can be changed
 via configuration.
 You can send events to the Dynamus event server by using the Dynamus API
 with an external program.
\end_layout

\begin_layout Standard
For now, the event sending API is only availaible for Java.
 Inside the Dynamus jarfile, there is a package named 
\family typewriter
coconauts.dynamus.events.api
\family default
, containing a set of classes implementing the API as a library.
 In order to be able to send events to a Dynamus program from your own Java
 program, you will need to include the Dynamus jarfile in your classpath.
 
\end_layout

\begin_layout Standard
To send an event from your program, the first thing you need to do is to
 instantiate an event client:
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

EventClient client = new EventClient();
\end_layout

\end_inset


\end_layout

\begin_layout Standard
You can use the argumentless constructor, as shown above, that will connect
 to a Dynamus server on the default port, or you can provide a custom port
 to perform the connection as an argument to the constructor.
\end_layout

\begin_layout Standard
Once you have your client object, you can use the send method to communicate
 with Dynamus:
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

client.send(new Event(foo));
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Any events you create carry a message, that can be whatever you want.
 Once you finish sending events, you should close the event client by calling
 the apropriate method:
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

client.close();
\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Configuring Dynamus
\end_layout

\begin_layout Standard
Certain runtime parameters of Dynamus can be changed via configuration.
 The default configuration file for Dynamus lies inside the jarfile, in
 
\family typewriter
/main/resources/config.properties
\family default
.
 After a fresh install, this file contains the default values for several
 Dynamus parameters, like the event server port, or the default tempo to
 use for playback, among others.
 The full list of parameters follows:
\end_layout

\begin_layout Description
network.port The TCP port where the event server will be listening (and where
 the clients must send their event messages).
 Default value is 10000.
\end_layout

\begin_layout Description
music.measure.granularity The duration granularity to be used by the measures.
 This value will determine the minimun possible duration value to be used
 in your music strings.
 The default value is 1/128.
\end_layout

\begin_layout Description
music.default.tempo The tempo in beats per minute (BPM) at which to play music
 elements, if it's unspecified in the program.
 The default value is 98 BPM.
\end_layout

\begin_layout Description
music.default.instrument The instrument to use to play music elements unless
 otherwise specified.
 The default instrument is piano.
\end_layout

\begin_layout Description
music.default.dynamics The default volume at which to play music elements
 unless otherwise specified.
 The defailt is mezzo forte.
\end_layout

\begin_layout Standard
You can however override these values by providing your own configuration
 file.
 You can place a config.properties file in the same directory where the Dynamus
 jarfile sits, and it will be picked up by the program automatically.
 If you prefer to use a file at a different path, or with a custom name,
 you can indicate this to Dynamus with a runtime argument when you execute
 it:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "breaklines=true"
inline false
status open

\begin_layout Plain Layout

$ java -jar Dynamus-x.x.x.jar --config=/path/to/custom/configuration.properties
 mymusic.dyn 
\end_layout

\end_inset


\end_layout

\begin_layout Standard
If you do this, the values in the file provided as a runtime argument will
 override both those of the config.properties outsite the jarfile, and the
 one inside it.
\end_layout

\end_body
\end_document
