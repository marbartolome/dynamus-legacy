package coconauts.dynamus.music.properties;

import java.io.Serializable;

import coconauts.dynamus.music.playables.Note;
import coconauts.dynamus.music.playables.Rest;

public class Tempo implements MusicProperty, Serializable{
	//add variables: beats per second and beat unit
	DurationValue beatUnit;
	int beatsPerMinute;
	
	Tempo(DurationValue beatUnit, int beatsPerMinute){
		this.beatUnit = beatUnit;
		this.beatsPerMinute = beatsPerMinute;
	}
	
	/**
	 * Considers the cuarter note as the default beat unit
	 * @param beatsPerSecond
	 */
	public Tempo(int beatsPerSecond){
		this(new DurationValue((float)1/4), beatsPerSecond);
	}
	
	public DurationValue getBeatUnit(){
		return this.beatUnit;
	}
	
	public int getBeatsPerSecond(){
		return this.beatsPerMinute;
	}
	
	public float getMsPerBeatUnit(){
		return (float) ((1/((float)this.beatsPerMinute/60))*Math.pow(10,3));
	}
	
	
	public float calculateMsDuration(DurationValue noteDuration){
		
		return ( noteDuration.getFraction() / this.beatUnit.getFraction() ) * this.getMsPerBeatUnit();
		
		//return (this.beatUnit.getFraction()/noteDuration.getFraction()*this.getMsPerBeatUnit());
	}
	
	public float calculateMsDuration(Note note){
		return this.calculateMsDuration(note.getDuration());
	}
	
	public float calculateMsDuration(Rest rest){
		return this.calculateMsDuration(rest.getDuration());
	}
	
	public String toString(){
		return new String(this.beatUnit + " at "+this.beatsPerMinute+"/sec");
	}

	@Override
	public int toMidi() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
