package coconauts.dynamus.logs;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.or.ObjectRenderer;

import coconauts.dynamus.compiler.DynamusLangParserSym;
import java_cup.runtime.Symbol;


public class SymbolRenderer implements ObjectRenderer {
	
	public class ReversedSymbols{
		
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		public ReversedSymbols(){
			Field[] symbolFields = DynamusLangParserSym.class.getDeclaredFields();
			for(Field field:symbolFields){
				try {
					//field.get(null);
					String name = field.getName();
					int value = field.getInt(null);
					map.put(value, name);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		public String getString(int value){
			return map.get(value);
		}
	}
	
	private ReversedSymbols revSymbols = new ReversedSymbols();
	
	@Override
	public String doRender(Object symbol){
		String name = new String();
        if(symbol instanceof Symbol) {	
        	int intValue = ((Symbol)symbol).sym;
        	name = revSymbols.getString(intValue);
        }     
        
        return "received new Symbol: "+name+" "+((Symbol)symbol).value;
	}
/*
    @Override
    public String doRender(Object symbol) {
    	String name = new String();
        if(symbol instanceof Symbol) {	
        	switch(((Symbol)symbol).sym){
            case 0: name = "EOF";     break;
            case 1: name = "error";    break;
            case 2: name = "VARNAME";    break;
            case 3: name = "INNERVARNAME";    break;
            case 4: name = "NOTE";    break;
            case 5: name = "REST";	break;
            case 6: name = "OCTAVE_SELECT";    break;
            case 7: name = "EVENT";    break;
            case 8: name = "ASIGANTION";    break;
            case 9: name = "LEFTBRACE";    break;
            case 10: name = "RIGHTBRACE";    break;
            case 11: name = "ILLEGAL";    break;
            case 12: name = "ADDITION";	break;
            default: //this should never happen though
            try {
                throw new Exception();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        	} //switch
        } //if
        return "received new Symbol: "+name+" "+((Symbol)symbol).value;
    }
    */
}
    

