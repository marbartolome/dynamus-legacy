package coconauts.dynamus.music.playables;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import coconauts.dynamus.compiler.controlStructures.ControlStructure;
import coconauts.dynamus.compiler.controlStructures.ParallelStructure;
import coconauts.dynamus.compiler.controlStructures.SequentialStructure;
import coconauts.dynamus.compiler.controlStructures.TransitionStructure;
import coconauts.dynamus.compiler.controlStructures.TransitionStructure.InvalidStateTransitionException;
import coconauts.dynamus.compiler.instructions.ExecControlStructureInstruction;
import coconauts.dynamus.compiler.instructions.Instruction;
import coconauts.dynamus.compiler.instructions.PlayMusicInstruction;
import coconauts.dynamus.music.MidiPlayer;
import coconauts.dynamus.music.playables.wrappers.InstrumentWrapper;
import coconauts.dynamus.music.properties.Defaults;
import coconauts.dynamus.music.properties.DurationValue;
import coconauts.dynamus.music.properties.Pitch;
import coconauts.dynamus.music.properties.TimeSignature;

public class Measure extends Playable{
	private static final Logger log = Logger.getLogger(Measure.class);
//	enum PlayableFragmentType {start, sustain, end; }
//	
//	//TODO REDO
//	// instead of a list of using a list of voiced cells,
//	// use two classes: monophonic measure and poliphonic measure.
//	//    (both extending a common class measure)
//	//												
//	// the first being a list of simple cells, 
//	// and the second being a list of monophonic measures 
//	// sharing time signature and resolution.
//	
//	
//	TimeSignature tsig;
//	DurationValue resolution;
//	
//	
//	/**
//	 * Appends element at the last free position
//	 * @param element
//	 */
////	public abstract void append(BasicPlayableElement element);
//	
//	public abstract TransitionStructure toTransitionStructure();
//	
////	public abstract List<BasicPlayableElement> toPlayableElementList();
//	
//	@Override
//	protected abstract void playImpl(MidiPlayer dev) throws PlayException;
//	
//	@Override
//	public abstract String toString();
//	
//	
	
	
	
	enum PlayableFragmentType {start, sustain, end; }
	
	/**
	 * Contents of a cell in our structure.
	 * It will contain a fragment of a basic playable
	 * element (note, rest...).
	 * @author marbu
	 *
	 */
	class MeasureVoiceCell {
		BasicPlayableElement playableElement;
		PlayableFragmentType fragmentType;
		
		public MeasureVoiceCell(BasicPlayableElement p, PlayableFragmentType s){
			playableElement = p;
			fragmentType = s;
		}
		
		public boolean isStart(){
			return fragmentType.equals(PlayableFragmentType.start);
		}
		
		public boolean isEnd(){
			return fragmentType.equals(PlayableFragmentType.end);
		}
		
		public boolean isSustain(){
			return fragmentType.equals(PlayableFragmentType.sustain);
		}
	}
	
	/**
	 * Each measure position can 
	 * hold a list of cells, one for each voice.
	 * @author marbu
	 *
	 */
	class MeasureCell {
		
		/**
		 * Voices are stored as a positional list.
		 * Higher-pitched voices go first.
		 * Unpitched voices go in the last positions.
		 * 
		 * In order to calculate the correct placement
		 * for a new voice in the list, we can use
		 * the helper function {@link calculateVoiceIndex}
		 */
		// Higher pitched voices go first.
		// Unpitched voices go in the last positions.
		List<MeasureVoiceCell> voices;
		
		public MeasureCell(){
			this.voices = new ArrayList<MeasureVoiceCell>();
		}
		
		public MeasureCell(MeasureVoiceCell ... voicesContents){
			this();
			for(MeasureVoiceCell c : voicesContents){
				voices.add(c);
			}
		}
		
		public void add(MeasureVoiceCell c){
			// find voice number and store there
			int voicenum = calculateVoiceIndex(c);
			voices.add(voicenum, c);
		}
		
		public boolean isEmpty(){
			return voices.isEmpty();
		}
		
		/**
		 * Gets contents of a voice.
		 * 
		 * Higher-pitched voices go first.
		 * Unpitched voices go in the last positions.
		 * @param voiceNum
		 * @return
		 */
		public MeasureVoiceCell getVoice(int voiceNum){
			return voices.get(voiceNum);
		}
		
		int calculateVoiceIndex(MeasureVoiceCell c){
			BasicPlayableElement p = c.playableElement;
			if (p instanceof Note){
				return calculateVoiceIndex(((Note)p).getPitch());
			}
			else{
				//unpitched voices go last
				return voices.size();
			}
			
		}
		
		/**
		 * Calculates the voice number that would 
		 * correspond to a given pitch.
		 * @param p
		 * @return
		 */
		int calculateVoiceIndex(Pitch p){
			int currentVoice = 0;
			Note voiceNote;
			for(MeasureVoiceCell voice : voices){
				if( voice.playableElement instanceof Note){
					voiceNote = (Note) voice.playableElement;
					if (voiceNote.getPitch().compareTo(p)==-1){
						//my pitch p is higher that the current voice
						return currentVoice;
					}
				} else {
					//unpitched voices always go at the end,
					//so when we reach one, this is our place
					return currentVoice;	
				}
				currentVoice++;	
			}
			return currentVoice;
		}
	}
	
	
	TimeSignature tsig;
	DurationValue resolution;
	//ADT to hold contents TODO
	//List<List<Playable>> contents;
	//List<MeasureCell> contents;
	MeasureCell[] contents;
	int firstEmptyPosition;
	
	
	public Measure(TimeSignature tsig, DurationValue resolution){
		this.tsig = tsig;
		this.resolution = resolution;
		//contents = new ArrayList<MeasureCell>();
		contents = new MeasureCell[numberOfCells()];
//		for (MeasureCell cell : contents){
//			cell = new MeasureCell();
//		}
		for(int i=0; i<contents.length;i++){
			contents[i] = new MeasureCell();
		}
		firstEmptyPosition = 0;
		
		//override default instrument
		this.instrument = null;
	}
	
	public Measure(TimeSignature tsig){
		this(tsig, Defaults.MIN_DURATION); 
	}
	
//	public Measure(){
//		this(new TimeSignature(0, new DurationValue((float)1/4)), Defaults.MIN_DURATION); 
//	}
	
	int numberOfCells(){
		return tsig.numerator *  tsig.denominator.wholeFragmentsWithResolution(resolution);
	}
	
	/**
	 * Appends element at the last free position after the
	 * furthest positioned element.
	 * 
	 * If the ending position of the measure already has contents,
	 * this method will do nothing. 
	 * 
	 * @param element
	 */
	public void append(BasicPlayableElement element){
//		List<MeasureVoiceCell> cellList = toMeasureCells(element);
//		for(MeasureVoiceCell cell : cellList){
//			contents.add(new MeasureCell(cell));
//		}
		addOnResolutionPosition(firstEmptyPosition, element);
	}
	
	/**
	 * Add a playable element AFTER the position of the given duration unit
	 * repeated a certain number of times.
	 * 
	 * Notice that if numberOfUnits is 0, the element will be placed
	 * at the start of the measure, regardless of the value of the durationUnit.
	 * 
	 * For instance: place a playable element right AFTER
	 * the third quarter note of the measure.
	 * @param indexUnit
	 * @param index
	 * @param element
	 */
	public void addOnPosition(DurationValue durationUnit, int numberOfUnits, BasicPlayableElement element){
		addOnResolutionPosition((durationUnit.wholeFragmentsWithResolution(resolution))*numberOfUnits, element);
	}
	
	/**
	 * Add a playable element in the position of the given duration unit
	 * repeated a certain number of times, setting a beat number as the
	 * start offset.
	 * 
	 * For instance: place a playable element in the position of
	 * the third quarter note after the second beat of the measure.
	 * @param indexUnit
	 * @param index
	 * @param element
	 */
	public void addOnBeatPosition(int beatNumber, DurationValue durationUnit, int numberOfUnits, BasicPlayableElement element){
		addOnResolutionPosition(
				numberOfUnits*(durationUnit.wholeFragmentsWithResolution(resolution))
				+beatNumber*(tsig.denominator.wholeFragmentsWithResolution(resolution))
				, element);
	}
	
	/**
	 * Adds a playable element on the absolute position indexed by the resolution unit.
	 * 
	 * For instance: if we have a resolution of 16ths, addOnResolutionPosition(4, element)
	 * will place the element four 16th-notes after the start of the measure. If the index 
	 * is 0, it will be placed at the very start of the measure.
	 * 
	 * If we try to insert something too big to fit on the measure, only the fragment
	 * that fits will be inserted. If not, the combined duration of the non-inserted fragments
	 * will be returned.
	 * @param index
	 * @param element
	 */
	public DurationValue addOnResolutionPosition(int index, BasicPlayableElement element){
		List<MeasureVoiceCell> cellList = toMeasureCells(element);
		//see if the element fits in the measure
		//boolean fits = (tsig.numerator==0) || (cellList.size()> tsig.measureDuration().inResolution(resolution));
		DurationValue remainerDuration = new DurationValue(0); //duration of the fragments that could not be inserted
		for(MeasureVoiceCell cell : cellList){
			if (index<numberOfCells()){
				contents[index].add(cell);
				index++;
			}
//			if (index<=(int)this.duration().inResolution(resolution)){
//				contents.get(index).add(cell);
//				contents[index]
//				index++;
//			}
			else {
				//no more cells fit in the measure
				remainerDuration.add(resolution);
				//break;
			}
			
		}
		if(index>firstEmptyPosition){
			firstEmptyPosition = index;
		}
		return remainerDuration;
	}
	
	
	/**
	 * Whole measure duration
	 * @return
	 */
	public DurationValue duration(){
		return tsig.measureDuration();
	}
	
	/**
	 * Converts a playable element to a list of measure cells
	 * @param element
	 * @return
	 */
	List<MeasureVoiceCell> toMeasureCells(BasicPlayableElement element){
		List<MeasureVoiceCell> cellList = new ArrayList<MeasureVoiceCell>();
		int fragments =  element.getDurationValue().wholeFragmentsWithResolution(resolution);
		for(int i=1; i<fragments-1; i++){
			cellList.add(new MeasureVoiceCell(element, PlayableFragmentType.sustain));
		}
		cellList.add(new MeasureVoiceCell(element, PlayableFragmentType.end));
		cellList.add(0, new MeasureVoiceCell(element, PlayableFragmentType.start));
		
		log.debug("element "+element+" spans "+cellList.size()+" cells");
		
		return cellList;
	}
	
//	public List<BasicPlayableElement> getBeat(int beatNum){
//	 contents.get(beatNum*(int)tsig.denominator.inResolution(resolution));
//	}
	
	// Deprecating this
//	public TransitionStructure toTransitionStructure(){
//		SequentialStructure seq = new SequentialStructure();
//		for (MeasureCell cell : contents){
//			Instruction cellInst; //instruction for playing the contents of the cell
//			if (cell.voices.isEmpty()){
//				//add rest of duration=resolution
//				cellInst = new PlayMusicInstruction(new Rest(resolution));
//			}
//			else {
//				//add cell elements in parallel
////				boolean  noStarts = true; //will be set to true if an element starts on this cell
//				ParallelStructure paral = new ParallelStructure();
//				for(MeasureVoiceCell voice : cell.voices){
//					if (voice.isStart()){
//						try {
//							//add voice in parallel
//							if(this.instrument==null){
//								paral.addSingleElement(new PlayMusicInstruction(voice.playableElement));
//							}
//							else { //inherit measure instrument
//								paral.addSingleElement(new PlayMusicInstruction(new InstrumentWrapper(this.instrument, voice.playableElement)));
//							}
//						} catch (InvalidStateTransitionException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
////						noStarts = false;
//					}
//					
//				}
////				if (noStarts){
////					//we haven't added any elements, so we need to play a rest
////					cellInst = new PlayMusicInstruction(new Rest(resolution));
////				}
////				else {
//					//cellInst = paral;
//					cellInst = new ExecControlStructureInstruction(new ControlStructure(paral, null));
////				}
//			}
//			try {
//				seq.addSingleElement(cellInst);
//			} catch (InvalidStateTransitionException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//		}
//		
//		return seq;
//		
//	}
	
	@Override
	protected void playImpl(MidiPlayer dev) throws PlayException {
		log.debug("playing a measure: "+this);
		
		
		for (MeasureCell cell : contents){
			if (cell.voices.isEmpty()){
				//If we have nothing in that cell, play rest of duration=resolution
				new Rest(resolution).playImpl(dev);
			}
			else {
				// I we have things, play them if they start on this cell
				for(MeasureVoiceCell voice : cell.voices){
					if (voice.isStart()){
						voice.playableElement.playImpl(dev);
					}
				}
			}
		}
//		//maybe not the best option, but for sure the easiest!
//		toTransitionStructure().exec();
	}

	@Override
	public String toString() {
		String measurestr = "measure(r="+resolution+"):\n";
		int cellNum = 0;
		for(MeasureCell cell : contents){
			for(MeasureVoiceCell voice : cell.voices){
				measurestr += "    cell#"+cellNum+": ";
				if(voice.isStart()){
					measurestr += "START of ";
				} else if (voice.isEnd()){
					measurestr += "END of   ";
				}
				else if (voice.isSustain()) {
					measurestr += "   -     ";
				}
				measurestr += voice.playableElement+"\n";
			}
			cellNum++;
		}
		return measurestr;
	}
	
//	TODO
//	public void merge(Measure mes){
//  
//	}
	 

	
}
