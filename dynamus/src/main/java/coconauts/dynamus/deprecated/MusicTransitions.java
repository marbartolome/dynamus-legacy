//package coconauts.dynamus.deprecated;
//
//import java.io.Serializable;
//import java.util.*;
//
//import coconauts.dynamus.music.MidiPlayer;
//import coconauts.dynamus.music.playables.EmptyMusic;
//import coconauts.dynamus.music.playables.Playable;
//import coconauts.dynamus.music.playables.Playable.PlayException;
//import coconauts.dynamus.music.playables.controlStructures.MusicTransitions.State.AmbiguousEventLogicException;
//
//import org.apache.log4j.Logger;
//
//
//import coconauts.dynamus.compiler.controlStructures.ControlStructure;
//import coconauts.dynamus.compiler.controlStructures.TransitionStructure;
//import coconauts.dynamus.compiler.controlStructures.TransitionStructure.InvalidStateTransitionException;
//import coconauts.dynamus.compiler.instructions.PlayMusicInstruction;
//import coconauts.dynamus.events.*;
//import coconauts.dynamus.events.EventListener;
//import coconauts.dynamus.events.EventListener.AlreadyListeningException;
//import coconauts.dynamus.events.AgregateEventLogic.Operator;
//
//
//public class MusicTransitions extends Playable implements Serializable{
//	
//	private static Logger log = Logger.getLogger(MusicTransitions.class);
//	
//	
//	//Map<String, State> states;
//	//State startState;
//	
//	TransitionStructure transitions;
//	
//	public MusicTransitions(){
//		//this.states = new HashMap<String,State>();
//		//this.startState = new State(null); //start state with no music
//		this.transitions = new TransitionStructure();
//	}
//	
//	
//	
//	/**
//	 * Constructor with data to create the start state
//	 */
//	/*public TransitionStructure(String startState, MusicStructure music){
//		this();
//		addStartState(startState, music);
//	}*/
//	
//	/*public void addStartState(String name, MusicStructure music){
//		this.addState(name, music);
//		try {
//			this.makeStartState(name);
//		//It should  never enter the catch though
//		} catch (InexistentStateException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}*/
//	
//	public void addState(String name, Playable music){
//		//states.put(name, new State(music));
//		transitions.addState(name, new PlayMusicInstruction(music));
//	}
//	
//	/**
//	 * Makes the specified state the start state.
//	 * State must previously exist in our machine,
//	 * or a exception will be thrown.
//	 * @param name
//	 */
//	/*public void makeStartState(String name) throws InexistentStateException{
//		if(states.containsKey(name)){
//			this.startState = name;
//		}
//		else{
//			throw new InexistentStateException();
//		}
//	}*/
//	
//	public void addTransition(String from, String to, EventLogic evl) throws InvalidStateTransitionException, InvalidStateTransitionException, AmbiguousEventLogicException, InvalidStateTransitionException {
//		/*if (states.containsKey(from) && states.containsKey(to)){
//			states.get(from).addTransition(evl, new Transition(to));
//			//states.get(from).transitions.put(evl, new Transition(to));
//			//Transition trans = new Transition(to, evl);
//			//states.get(from).addTransition(trans);
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}*/
//		
//		transitions.addTransition(from, to, evl);
//	}
//	
//	public void addTransition(String from, String to, EventLogic evl, Playable using) throws InvalidStateTransitionException, AmbiguousEventLogicException{
//		/*if (states.containsKey(from) && states.containsKey(to)){
//			states.get(from).addTransition(evl, new Transition(to, using));
//			//states.get(from).transitions.put(evl, new Transition(to, using));
//			//Transition trans = new Transition(to, evr, using);
//			//states.get(from).addTransition(trans);
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}*/
//		transitions.addTransition(from, to, evl, new PlayMusicInstruction(using));
//	}
//	
//	public void addEndOfStateTransition(String from, String to) throws InvalidStateTransitionException{
//		/*if (states.containsKey(from) && states.containsKey(to)){
//			states.get(from).addEOSTransition(new Transition(to));
//			//EventReference evr = new EventReference(from+".end");
//			//states.get(from).transitions.put(evr, new Transition(to));
//			//Transition trans = new Transition(to, evr);
//			//states.get(from).addTransition(trans);
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}*/
//		transitions.addEndOfStateTransition(from, to);
//	}
//	
//	public void addEndOfStateTransition(String from, String to, Playable using) throws InvalidStateTransitionException{
//		/*if (states.containsKey(from) && states.containsKey(to)){
//			states.get(from).addEOSTransition(new Transition(to, using));
//			//EventReference evr = new EventReference(from+".end");
//			//states.get(from).transitions.put(evr, new Transition(to, using));
//			//Transition trans = new Transition(to, evr, using);
//			//states.get(from).addTransition(trans);
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}*/
//		transitions.addEndOfStateTransition(from, to, new PlayMusicInstruction(using));
//	}
//	/*
//	public void addExitTransition(String from, EventLogic evl) throws InvalidStateTransitionException, AmbiguousEventLogicException{
//		if (states.containsKey(from)){
//			String to = "SMEXIT";
//			states.get(from).addTransition(evl, new Transition(to));
//			//states.get(from).transitions.put(evl, new Transition(to));
//			/*Transition trans = new Transition(to, evl);
//			states.get(from).addTransition(trans);
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addExitTransition(String from, EventLogic evl, MusicStructure using) throws InvalidStateTransitionException, AmbiguousEventLogicException{
//		if (states.containsKey(from)){
//			String to = "SMEXIT";
//			states.get(from).addTransition(evl, new Transition(to, using));
//			//states.get(from).transitions.put(evl, new Transition(to, using));
//			//Transition trans = new Transition(to, evl, using);
//			//states.get(from).addTransition(trans);
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addEOSandExitTransition(String from) throws InvalidStateTransitionException{
//		if (states.containsKey(from)){
//			String to = "SMEXIT";
//			states.get(from).addEOSTransition(new Transition(to));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addEOSandExitTransition(String from, MusicStructure using) throws InvalidStateTransitionException{
//		if (states.containsKey(from)){
//			String to = "SMEXIT";
//			states.get(from).addEOSTransition(new Transition(to, using));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	*/
//	
//	public void addStartTransition(String to, EventLogic evl) throws AmbiguousEventLogicException, InvalidStateTransitionException{
//		/*if (states.containsKey(to)){
//			startState.addTransition(evl, new Transition(to));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}*/
//		transitions.addStartTransition(to, evl);
//	}
//	
//	public void addStartTransition(String to, EventLogic evl, Playable using) throws InvalidStateTransitionException, AmbiguousEventLogicException {
//		if (states.containsKey(to)){
//			startState.addTransition(evl, new Transition(to, using));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	
//	public void addStartEOSTransition(String to) throws InvalidStateTransitionException{
//		if (states.containsKey(to)){
//			startState.addEOSTransition(new Transition(to));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void addStartEOSTransition(String to, Playable using) throws InvalidStateTransitionException, AmbiguousEventLogicException {
//		if (states.containsKey(to)){
//			startState.addEOSTransition(new Transition(to, using));
//		}
//		else{
//			throw new InvalidStateTransitionException();
//		}
//	}
//	
//	public void removeTransition(String from, String to, EventLogic evl){
//		if(states.containsKey(from)&&states.containsKey(to)){
//			states.get(from).removeTransition(evl, to);
//		}
//		else {
//			//do nothing, nothing to remove
//		}
//	}
//	
//	public void removeEOSTransition(String from, String to){
//		if(states.containsKey(from)&&states.containsKey(to)){
//			states.get(from).removeEOSTransition(to);
//		}
//		else {
//			//do nothing, nothing to remove
//		}
//	}
//	
//	public int numberOfStates(){
//		return this.states.size();
//	}
//	
//	/*public boolean hasStartState(){
//		return this.startState!=null;
//	}*/
//	
//	/**
//	 * Returns true if the machine has at least one
//	 * start transition.
//	 */
//	public boolean isPlayable(){
//		return (this.startState.numberOfEOSTransitions()+this.startState.numberOfTriggeringEventLogics()) > 0;
//	}
//	
//	/**
//	 * Returns true if the machine is complete and 
//	 * ready to be used.
//	 * 
//	 * A machine is complete if:
//	 * - there is at least one transition to the SMEXIT state.
//	 * - each state has at least one transition triggered by it's state end event.
//	 * - there are no inconexe states
//	 * 
//	 * Ok, for now we're not needing this
//	 * @return
//	 *
//	public boolean isComplete(){
//		
//	}*/
//
//
//	/**
//	 * Plays the concurrent state machine.
//	 * 
//	 * for each state:
//	 * 		set up listener for combined logic
//	 * 		play state's music
//	 * 		check fulfilled event transitions
//	 * 			yes: launch parallel transitions (playing trans music)
//	 * 			no: if EOS transitions, launch in parallel
//	 * 			wait for child threads to return
//	 * 		if no transitions at all
//	 * 			end
//	 * 		
//	 */
//	/*public void play(Object ... params ) throws PlayException{
//		if(!isPlayable()){
//			throw new NoStartTransitionsException();
//		}
//		
//		this.playParams = params;
//		this.startState.play(playParams);
//		
//		State currentState = this.startState;
//
//	}*/
//	@Override
//	protected void playImpl(MidiPlayer player) throws PlayException{
//		if(!isPlayable()){
//			throw new NoStartTransitionsException();
//		}
//		this.player = player;
//		//this will take care of all
//		Thread originThread = new Thread(startState);
//		//wait for all to end
//		try {
//			originThread.join();
//		} catch (InterruptedException e) {
//			throw new PlayException(e.getMessage());
//		}
//	}
//	
//	/**
//	 * Plays the state machine.
//	 * 
//	 * Starts with the start state. It plays it's music.
//	 * 
//	 * If an appropriate event arrives during the playing 
//	 * (one that triggers a transition),the music execution will
//	 * finalize at a proper point (escape point or at the end of
//	 * the music structure), and then the machine will move to
//	 * execute the next state.
//	 * 
//	 * If the music within the state reaches it's end and no 
//	 * event arrived, or has no event driven transitions:
//	 * 		- if the state has a end-of-state transition, it will
//	 * 		move to the corresponding next state
//	 * 		- if it doesn't it will just stay idling waiting for an
//	 * 		event that triggers a transition.
//	 * 		- if a state has no outgoing transitions watsoever
//	 * 		(event drived or end-of-state)... it will just stay
//	 * 		 stopped forever!
//	 * 			-
//	 * If the machine reaches a SMEXIT state, the paly method will
//	 * correctly finalize and exit.
//	 * 
//	 * Notice that incomplete state machines are allowed. Their execution 
//	 * might be awkward, and probably not what you should expect, but the
//	 * play method will not complain about it. It is up to you to notice
//	 * if you're constructing a state machine step by step.
//	 * 
//	 * You've got at your disposition several helper methods that can 
//	 * inform you about the current architecture of the state machine:
//	 * 		boolean isCorrect()
//	 * 			Machine if correct if:
//	 * 			- there are no unreachable states
//	 * 			- it has at least an exit transition from a reachable state
//	 * 			- each state has a state-end transition
//	 * 
//	 * 		Map<String, boolean> exitTransitions()
//	 * 			Returns a map with all states, and information about if
//	 * 			they have or don't have an exit transition
//	 * 
//	 * 		Map<String, String> endTransitions()
//	 * 			Returns a map with all states, and information about
//	 * 			if they have an end-of-state transition. If they have,
//	 * 			next State is associated with them, if they don't, 
//	 * 			the map content for that key will be null.
//	 * 
//	 * TODO these are not yet implemented, so implement them!
//	 * @throws PlayException 
//	 */
//	/*@Override
//	public void play(Object... params) throws PlayException {
//		if (!this.hasStartState()){
//			throw new NoStartStateException();
//		}
//		
//		super.isPlaying = true;
//		
//		String nextState = this.startState;
//		
//		log.debug("nextState is: "+nextState);
//		
//		while (!nextState.equals("SMEXIT")){
//			State currentState = this.states.get(nextState);
//			log.trace("currently in state: "+currentState);
//			Set<EventLogic> stateTransEvents = currentState.getTransitionsEvents();
//			EventListener evListener = new EventListener();
//			
//			if (stateTransEvents.size()>0){
//				log.trace("state has "+stateTransEvents.size()+" event-driven transitions");
//				EventLogic stateEL = new AgregateEventLogic(Operator.or, stateTransEvents);
//				try {
//					evListener.listen(stateEL);
//					log.trace("listening to transitions logic: "+stateEL);
//				} catch (AlreadyListeningException e) {
//					// we will really never enter here, 
//					//but Java insists...
//					e.printStackTrace();
//				}
//			}
//			
//			//PLAY
//			//currentState.play(evListener, stateEL, params);
//			log.trace("starting to play");
//			currentState.play(params);
//			log.debug("finished playing");
//			//\PLAY
//			
//			//finished playing, now we have to decide what to do
//			//or where to go next
//
//			Transition trans = null;
//			log.debug("number of transitions:"+currentState.numberOfEventTransitions());
//			
//			//if our listener is still listening
//			//or if there was nothing to listen to (no event transitions)
//			if ((evListener.listening()) || (currentState.numberOfEventTransitions()==0)){
//				log.debug("proceeding with EOS transition");
//				//if we have a EOS transition, we take it
//				//and we stop our listener
//				//nextState = currentState.getNextIfEOS();
//				trans = currentState.getEndOfStateTransition();				
//				if (trans!=null){
//					log.debug("found EOS transition, stopping listening");
//					evListener.stopListening(); //weather it was listening or not, doesn't matter to us
//				}
//				//if we don't have it, we'll just have to wait
//				//until our listener gets what he wants
//				//and then move to the corresponding next state
//				else{
//					log.debug("no EOS transition, listening until logic is fulfilled");
//					//nextState = currentState.nextState(evListener.waitForLogic());
//					trans = currentState.getTransition(evListener.waitForLogic());
//				}
//			}
//			//the listener did listen and stopped because it catched
//			//something during the playback of music
//			else{
//					log.debug("event listener catched something, moving to corresponding state");
//					//nextState = currentState.nextState(evListener.capturedEvents());
//					trans = currentState.getTransition(evListener.capturedEvents());
//			}
//			
//			if (trans==null){ //nowhere to go!
//				throw new NoTransitionsException();
//			}
//			else{ //perform transition
//				log.debug("nextState is: "+trans.getNext());
//				log.debug("playing link music and going there");
//				//play linking music
//				trans.play(params);
//				//get next state
//				nextState = trans.getNext();			
//			}
//			
//		//and then we start from the begining with the new state
//		}
//		
//		super.isPlaying = false;
//	
//	}*/
//
//	@Override
//	public String toString() {
//		// TODO Auto-generated method stub
//		return states.toString();
//	}
//}
