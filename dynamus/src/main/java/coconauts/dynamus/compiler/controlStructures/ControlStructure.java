package coconauts.dynamus.compiler.controlStructures;

import coconauts.dynamus.compiler.Environment;

public class ControlStructure /* implements Instruction*/{
	TransitionStructure trans;
	Environment ns; 
	
	
	public ControlStructure(TransitionStructure trans, Environment ns){
		this.trans = trans;
		this.ns = ns;
	}
	
	public void exec(){
		trans.exec();
	}
	

	public TransitionStructure getTransitionStructure(){
		return trans;
	}
	
	public Environment getEnv(){
		return ns;
	}
}
