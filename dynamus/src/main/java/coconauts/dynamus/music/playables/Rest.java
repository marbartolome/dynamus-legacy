package coconauts.dynamus.music.playables;

import java.io.Serializable;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import org.apache.log4j.Logger;

import coconauts.dynamus.music.MidiPlayer;
//import coconauts.dynamus.music.SoundEngine;
import coconauts.dynamus.music.properties.DurationValue;
import coconauts.dynamus.music.properties.Tempo;


public class Rest extends BasicPlayableElement implements Serializable {
	
	private static Logger log = Logger.getLogger(Note.class);

	
/*------Constructors--------*/
	
	public Rest(String silenceString){
		this.duration = new DurationValue((float)1/Integer.parseInt(silenceString.substring(1)));
	}
	

	public Rest( DurationValue duration ){
		this.duration = duration;
	}

	/**
	 * Creates a cuarter note silence
	 */
	public Rest(){
		this( new DurationValue((float)1/4));
	}
		
	
	/*------Methods--------*/
	
	public DurationValue getDuration(){
		return this.duration;
	}
	
	public String toString(){
		return new String("r"+ duration.toString()) ;	
	}
	
	
	/*public void play(Object ... params){
		//we ignore the params
		this.play(SoundEngine.getMidiReceiver(), SoundEngine.defaultTempo );
	}*/
	
	/*public void play(Receiver receiver, Tempo tempo, Object ... params){
		//we may need parameters float startstamp, float timeoffset if playing is not steady
		
		super.isPlaying = true;
	
		    
	    log.debug("we are playing a rest: "+this);
	    
	    log.debug("tempo: "+tempo);
	    
	    log.debug("rest ms duration: "+tempo.calculateMsDuration(this.duration));
	    
	    //calculate the duration of the note in milliseconds at the given tempo
	    //then sleep for that amount of time
	    try {
			Thread.sleep((long) tempo.calculateMsDuration(this.duration));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    //we finished playing
	    super.isPlaying = false;
	}*/


	@Override
	protected void playImpl(MidiPlayer dev) throws PlayException {
		
		log.debug("we are playing a rest: "+this);
		    
	    log.debug("tempo: "+tempo);
	    
	    log.debug("rest ms duration: "+tempo.calculateMsDuration(this.duration));
	    
	    //calculate the duration of the note in milliseconds at the given tempo
	    //then sleep for that amount of time
	    try {
			Thread.sleep((long) tempo.calculateMsDuration(this.duration));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}



