package coconauts.dynamus.compiler;

import java_cup.runtime.*;
import java.io.IOException;
import org.apache.log4j.Logger;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

//import coconauts.dynamus.compiler.MusicStringSym;

//import sun.util.logging.resources.logging;

%%

%class MusicStringLexer

%unicode //user unicode
%line  //count lines and columns
%column

%public
//%final

//CUP compatibility
%cupsym MusicStringParserSym
%cup


%init{
	// code that goes to constructor
%init}


%{	//user code section
	static Logger log = Logger.getLogger("MusicStringLexer");
	
	private Symbol sym(int type)
	{
		return sym(type, yytext());
	}

	private Symbol sym(int type, Object value)
	{
		//add +1 to yyline and yycolumn because they seem to start their count at 0
		return new Symbol(type, yyline+1, yycolumn+1, value);
	}

	private void error() throws IOException
	{
		throw new IOException("LEX -- error at line = "+(yyline+1)+", column = "+(yycolumn+1)+", text = '"+yytext()+"'");
	}
	
	private void syntaxError(){
		log.error("illegal text at line = "+(yyline+1)+", column = "+(yycolumn+1)+", text = '"+yytext()+"'");
		this.syntaxErrors++;
	}
	
	//will contain the current variable name when scanning a variable declaration
	String currentVarname = "";
	
	//counting number of errors
	int syntaxErrors = 0;
	
	public int syntaxErrors(){
		return this.syntaxErrors;
	}
	
	public boolean syntaxErrorsPresent(){
		return this.syntaxErrors>0;
	}
	
	
%}

    

LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]
InputCharacter = [^\r\n]
NotBlank = [^ \r\n\t\f]
                                     
            
/*music string tokens*/
Pitch = [a-g](#{0,3}|b{0,3})
Duration = (1|2|4|8|16|32|64|128)(\.){0,2}
Rest = r
OctaveUp = '
OctaveDown = ,

ANY = .|\n 
/*
TimeSignature = {DurationValue}"/"{DurationVaue}
Tonality = {PitchClass}[Mm]
OctaveMod = (,+|'+)
Octave = OCT[0-9]

Identifier = [A-z][A-z0-9_]*
               */

/* Dynamus tokens            
DurationValue = (1|2|4|8|16|32|64|128)
PitchClass = [a-g][#b]?
Note = {PitchClass}(,+|'+)?{DurationValue}\.{0,2}
Rest = r{DurationValue}\.{0,2}
TimeSignature = {DurationValue}/{DurationVaue}
Tonality = {PitchClass}[Mm]
//InvalidNote = [a-gr][#b]?[:digit:]+
//Octave = OCT[0-9]
//we are restraining variable names so that they can't be called as a note or a rest. TODO Maybe in the future we could find a way so that this restricion is not neccesary         
//VarName = !( !([A-z][A-z0-9_]*) | {Note} | {Rest} ) //De Morgan laws, saving our ass since the 19th century (we really needed an AND here)
Identifier = [A-z][A-z0-9_]* 
//MusicString = \".*\"
//Event = "ev"
*/



//%state MUSICSTRING
%%

                       
{Pitch}				{
						return sym(MusicStringParserSym.MS_PITCH);
					}
{Rest}				{
						return sym(MusicStringParserSym.MS_REST);
					}
{Duration}			{
						return sym(MusicStringParserSym.MS_DURATION);
					}
{OctaveUp}			{
						return sym(MusicStringParserSym.MS_OCTUP);
					}
{OctaveDown}		{
						return sym(MusicStringParserSym.MS_OCTDW);
					}


{WhiteSpace} 		{/*Ignore*/}

{ANY}				{	
						//syntaxError();
						return sym(MusicStringParserSym.MS_ILLEGAL); 
					}
