package coconauts.dynamus.music.playables.wrappers;

import coconauts.dynamus.compiler.controlStructures.TransitionStructure;
import coconauts.dynamus.music.MidiPlayer;
import coconauts.dynamus.music.playables.Playable;

/**
 * Notice: for the moment, this class is not needed. It is intended to
 * wrap a control structure as if it were a musical object.
 * 
 * In order for this class to be implemented neatly, it should ensure that all the instructions
 * included in the transition structure are of class PlayMusicInstruction.
 * 
 * But it could also be implemented in a "quick and dirty" fashion, 
 * simply providing the transition structure with a variable to store
 * a MidiPlayer, and en the exec method, ensure that if we're executing 
 * a PlayMusicInstruction, it should use that MidiPlayer instead of the 
 * one stored within the PlayMusicInstruction object.
 * @author marbu
 *
 */

public class ControlStructureWrapper extends Playable {
	TransitionStructure transitions;
	@Override
	protected void playImpl(MidiPlayer dev) throws PlayException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

}
