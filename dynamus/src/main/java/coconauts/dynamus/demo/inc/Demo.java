package coconauts.dynamus.demo.inc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
//import java.util.concurrent.ExecutorService;

import coconauts.dynamus.events.API.Event;
import coconauts.dynamus.events.API.EventClient;
import coconauts.dynamus.events.API.EventSystem;

public class Demo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			EventClient client = new EventClient(EventSystem.serverPort, false);
			System.out.println("Conectado a "+client.getConnectionParams());
//			System.out.println("¿Número de músico?");
//			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
//		    int num;
//			num = Integer.parseInt(bufferRead.readLine());
			Thread.sleep(10000);
			client.send(new Event("start"));
			// Read from console input1
			String input = "dummy";
			while(!"s".equals(input)){
				try{	
				    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
				    System.out.println("Número de músico para enviar eventos, 's' para salir");
				    input = bufferRead.readLine();
//				    if ("1".equals(input) || "2".equals(input)){
				    	sendEvent(client, input);
//				    }
				}
				catch(IOException e)
				{
					e.printStackTrace();
					break;
				}
			}
			client.close();
			
//			Player p1 = new Player(client, 1);
//			Player p2 = new Player(client, 2);
//			p.run();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		
	}
	
	public static void sendEvent(EventClient c, String playerNumber) throws IOException{
		c.send(new Event("evpy"+playerNumber));
		System.out.println("    enviado evpy"+playerNumber+ " a "+EventSystem.serverPort);
	} 

}
