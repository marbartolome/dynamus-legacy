package coconauts.dynamus.music.properties;

import java.io.Serializable;

public class Octave implements MusicProperty, Serializable, Comparable{
	int octNumber;
	
	public Octave(int num){
//		if ((num>=0) && (num<10)){
//			this.octNumber = num;
//		}
//		else {
//			//TODO throw error
//		}
		this.octNumber = num;
	}
	
	public int getOctaveNumber(){
		return octNumber;
	}
	
	public void modify(OctaveModifier om){
		this.octNumber += om.getInterval();
	}
	
	/*public int toMidi(){
		return this.octNumber+1;
	}*/
	
	public String toString(){
		return ""+this.octNumber;
	}

	@Override
	public int toMidi() {
		return octNumber+1;
	}

	@Override
	public int compareTo(Object arg) {
		//TODO watch out for type safety
		Octave octave = (Octave) arg;
		if (octNumber > octave.octNumber){
			return 1;
		}
		else if (octNumber < octave.octNumber){
			return -1;
		}
		else {
			return 0;
		}
	}



}
