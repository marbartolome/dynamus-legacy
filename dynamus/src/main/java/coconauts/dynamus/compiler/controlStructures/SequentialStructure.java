package coconauts.dynamus.compiler.controlStructures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

//import org.apache.commons.lang.tuple.Pair;
import org.apache.commons.lang3.tuple.ImmutablePair;

import coconauts.dynamus.compiler.controlStructures.TransitionStructure.InvalidStateTransitionException;
import coconauts.dynamus.compiler.instructions.Instruction;
import coconauts.dynamus.music.MidiPlayer;
import coconauts.dynamus.music.playables.Playable;
//import coconauts.dynamus.music.playables.controlStructures.MusicTransitions.InvalidStateTransitionException;



public class SequentialStructure extends TransitionStructure{
	int currentInsertIndex;
	
	//MusicTransitions transitions;
	
	/*public SequentialStructure(){
		transitions = new MusicTransitions();
	}*/
	
	public SequentialStructure(){
		super();
		currentInsertIndex = 0;
	}
	
	public SequentialStructure(Instruction ... instrs){
		this();
		try {
			this.addElements(instrs);
		} catch (InvalidStateTransitionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*public void addElements(Playable ... musics){
		//Map<String, MusicStructure> musicmap =  new HashMap<String, MusicStructure>();
		List<ImmutablePair<String, Playable>> namedMusics = new ArrayList<ImmutablePair<String, Playable>>();
		int i = 0;
		for(Playable music : musics){
		//	musicmap.put(null, music);
			//namedMusics[i] = new ImmutablePair<String, MusicStructure>(null, music);
			namedMusics.set(i, new ImmutablePair<String, Playable>(null, music));
		}
		//this.addNamedElements(musicmap);
		addNamedElements(namedMusics);
		/*TransitionStructure transitionStruc = new TransitionStructure();
		int i = 0;
		for(MusicStructure music : musics){
			transitionStruc.addState("st"+i, music);
			i++;
		}
		try{
			for(int j=0;j<i;j++){
				transitionStruc.addEndOfStateTransition("st"+j, "st"+(j+1));
			}
			transitionStruc.addStartEOSTransition("st0");
			transitionStruc.addEOSandExitTransition("st"+i);
		} catch(InvalidStateTransitionException e){
			e.printStackTrace();
		}
		*//*
	}*/

	
	 /*private String stateNameResolver(String customName, int position){
		 if (customName==null){
			return "st"+position; 
		 }
		 else{
			 return customName;
		 }
	 }*/
	
	private String stateNameGenerator(Integer pos){
		if (pos==null){
			return "st#"+currentInsertIndex;
		}
		else{
			return "st#"+pos;
		}
	}
	 
	 public void addSingleElement(Instruction instr) throws InvalidStateTransitionException{
		addState(stateNameGenerator(currentInsertIndex), instr); 
		if(currentInsertIndex==0){
			//this was our very first state, add transition from start state
			addStartEOSTransition(stateNameGenerator(currentInsertIndex));
		}
		else{
			//add transition from last state
			addEndOfStateTransition(stateNameGenerator(currentInsertIndex-1), stateNameGenerator(currentInsertIndex));
		}
		currentInsertIndex++;
	 }
	 
	 public void addElements(Instruction ... instrs) throws InvalidStateTransitionException{
		 for(Instruction instr : instrs){
			 addSingleElement(instr);
		 }
	 }
	 
	/* public void addNamedElements(List<ImmutablePair<String, Playable>> elements){
		 Iterator<ImmutablePair<String, Playable>> iter = elements.iterator();
		 ImmutablePair<String, Playable> currentEntry, prevEntry;
		 try{
			//iterate over all map entries, adding all the states 
			//and inter-state transitions
			int pos = transitions.numberOfStates();
			prevEntry = null;
			while(iter.hasNext()){
				//add state
				currentEntry = iter.next();
				pos++;
				transitions.addState(stateNameResolver(currentEntry.getKey(),pos), currentEntry.getValue());
				//add transition
				if(iter.hasNext()&&(prevEntry!=null)){
					prevEntry = currentEntry;
					currentEntry = iter.next();
					transitions.addEndOfStateTransition(stateNameResolver(prevEntry.getKey(), pos-1), stateNameResolver(currentEntry.getKey(), pos));
				}
				prevEntry = currentEntry;
			}
			//add start transition (if not present already)
			if(!transitions.isPlayable()){
				transitions.addStartEOSTransition(elements.get(0).getLeft());
			}

		 }catch (InvalidStateTransitionException e){
				e.printStackTrace();
		}
	 
	 }*/

	/** Creates a sequence providing custom names for the states.
	 * If the name for a state is null, it will be named with 
	 * it's number inside the sequence.
	 * 
	 * @param musics
	 * @throws InvalidStateTransitionException 
	 */
	/*public void addNamedElements(Map<String, MusicStructure> musicmap) {
		try{
			TransitionStructure transitionStruc = new TransitionStructure();
			Iterator<Entry<String, MusicStructure>> iter = musicmap.entrySet().iterator();
			Entry<String, MusicStructure> currentEntry, prevEntry;
			//iterate over all map entries, adding all the states 
			//and inter-state transitions
			int pos = 0;
			prevEntry = null;
			while(iter.hasNext()){
				//add state
				currentEntry = iter.next();
				pos++;
				transitionStruc.addState(stateNameResolver(currentEntry.getKey(),pos), currentEntry.getValue());
				//add transition
				if(iter.hasNext()&&(prevEntry!=null)){
					prevEntry = currentEntry;
					currentEntry = iter.next();
					transitionStruc.addEndOfStateTransition(stateNameResolver(prevEntry.getKey(), pos-1), stateNameResolver(currentEntry.getKey(), pos));
				}
				prevEntry = currentEntry;
			}
			//add transitions
		} catch (InvalidStateTransitionException e){
			e.printStackTrace();
		}
		
	}*/
	
	/*@Override
	public void play(Object... params) throws PlayException {
		transitions.play(params);		
	}*/
	/*protected void playImpl(MidiPlayer player) throws PlayException{
		transitions.play(player);
	}
	*/

	/*@Override
	public String toString() {
		// TODO Auto-generated method stub
		return transitions.toString();
	}*/


	
}
