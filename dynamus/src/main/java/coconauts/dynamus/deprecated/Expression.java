package coconauts.dynamus.deprecated;

public interface Expression<T> {
	public T eval();
}
