package coconauts.dynamus.music.properties;

public class Defaults {
	public static Tempo DEFAULT_TEMPO = new Tempo(90);
	public static Dynamics DEFAULT_DYNAMICS = Dynamics.fff;
	public static DurationValue DEFAULT_DURATION = new DurationValue(1, 4);
	public static Instrument DEFAULT_INSTRUMENT = Instrument.piano;
	public static Octave DEFAULT_OCTAVE = new Octave(4);
	public static TimeSignature DEFAULT_TIME_SIGNATURE = new TimeSignature(4,DEFAULT_DURATION);
	public static DurationValue MIN_DURATION = new DurationValue((float)1/(float)16);
	public static DurationValue DEFAULT_MEASURE_RESOLUTION = MIN_DURATION;
	
}
