package coconauts.dynamus.events.API;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.omg.PortableServer.POAManagerPackage.State;

import coconauts.dynamus.events.EventInputStream;

public class EventClient {
	private static final Logger log = Logger.getLogger(EventClient.class);
	Socket connToDynamus;
	EventOutputStream output;
	Daemon daemonThread;
	boolean running;
	
	/**
	 * When the event client is started, it will run a daemon to listen from events 
	 * sent from within Dynamus. It will then send every event back to the event server.
	 * 
	 * This is a very ugly solution, but it allows us to not have to implement a 
	 * multiclient server socket for the moment. 
	 * @author marbu
	 *
	 */
	private class Daemon extends Thread{
		public void run(){
			log.debug("parallel thread");
			log.debug("creating external server");
			ServerSocket server;
			//Socket connection ;
			//EventInputStream input;
			try {
				//server = new ServerSocket(EventSystem.clientPort);
				server = new ServerSocket();
				server.setReuseAddress(true);
				server.bind(new InetSocketAddress(EventSystem.clientPort));
				log.debug("created external server on CLIENTPORT");
				log.debug("waiting for Dynamus to connect (on CLIENTPORT)");
				if (server.isClosed()) {log.debug(".... btw: CLIENTPORT closed");} else {log.debug(".... btw: CLIENTPORT --NOT-- closed");}
				Socket connection = server.accept();
				log.debug("accepted connection from Dynamus (on CLIENTPORT:"+EventSystem.clientPort+")");
				
				
				
				EventInputStream input = new EventInputStream(connection.getInputStream());
				log.debug("listening to events in daemon mode");
				running = true;
				Event readEvent;
				readEvent = input.readEvent();
				while(!readEvent.equals(EventSystem.endOfProgramEvent)){
					log.debug("event arrived: "+readEvent);
					//send event back to server
					try {
						send(readEvent);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//read next event
					log.debug("waiting for next event");
					readEvent = input.readEvent();
					
				}
				
				log.debug("EOP event arrived");
				send(readEvent);
				log.debug("sent EOP to server");
				try {
					connection.close();
					server.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				log.debug("exiting listening daemon");
				server.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			
		}
	}
	
	public EventClient(String address, boolean daemonMode){
		this(address, EventSystem.serverPort, daemonMode);
	}
	
	public EventClient(int port, boolean daemonMode){
		this("localhost", port, daemonMode);
	}
	
	public EventClient(){
		this("localhost", EventSystem.serverPort, true);
	}
	
	public EventClient(boolean daemonMode){
		this("localhost", EventSystem.serverPort, daemonMode);
	}
	
	public EventClient(String address, int port, boolean daemonMode) {

	     try {
			//connToDynamus = new Socket ("localhost", EventSystem.serverPort);
	    	 connToDynamus = new Socket();
	    	 connToDynamus.setReuseAddress(true);
	    	 connToDynamus.connect(new InetSocketAddress(address, port));
			log.info("client connected to server (on SERVERPORT)");
			
			//ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
			//EventOutputStream output = (EventOutputStream)input.readObject();
			//input.close();
			//input.r
			
		    output = new EventOutputStream(connToDynamus.getOutputStream());
		    
		    if (daemonMode){
		    	running = false;
			    daemonThread = new Daemon();
			    daemonThread.start();
			    
			    log.debug("waiting for daemon to initialize");
			    if(!running){
			    	try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    }
		    }
		    
		    log.debug("client creation complete");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    
	}
	
	public String getConnectionParams(){
		return connToDynamus.getLocalSocketAddress()+":"
				+connToDynamus.getLocalPort();
	}
	
	public void send(Event event) throws IOException{
		log.debug("sending event to Dynamus: "+event);
		output.writeEvent(event);
		log.debug("event "+event+" sent");
	}
	 
	
	public void close(){
		log.debug("closing client");
		try {
			connToDynamus.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (State.ACTIVE.equals(daemonThread.getState())){
			try {
				log.debug("waiting for daemon to return");
				daemonThread.join();
				log.debug("daemon finished successfully");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
