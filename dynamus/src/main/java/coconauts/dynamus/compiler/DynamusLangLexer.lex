package coconauts.dynamus.compiler;

import java_cup.runtime.*;
import java.io.IOException;
import org.apache.log4j.Logger;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

//import sun.util.logging.resources.logging;

//import coconauts.dynamus.compiler.DynamusLangParserSym.*;

%%

%class DynamusLangLexer

%unicode
%line
%column

%public
//%final

//CUP compatibility
%cupsym DynamusLangParserSym
%cup


%init{
	// code that goes to constructor
%init}

%{	//user code section
	static Logger log = Logger.getLogger(DynamusLangLexer.class);
	
	private Symbol sym(int type)
	{
		return sym(type, yytext());
	}

	private Symbol sym(int type, Object value)
	{
		//add +1 to yyline and yycolumn because they seem to start their count at 0
		return new Symbol(type, yyline+1, yycolumn+1, value);
	}

	private void error() throws IOException
	{
		throw new IOException("LEX -- error at line = "+(yyline+1)+", column = "+(yycolumn+1)+", text = '"+yytext()+"'");
	}
	
	private void syntaxError(){
		log.error("illegal text at line = "+(yyline+1)+", column = "+(yycolumn+1)+", text = '"+yytext()+"'");
		this.syntaxErrors++;
	}
	
	//will contain the current variable name when scanning a variable declaration
	String currentVarname = "";
	
	//counting number of errors
	int syntaxErrors = 0;
	
	public int syntaxErrors(){
		return this.syntaxErrors;
	}
	
	public boolean syntaxErrorsPresent(){
		return this.syntaxErrors>0;
	}
	
	
%}



LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]
InputCharacter = [^\r\n]
NotBlank = [^ \r\n\t\f]
                                      
/* comments */
Comment = {TraditionalComment} | {EndOfLineComment} | {DocumentationComment}
TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
EndOfLineComment     = "//" {InputCharacter}* {LineTerminator}
DocumentationComment = "/**" {CommentContent} "*"+ "/"
CommentContent       = ( [^*] | \*+ [^/*] )*

/*reserved words*/
DefineMark = "def"
FireEventMark = "fire"
EventMark = "!"
LoopStartMark = "loop"
LoopUntilMark = "until"
TransStructMark = "transitions"
StateMark = "state"
TransitionMark = "-"  //"if"
TransGoToMark = "->"  // go to

/*delimiters*/
MusicStringStart = \"
MusicStringEnd = \"               
SequenceStart = \{
SequenceEnd = \}
TransStructStart = \<
TransStructEnd = \>


/*operators*/
ParallelOp = \|\|
DefineOp = ":"


/*music string tokens*/
//Pitch = [a-g](#{0,3}|b{0,3})
//Duration = (1|2|4|8|16|32|64|128)(\.){0,2}
//Rest = r
//OctaveUp = '
//OctaveDown = ,
//MusicStringToken = {Pitch} | {Duration} | {Rest} | {OctaveUp} | {OctaveDown} 

/*elements*/

Identifier = [A-z][A-z0-9_]*
//MusicString = {MusicStringStart} ({MusicStringToken})+ {MusicStringEnd}
MusicString = {MusicStringStart} [^\"\"]+ {MusicStringEnd}

ANY = .|\n 


%state MUSICSTRING


%%

{DefineMark}		{  	
						return sym(DynamusLangParserSym.DEF_RW);
					}
{FireEventMark}		{  	
						return sym(DynamusLangParserSym.FEV_RW);
					}
{EventMark}			{	
						return sym(DynamusLangParserSym.EV_RW);
					}
{LoopStartMark}		{
						return sym(DynamusLangParserSym.LOOP_ST_RW);
					}
{LoopUntilMark}		{
						return sym(DynamusLangParserSym.LOOP_UN_RW);
					}
{SequenceStart}		{  	
						return sym(DynamusLangParserSym.SEQ_ST_DL);
					}
{SequenceEnd}		{  	
						return sym(DynamusLangParserSym.SEQ_END_DL);
					}
{TransStructStart}	{
						return sym(DynamusLangParserSym.TRST_ST_DL);
					}
{TransStructEnd}	{
						return sym(DynamusLangParserSym.TRST_END_DL);
					}
"("					{
						return sym(DynamusLangParserSym.LPAR_DL);
					}
")"					{
						return sym(DynamusLangParserSym.RPAR_DL);
					}
{ParallelOp}		{  	
						return sym(DynamusLangParserSym.PARAL_OP);
					}
{DefineOp}			{  	
						return sym(DynamusLangParserSym.DEF_OP);
					}
{TransStructMark}	{
						return sym(DynamusLangParserSym.TRST_RW);
					}
{StateMark}			{
						return sym(DynamusLangParserSym.TRSTATE_RW);
					}
{TransitionMark}	{
						return sym(DynamusLangParserSym.TRCASE_RW);
					}
{TransGoToMark}		{
						return sym(DynamusLangParserSym.TRGOTO_RW);
					}
{Identifier}		{  	
						return sym(DynamusLangParserSym.ID);
					}
{MusicString}		{  	
						//strip delimiters before sending to parser
						//log.debug("full MUSSTR lexeme: "+yytext());
						//log.debug("lexeme without delimiters: "+yytext().substring(1, yytext().length()-1));
						return sym(DynamusLangParserSym.MUSSTR, yytext().substring(1, yytext().length()-1));
					}

{Comment}|{WhiteSpace} {/*Ignore*/}

{ANY}				{	
						return sym(DynamusLangParserSym.ILLEGAL);
					}
