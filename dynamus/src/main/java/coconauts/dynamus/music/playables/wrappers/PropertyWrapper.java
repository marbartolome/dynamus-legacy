package coconauts.dynamus.music.playables.wrappers;

import org.apache.log4j.Logger;

import coconauts.dynamus.music.MidiPlayer;
import coconauts.dynamus.music.playables.Playable;
import coconauts.dynamus.music.properties.Instrument;
import coconauts.dynamus.music.properties.MusicProperty;

public class PropertyWrapper<P extends MusicProperty> extends Playable {
	private static final Logger log = Logger.getLogger(InstrumentWrapper.class);
	
	Playable mus;
	P prop;
	
	public PropertyWrapper(P prop, Playable mus){
		this.prop = prop;
		this.mus = mus;
	}

	protected void playImpl(MidiPlayer player) throws PlayException {
		//remember original property
		P origProp = (P) mus.getProperty(prop.getClass());
		//set wrapper property
		mus.setProperty(prop);
		//play music (with new instrument)
		mus.play(player);
		//revert instrument to original
		mus.setProperty(origProp);		
	}

	@Override
	public String toString() {
		return "<"+super.instrument+">"+mus+"</>";
	}
}
