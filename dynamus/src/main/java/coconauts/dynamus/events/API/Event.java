package coconauts.dynamus.events.API;

import java.io.*;

import coconauts.dynamus.events.EventReference;

public class Event implements Serializable {
	public String name;
	
	public Event(String name){
		this.name=name;
	}
	
	public Event(EventReference evr){
		this.name = evr.getName();
	}
	
	public Event(){
		this("event");
	}
	
	/**
	 * Creates a new event from data read from an inputstream
	 * @param in
	 * @throws IOException 
	 */
	public Event(DataInputStream in) throws IOException{
			this.name = in.readUTF();
	}
	
	public byte[] toBytes(){
		//TODO
		return null;
	}
	
	public boolean isReferencedBy(EventReference er){
		return this.name.equals(er.getName());
	}
	
	public String getName(){
		return this.name;
	}
	
	public boolean equals(Event ev){
		return this.name.equals(ev.name);
	}
	
	public String toString(){
		return this.name;
	}
	
}
