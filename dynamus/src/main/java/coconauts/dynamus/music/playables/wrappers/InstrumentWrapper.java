package coconauts.dynamus.music.playables.wrappers;

import javax.sound.midi.Receiver;
import javax.sound.midi.Synthesizer;

import org.apache.log4j.Logger;

import coconauts.dynamus.music.MidiPlayer;
import coconauts.dynamus.music.MidiPlayer.InstrumentLimitReachedException;
import coconauts.dynamus.music.MidiPlayer.NullInstrumentException;
import coconauts.dynamus.music.playables.Playable;
import coconauts.dynamus.music.playables.Playable.PlayException;
import coconauts.dynamus.music.properties.Instrument;
import coconauts.dynamus.music.properties.MusicProperty;

public class InstrumentWrapper extends Playable{
	private static final Logger log = Logger.getLogger(InstrumentWrapper.class);
	
	Playable mus;
	
	public InstrumentWrapper(Instrument inst, Playable mus){
		super.setInstrument(inst);
		this.mus = mus;
	}

	@Override
	protected void playImpl(MidiPlayer player) throws PlayException {
		//remember original instrument
		Instrument origInstr = mus.getInstrument();
		//set wrapper instrument
		mus.setInstrument(super.instrument);
		//play music (with new instrument)
		mus.play(player);
		//revert instrument to original
		mus.setInstrument(origInstr);		
	}

	@Override
	public String toString() {
		return "<"+super.instrument+">"+mus+"</>";
	}
	
}
