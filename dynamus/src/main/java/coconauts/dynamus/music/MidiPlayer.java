package coconauts.dynamus.music;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Synthesizer;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import coconauts.dynamus.music.playables.Playable;
import coconauts.dynamus.music.playables.Playable.PlayException;
import coconauts.dynamus.music.properties.DurationValue;
import coconauts.dynamus.music.properties.Instrument;
import coconauts.dynamus.music.properties.Octave;
import coconauts.dynamus.music.properties.PitchClass;
import coconauts.dynamus.music.properties.Tempo;

/**
 * This class holds a java representation of a MIDI
 * Synthesizer, that we can use to play our music in.
 * @author rephus
 */
public class MidiPlayer {
	public static final Logger log = Logger.getLogger(MidiPlayer.class);
	
	public class InstrumentLimitReachedException extends Exception {
		
	}
	public class NullInstrumentException extends RuntimeException {
		
	}
	
	/**
	 * Holds a record of the instrument a channel has assigned, 
	 * and the number of voices making use of the instrument on the
	 * channel.
	 * @author marbu
	 *
	 */
	public static class ChannelAssignment {
		Instrument instrument;
		int count;
		
		public ChannelAssignment(){
			this.instrument = null;
			this.count = 0;
		}
		
		public Instrument getInstrument(){
			return instrument;
		}
		
		public int getCounter(){
			return count;
		}
		
		public void setInstrument(Instrument instr){
			this.instrument = instr;
		}
		
		public void incrementCounter(){
			this.count++;
		}
		
		public void decrementCounter(){
			if(count>0){
				count--;
			}
		}
	}
	/** Will hold the identifier of the instrument
	 * currently active in each of the 16 MIDI channels
	 * of the device */
	public static final ChannelAssignment[] activeInstrumentsOnChannels = new ChannelAssignment[16];
	
	/** This is the MIDI device to whom we will send
	 * MIDI messages. He will be responsible of generating
	 * sound with them.*/
	public static final Synthesizer synth = initSynth();
	
	
	
	public static Synthesizer initSynth(){
		Synthesizer synth = null;
		try {
			synth = MidiSystem.getSynthesizer();
		} catch (MidiUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//init channel mappings to instruments
		for(int channelNum=0; channelNum<16; channelNum++){
			activeInstrumentsOnChannels[channelNum] = new ChannelAssignment();
		}
		//load general midi soundbank
		synth.loadAllInstruments(synth.getDefaultSoundbank());
		
		try {
			synth.open();
		} catch (MidiUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return synth;
	}
	
	@Override
	protected void finalize() {
		synth.close();
	}
	
	
	/**
	 * Creates a MIDI player using the default Java 
	 * MIDI Syntheziser.
	 * @throws MidiUnavailableException 
	 */
	public MidiPlayer() throws MidiUnavailableException{
		//this(MidiSystem.getSynthesizer());
		// Does nothing!! maintaining the method for compatibility.
	}
	
	/**
	 * Creates a MIDI player using the provided
	 * MIDI device descriptor.
	 * @param dev
	 */
	public MidiPlayer(Synthesizer synth){
		// Does nothing!! maintaining the method for compatibility.
//		this.synth = synth;
//		//init channel mappings to instruments
//		for(int channelNum=0; channelNum<16; channelNum++){
//			activeInstrumentsOnChannels[channelNum] = new ChannelAssignment();
//		}
//		//load general midi soundbank
//		this.synth.loadAllInstruments(this.synth.getDefaultSoundbank());
//		
////		for (ChannelAssignment instrEntry : activeInstrumentsOnChannels){
////			instrEntry = new ChannelAssignment();
////		}
	}
	
	public void open() throws MidiUnavailableException{
		//synth.open();
	}
	
	public void close(){
		//synth.close();
	}
	
	public boolean isOpen(){
		return synth.isOpen();
	}
	
	public Synthesizer getSynth() throws MidiUnavailableException{
		return synth;
	}
	
	public Receiver getMidiReceiver() throws MidiUnavailableException{
		return synth.getReceiver(); 
	}
	
	public MidiChannel getMidiChannel(int channelNumber){
		return synth.getChannels()[channelNumber];
	}
	
	/**
	 * Returns a currently unused MIDI channel.
	 * Returns null if no channel is availaible.
	 * @return
	 
	public MidiChannel getAvailaibleMidiChannel(){
		int channelNumber = 0;
		for (String instrument : activeInstrumentsOnChannels){
			if (instrument==null){
				break;
			}
			else{
				channelNumber++;
			}
		}
		if (channelNumber<16){
			return getMidiChannel(channelNumber);
		}
		else {
			return null;
		}
	}*/
	
	/**
	 * Returns the number of MIDI channel where it is safe to play
	 * this instrument.
	 */
	public synchronized int reserveResourcesForInstrument(Instrument instr) throws InstrumentLimitReachedException, NullInstrumentException{
		
		if(instr == null) {
			throw new NullInstrumentException();
		}
		
		int channelNumber = 0;
		int firstAvailaibleChannel = 16; //initialized with the highest possible value
		
		for (ChannelAssignment instrEntry : activeInstrumentsOnChannels){
//			log.debug("instr = "+instr);
//			log.debug("instrEntry: "+instrEntry);
			
			if (instr.equals(instrEntry.getInstrument())){
				//found channel with same instrument assigned
				instrEntry.incrementCounter();
				return channelNumber;
			}
			
			else if ((instrEntry.getInstrument() == null)&&(firstAvailaibleChannel==16)){
				//found an available channel, we store it, but keep looking
				//in case there's a channel with same instrument assigned
				firstAvailaibleChannel = channelNumber;
			}
			
			channelNumber++;
			
		}
		
		//if we reached here, we didn't found a channel with the same instrument
		
		if (firstAvailaibleChannel<16){
			//we have found a free channel however, so we reserve that one for our instrument
			activeInstrumentsOnChannels[firstAvailaibleChannel].setInstrument(instr);
			return firstAvailaibleChannel;
		}
		
		else {
			//this is the case where there's not a free channel for us, all are in use
			//by different instruments
			throw new InstrumentLimitReachedException();
		}
	}
	
	public synchronized void releaseResourcesForInstrument(Instrument instr) throws NullInstrumentException{
		if(instr == null) {
			throw new NullInstrumentException();
		}
		
		for (ChannelAssignment instrEntry : activeInstrumentsOnChannels){
			if (instr.equals(instrEntry.getInstrument())){
				instrEntry.decrementCounter();
				if (instrEntry.getCounter()==0){
					instrEntry = null;
				}
				return;
			}
		}
	}


}
