package coconauts.dynamus.events;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import coconauts.dynamus.events.API.Event;

public class EventReference extends EventLogic implements Serializable {
	private static final Logger log = Logger.getLogger(EventReference.class);
	String name;
	
	public EventReference(String name){
		super();
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public boolean isEmpty(){
		return false;
	}
	
	/**
	 * Retrieve a set of sets of event references.
	 * The presence of all the expressions in each set
	 * would evaluate the EventLogic as true.
	 * 
	 * For instance, if our logic is:
	 * (A or B) and (D and C)
	 * 
	 * Out truth values set would be:
	 * 1. A, D, C
	 * 2. B, D, C
	 * 
	 * Either of those combinations will evaluate
	 * the event logic to true.
	 * 
	 */
	@Override
	public Set<Set<EventReference>> getTruthValues(){
		// In this case, we only have one truth value.
		// We will return it encapsulated whithin two nested sets.
		// The inner one will contain the event reference,
		// and the outer one, the inner set.
		Set<Set<EventReference>> outerSet = new HashSet<Set<EventReference>>();
		Set<EventReference> innerSet = new HashSet<EventReference>();
		innerSet.add(this);
		outerSet.add(innerSet);
		return outerSet;
	}
	
	@Override
	public boolean isInLogic(EventReference evr){
		log.trace("checking if ER "+evr+" is in logic: "+this);
		log.trace("....... "+this.equals(evr));
		return this.equals(evr);
	}
	
	@Override
	public boolean evaluate(EventReference ... params){
		//List<EventReference> lista;
		//lista = Arrays.asList(params);

		log.trace("evaluating logic: "+this +" against events: "+Arrays.toString(params)+ "... returns: "+Arrays.asList(params).contains(this));
		//for(EventReference param : params){
			//log.trace("...."+this+" equals "+param+"?: "+this.equals(param));
		//}
		//for(EventReference param : lista){
			//log.trace("...."+this+" equals "+param+"?: "+this.equals(param));
		//}
		//return Arrays.asList(params).contains(this);
		
		
		boolean evalResult = false;
		for(EventReference param : params){
			if (param.equals(this)){
				evalResult = true;
				break;
			}
		}
		
		return evalResult;
	}
	
	public boolean isReferenceOf(Event ev){
		return this.name.equals(ev.getName());
	}
	
	public boolean equals(EventReference evr){
		return this.name.equals(evr.getName());
	}
	/*
	public Set<EventReference> getEvalEvents(){
		return this.evalEvents;
	}

	public void addEvalEvent(EventReference evr){
		boolean added = false;
		if (this.isInLogic(evr)){
			added = evalEvents.add(evr);
		}
	}*/
	

	public String toString(){
		return this.name;
	}
}
