package coconauts.dynamus.music.properties;

public enum Dynamics implements MusicProperty{
	ppp, pp, p, mf, f, ff, fff;
	
	public int toMidi(){
		final int MIDI_DYNAMIC_RANGE = 127;
		int ordinal = this.ordinal()+1;
		int numberOfValues = Dynamics.values().length;
		return (int) (MIDI_DYNAMIC_RANGE * ((float)1/(float)numberOfValues) * (this.ordinal()+1));
	}
}
