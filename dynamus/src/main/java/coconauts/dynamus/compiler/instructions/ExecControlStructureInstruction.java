package coconauts.dynamus.compiler.instructions;

import coconauts.dynamus.compiler.Environment;
import coconauts.dynamus.compiler.controlStructures.ControlStructure;
import coconauts.dynamus.compiler.controlStructures.TransitionStructure;

/**
 * Executes a transition structure that uses objects defined in a namespace
 * @author marbu
 *
 */
public class ExecControlStructureInstruction implements Instruction{
	ControlStructure cs;
	
	public ExecControlStructureInstruction(ControlStructure cs){
		this.cs = cs;
	}
	
	public void exec(){
		cs.exec();
	}
//	
//	TransitionStructure trans;
//	Environment ns; //TODO i "think" that this is not neccessary outside the compiler. Take it out of here.
//	
//	
//	public ExecControlStructureInstruction(TransitionStructure trans, Environment ns){
//		this.trans = trans;
//		this.ns = ns;
//	}
//	
//	public void exec(){
//		trans.exec();
//	}
}


//
///**
// * Executes a transition structure that uses objects defined in a namespace
// * @author marbu
// *
// */
//public class ExecControlStructureInstruction implements Instruction{
//	TransitionStructure trans;
//	Environment ns; //TODO i "think" that this is not neccessary outside the compiler. Take it out of here.
//	
//	
//	public ExecControlStructureInstruction(TransitionStructure trans, Environment ns){
//		this.trans = trans;
//		this.ns = ns;
//	}
//	
//	public void exec(){
//		trans.exec();
//	}
//}