package coconauts.dynamus.music.properties;

public class Interval {
	int semitones;
	
	/**
	 * Interval between two pitches
	 * @param p1
	 * @param p2
	 */
	public Interval(Pitch p1, Pitch p2){
		semitones = p2.toMidi() - p1.toMidi();
	}
	
	public int getInSemitones(){
		return semitones;
	}
	
	public int getInSemitonesAbsolute(){
		if(semitones<0){
			return semitones*(-1);
		}else{
			return semitones;
		}
	}
}
